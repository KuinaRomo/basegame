#include "singletons.h"
//Include our classes
#include "SoundManager.h"
#include <stdio.h>

#define  BGMMusicFile  "Assets/snd/levels.bgm"

/*
 * ChannelFinished
 * PROBLEMA:
 *    SDL_Mixer quiere que esto esta en C clasico y no puedo pasar un metodo de una clase.
 *    asi que trato todo hacia el singleton de Sonido
 */
void ChannelFinished(int channel){
	SoundManager* pSM = sSoundManager;
	float diff = pSM->getBGMvolume();
	diff = diff / MAX_VOLUME;
	//Set control Mixer
	pSM->mMixer[channel].volume = -1;
	pSM->mMixer[channel].volumeMAX = -1;
	pSM->mMixer[channel].loop = false;
	pSM->mMixer[channel].isBGM = false;
	pSM->mMixer[channel].nameSnd.clear();
	pSM->mMixer[channel].SourceX = -1;
	pSM->mMixer[channel].SourceY = -1;
	Mix_SetPanning(channel,255,255);
	Mix_SetDistance(channel,0);

	if ((channel == pSM->getBGMChannel(0)) && (pSM->isBGMIntroPlay() && !pSM->isBGMIntroEnd() && !pSM->isBGMMusicPlay() ) ) {
		for (int i=1 ; i<4 ; i++){
			int chn;
			if (pSM->isBGMMusicLoop()){
				chn = pSM->playBGM(pSM->getBGMFileName(i).c_str(),-1);
			}
			else{
				chn = pSM->playBGM(pSM->getBGMFileName(i).c_str(),0);
			}
			if (chn >= 0){ // Only if channel is played
				pSM->setBGMChannel(i,chn);
				pSM->volumeChn(chn,(int)(pSM->getBGMVolEnd(i)*diff));
			}
		}
		pSM->setBGMChannel(0,-1);
		pSM->setBGMIntroEnd (true);
		pSM->setBGMMusicPlay (true);
	}
}


SoundManager* SoundManager::pInstance = NULL;

SoundManager* SoundManager::getInstance(){
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new SoundManager();
	}
	return pInstance;
}

SoundManager::SoundManager(){
	init();
}

SoundManager::~SoundManager(){
	mBGMMap.clear();
	mFXMap.clear();
}

Uint8 SoundManager::init(){
	if (SDL_InitSubSystem(SDL_INIT_AUDIO) < 0 )
	 {
		std::cerr << "Error when inititalizing AUDIO SDL:" << SDL_GetError() << std::endl;
		exit (1);
	 }
	 if (Mix_OpenAudio(44100,MIX_DEFAULT_FORMAT,2,1024) < 0 ) // Stereo 44100Hz
	 {
		std::cerr << "Error when inititalizing SDL MIXER:" << SDL_GetError() << std::endl;
		exit (1);
	 }
	 Mix_AllocateChannels(MAX_CHANNELSOUND); // Canales para FX de Sonido (Sonidos simultaneos).
	 Mix_ReserveChannels(90);
	 mFXvolume = MAX_VOLUME; // Set FX Volume to MIX_MAX_VOLUME(128)
	 mBGMvolume = MAX_VOLUME;// Set BGM Volume to MIX_MAX_VOLUME(128)

	mBGMFileName[0] = "";
	mBGMFileName[1] = "";
	mBGMFileName[2] = "";
	mBGMFileName[3] = "";
	mBGMMusicName = "NONE";

    // Reinit BGM parameters
	BGMreinitVar();
	// Reinit Mixer Controls
	resetMixer ();
	 // EVERY CHANNEL pass through this function when stops playing.
	 Mix_ChannelFinished(ChannelFinished);

	 return 0;
}

void SoundManager::resetMixer(){
	stopAllLoops();
	// Init Control Channels at mMixer
	for (int i=0 ; i < MAX_CHANNELSOUND; i++){
		mMixer[i].volume = -1;
		mMixer[i].volumeMAX = -1;
		mMixer[i].loop = false;
		mMixer[i].isBGM = false;
		mMixer[i].nameSnd.clear();
		mMixer[i].SourceX = -1;
		mMixer[i].SourceY = -1;
	}
	mCharacterPosX = -1;
	mCharacterPosY = -1;
}

void SoundManager::update(){
	//Corte BGM en el caso de estar en Intro y saltar la alarma
	/*if (sGameState->isAlarmOn()){
		if (mMixer[mBGMusic_Channel[0]].isBGM){
			if ((mBGMusic_bIntroPlay && !mBGMusic_bIntroEnd && !mBGMusic_bMusicPlay ) ) {
				stopChn(mBGMusic_Channel[0]); // Al hacer un Stop pasa por ChannelFinished
			}
		}
	}*/

	// Gestion de todos los canales que tengan posicion X,Y
	for (int i=0 ; i < MAX_CHANNELSOUND; i++){
		float screens = 0;
		if ( (mMixer[i].SourceX != -1) && (mMixer[i].volume != -1) ){
			Sint16 pan = mCharacterPosX - mMixer[i].SourceX;// Me da el signo de la direccion del sonido (Neg Right, Pos Left)
			Sint32 distanceX = abs(pan);
			Sint32 distanceY = abs(mCharacterPosY - mMixer[i].SourceY);
			screens = ((distanceX * distanceX) + (distanceY * distanceY)) / (float)((SCREEN_WIDTH*SCREEN_WIDTH)/*+(SCREEN_HEIGHT*SCREEN_HEIGHT)*/);
			//Set control Mixer
			//std::cout << "Chn:" << i << " Dis:" << screens<< "|" << mMixer[i].nameSnd.c_str() ;
			Sint16 AttVol = static_cast<Sint16>(255-(255-(255*(screens/3))));
			if (AttVol<0){
				AttVol = 0;
			}
			if (AttVol>255){
				AttVol = 255;
			}
			Mix_SetDistance(i,static_cast<Uint8>(AttVol));
			//std::cout <<" | -" << AttVol << "- | ";
			// Control de Panning (Done) Var�a en funci�n de distanceX solo.
			Sint16 rightSnd = 255;
			Sint16 leftSnd = 255;
			Sint16 panValue = 255-(255 * (distanceX*100/(SCREEN_WIDTH*2)))/100;
			if (panValue > 0 ){
				if (pan < 0){
					leftSnd = panValue;
				}else{
					rightSnd = panValue;
				}
			}else{
				if (pan < 0){
					leftSnd = 0;
				}else{
					rightSnd = 0;
				}
			}
			Mix_SetPanning(i,static_cast<Uint8>(leftSnd),static_cast<Uint8>(rightSnd));
			//std::cout << "| Pan" << leftSnd << "," << rightSnd << "(" << panValue << " - " << pan << ")";
			//std::cout << "\n";
		}
	}
	// if BGMusic is fadding
	if (mBGMusic_isFadeOut){
		int TotalVolume = 0;
		for (int i=0 ; i<4 ; i++){
			mBGMusic_VolumeEnd[i]  = 0;
			if (mBGMusic_Channel[i] != -1){
				TotalVolume += mBGMusic_VolumePlay[i];
			}
		}
		// BGMusic is now in Zero
		if (TotalVolume == 0){
			BGMusicStop();
		}
	}
	//Update BGMusic Volumen to Volumen End
	mBGMusic_TimeLastUpdate += global_delta_time;
	if (mBGMusic_TimeLastUpdate >= mBGMusic_IncVolumen){ // 1  Change every IncVolumen time (ms)
		mBGMusic_TimeLastUpdate = 0;
		if ( (mBGMusic_bIntroPlay) || (mBGMusic_bIntroEnd) ){
			for (int i=0 ; i<4 ; i++){
				float diff = mBGMvolume;
				diff = diff / MAX_VOLUME;
				int VolumenEnd = (int)(mBGMusic_VolumeEnd[i] * diff);
				int VolumePlay = (int)(mBGMusic_VolumePlay[i] * diff);
				if (mBGMusic_Channel[i] != -1){
					if (VolumenEnd > VolumePlay){
						mBGMusic_VolumePlay[i] +=2;
						if (VolumenEnd < VolumePlay){
							mBGMusic_VolumePlay[i] = mBGMusic_VolumeEnd[i];
						}
						volumeChn(mBGMusic_Channel[i],(int)(mBGMusic_VolumePlay[i] * diff));
					}
					if (VolumenEnd < VolumePlay){
						mBGMusic_VolumePlay[i] -=2;
						if (VolumenEnd > VolumePlay){
							mBGMusic_VolumePlay[i] = mBGMusic_VolumeEnd[i];
						}
						volumeChn(mBGMusic_Channel[i],(int)(mBGMusic_VolumePlay[i] * diff));
					}
				}
			}
		}
	}
}

Uint8 SoundManager::addSound(const char* file, SoundType type){
	if(!isLoaded(file, type)){
		Mix_Chunk* sound = Mix_LoadWAV(file);
		if(sound == NULL){
			std::cerr << "Error when loading sound: "<< file << ": " << SDL_GetError() << std::endl;
			return 1;
		}
		else {
			switch(type){
				case BGM:
					mBGMMap.insert(std::pair<std::string, Mix_Chunk*>(file, sound));
#ifndef __PUBLICRELEASE__
					std::cout<<"Adding BGM: " << file << std::endl;
#endif
					break;
				case FX:
				default:
					mFXMap.insert(std::pair<std::string, Mix_Chunk*>(file, sound));
#ifndef __PUBLICRELEASE__
					std::cout<<"Adding FX: " << file << std::endl;
#endif
					break;
			}
			return 0;
		}
	}
	else{
		return 3;
	}
}

void SoundManager::removeSound(const char* file, SoundType type){
	switch(type){
		case BGM:
			removeBGM(file);
			break;
		case FX:
			removeFX(file);
			break;
		default:
			break;
	}
}

bool SoundManager::isLoaded(const char* file, SoundType type){
	std::map<std::string,Mix_Chunk*>::iterator it;
	switch(type){
		case BGM:
			it = mBGMMap.find(file);
			if(it == mBGMMap.end()){
				return false;
			}
			break;
		case FX:
			it = mFXMap.find(file);
			if(it == mFXMap.end()){
				return false;
			}
			break;
		default:
			break;
	}
	return true;
}

void SoundManager::removeBGM(const char* file){
	std::map<std::string, Mix_Chunk*>::iterator iter = mBGMMap.find(file);
	if(iter == mBGMMap.end()){
#ifndef __PUBLICRELEASE__
		std::cout<<"The file "<<file<<" is not loaded yet. Cannot delete it."<<std::endl;
#endif
		return;
	}
#ifndef __PUBLICRELEASE__
	std::cout<<"Removing " << file << " from the SoundManager BGM"<<std::endl;
#endif
	Mix_FreeChunk(iter->second);
	mBGMMap.erase(file);
}

void SoundManager::removeFX(const char* file){
	std::map<std::string, Mix_Chunk*>::iterator iter = mFXMap.find(file);
	if(iter == mFXMap.end()){
#ifndef __PUBLICRELEASE__
		std::cout<<"The file "<<file<<" is not loaded yet. Cannot delete it."<<std::endl;
#endif
		return;
	}
#ifndef __PUBLICRELEASE__
	std::cout<<"Removing " << file << " from the SoundManager FX"<<std::endl;
#endif
	Mix_FreeChunk(iter->second);
	mFXMap.erase(file);
}

Mix_Chunk* SoundManager::getBGM(const char* file){
	std::map<std::string,Mix_Chunk*>::iterator it = mBGMMap.find(file);
	if(it == mBGMMap.end()){
		addSound(file, BGM);
#ifndef __PUBLICRELEASE__
		std::cout<<"Adding Sound BGM" << file << " (not loaded until now)" <<std::endl;
#endif
		it = mBGMMap.find(file);
	}
	return it->second;
}

Mix_Chunk* SoundManager::getFX(const char* file){
	std::map<std::string, Mix_Chunk*>::iterator it = mFXMap.find(file);
	if(it == mFXMap.end()){
		addSound(file, FX);
#ifndef __PUBLICRELEASE__
		std::cout<<"Adding Sound " << file << " (not loaded until now)" <<std::endl;
#endif
		it = mFXMap.find(file);
	}
	return it->second;
}

int	SoundManager::playBGM(const char* file, Sint8 loops){
    int channel = -1;
	if (strcmp(file,"") != 0){	// Only if soundname is assigned
		channel = Mix_PlayChannel(-1, getBGM(file), loops); // OJO ! en veces a sonar -1-> Infinito 0-> 1 vez. 1-> 2 veces...WTF!
		if (channel >= 0){
			//Set control Mixer
			volumeChn(channel, mBGMvolume);
			mMixer[channel].volumeMAX = MAX_VOLUME;
			mMixer[channel].loop = loops!=0?true:false;
			mMixer[channel].isBGM = true;
			mMixer[channel].nameSnd.assign(file);
			mMixer[channel].SourceX = -1;
			mMixer[channel].SourceY = -1;
		}else{
#ifndef __PUBLICRELEASE__
			std::cout << "BGM " << file << "NOT played. All channels full." <<std::endl;
#endif
		}
	}
    return channel;
}

int	SoundManager::playFX(const char* file, Sint8 loops,int channelNeed, bool forceChannel){
    int channel = -1;
	if (channelNeed == -1){
		channel = Mix_PlayChannel(-1, getFX(file), loops); // OJO ! en veces a sonar -1-> Infinito 0-> 1 vez. 1-> 2 veces...WTF!
	}else{
		if (mMixer[channelNeed].isBGM){	// Can't play on a BGM channel
#ifndef __PUBLICRELEASE__
			std::cout << "FX " << file << " NOT played. Channel selected is BGM." <<std::endl;
#endif
			return -1;
		}
		if(!Mix_Playing(channelNeed)){	
			channel = Mix_PlayChannel(channelNeed, getFX(file), loops); // OJO ! en veces a sonar -1-> Infinito 0-> 1 vez. 1-> 2 veces...WTF!
		}else{
			if (!forceChannel){ // Can't play if is Playing and not forced.
				//std::cout << "FX " << file << " NOT played. Channel selected is Playing sound and no forced. " <<std::endl;
				return -1;
			}else{
				Mix_HaltChannel(channelNeed);	// Forced. Stop and Play new sound
				channel = Mix_PlayChannel(channelNeed, getFX(file), loops); // OJO ! en veces a sonar -1-> Infinito 0-> 1 vez. 1-> 2 veces...WTF!
			}
		}
	}
	if (channel >= 0){
		//Set control Mixer
		volumeChn(channel, mFXvolume);
		mMixer[channel].volumeMAX = MAX_VOLUME;
		mMixer[channel].loop = loops!=0?true:false;
		mMixer[channel].isBGM = false;
		mMixer[channel].nameSnd.assign(file);
		mMixer[channel].SourceX = -1;
		mMixer[channel].SourceY = -1;
	}else{
#ifndef __PUBLICRELEASE__
		std::cout << "FX " << file << " NOT played. All channels full." <<std::endl;
#endif
	}
    return channel;
}

int	SoundManager::playFX(Sint16 posX, Sint16 posY, const char* file, Sint8 loops,int channelNeed, bool forceChannel){
    int channel = -1;
	if (channelNeed == -1){
		channel = Mix_PlayChannel(-1, getFX(file), loops); // OJO ! en veces a sonar -1-> Infinito 0-> 1 vez. 1-> 2 veces...WTF!
	}else{
		if (mMixer[channelNeed].isBGM){	// Can't play on a BGM channel
#ifndef __PUBLICRELEASE__
			std::cout << "FX " << file << " NOT played. Channel selected is BGM." <<std::endl;
#endif
			return -1;
		}
		if(!Mix_Playing(channelNeed)){
			channel = Mix_PlayChannel(channelNeed, getFX(file), loops); // OJO ! en veces a sonar -1-> Infinito 0-> 1 vez. 1-> 2 veces...WTF!
		}else{
			if (!forceChannel){ // Can't play if is Playing and not forced.
				//std::cout << "FX " << file << " NOT played. Channel selected is Playing sound and no forced. " <<std::endl;
				return -1;
			}else{
				Mix_HaltChannel(channelNeed);	// Forced. Stop and Play new sound
				channel = Mix_PlayChannel(channelNeed, getFX(file), loops); // OJO ! en veces a sonar -1-> Infinito 0-> 1 vez. 1-> 2 veces...WTF!
			}
		}
	}
	if (channel >= 0){
		//Set control Mixer
		volumeChn(channel, mFXvolume);
		mMixer[channel].volumeMAX = MAX_VOLUME;
		mMixer[channel].loop = loops!=0?true:false;
		mMixer[channel].isBGM = false;
		mMixer[channel].nameSnd.assign(file);
		mMixer[channel].SourceX = posX;
		mMixer[channel].SourceY = posY;
		// Atenuaci�n del volumen en funci�n de la distancia
		Sint16 pan = mCharacterPosX - mMixer[channel].SourceX;// Me da el signo de la direccion del sonido (Neg Right, Pos Left)
		Sint32 distanceX = abs(pan);
		Sint32 distanceY = abs(mCharacterPosY - mMixer[channel].SourceY);
		float screens = ((distanceX * distanceX) + (distanceY * distanceY)) / (float)((SCREEN_WIDTH*SCREEN_WIDTH)/*+(SCREEN_HEIGHT*SCREEN_HEIGHT)*/);
		//Set control Mixer
		//std::cout << "Chn:" << channel << " Dis:" << screens<< "|" << mMixer[channel].nameSnd.c_str() ;
		Sint16 AttVol = static_cast<Sint16>(255-(255-(255*(screens/3))));
		if (AttVol<0){
			AttVol = 0;
		}
		if (AttVol>255){
			AttVol = 255;
		}
		Mix_SetDistance(channel,static_cast<Uint8>(AttVol));
		//std::cout <<" | -" << AttVol << "- | ";
		// Control de Panning (Done) Var�a en funci�n de distanceX solo.
		Sint16 rightSnd = 255;
		Sint16 leftSnd = 255;
		Sint16 panValue = 255-(255 * (distanceX*100/(SCREEN_WIDTH*2)))/100;
		if (panValue > 0 ){
			if (pan < 0){
				leftSnd = panValue;
			}else{
				rightSnd = panValue;
			}
		}else{
			if (pan < 0){
				leftSnd = 0;
			}else{
				rightSnd = 0;
			}
		}
		Mix_SetPanning(channel,static_cast<Uint8>(leftSnd),static_cast<Uint8>(rightSnd));
		//std::cout << "| Pan" << leftSnd << "," << rightSnd << "(" << panValue << " - " << pan << ")";
		//std::cout << "\n";
	}else{
#ifndef __PUBLICRELEASE__
		std::cout << "FX " << file << " NOT played. All channels full." <<std::endl;
#endif
	}
    return channel;
}


int	SoundManager::playingChn(int chn){
    return Mix_Playing(chn);
}

void SoundManager::stopChn(int chn){
	// Al hacer un Stop pasa por ChannelFinished
    Mix_HaltChannel(chn);
}

void SoundManager::stopSnd(std::string name){
	 for (int i=0 ; i < MAX_CHANNELSOUND; i++){
		 if ((mMixer[i].volume != -1) && (mMixer[i].nameSnd==name) ){
			Mix_HaltChannel(i);
		 }
	 }
}

int SoundManager::volumeChn (int chn, int volume){
	if (mMixer[chn].volume != volume){
		mMixer[chn].volume = volume;
		Mix_Volume(chn,volume);
	}
	return volume;
}

void SoundManager::pauseChn (int chn){
    Mix_Pause(chn);
}

void SoundManager::resumeChn (int chn){
    Mix_Resume(chn);
}

void  SoundManager::BGMusicInit(const char* intro_file, const char* ch1_file, const char* ch2_file, const char* ch3_file) {

	BGMusicStop();	// Stops all BGM Channels

    mBGMFileName[0] = intro_file;
    mBGMFileName[1] = ch1_file;
    mBGMFileName[2] = ch2_file;
    mBGMFileName[3] = ch3_file;

    mBGMusic_bIntroPlay = false;
    mBGMusic_bIntroEnd = false;
    mBGMusic_bMusicPlay = false;

}

void  SoundManager::BGMusicInit(const char* ch1_file, const char* ch2_file, const char* ch3_file) {

	BGMusicStop();	// Stops all BGM Channels

    mBGMFileName[0].clear();
    mBGMFileName[1] = ch1_file;
    mBGMFileName[2] = ch2_file;
    mBGMFileName[3] = ch3_file;

    mBGMusic_bIntroPlay = false;
    mBGMusic_bIntroEnd = true;
    mBGMusic_bMusicPlay = true;

}

void SoundManager::BGMusicUpdate(int VolChn1,int VolChn2,int VolChn3) {
	 if (mBGMusic_bMusicPlay) {
         mBGMusic_VolumeEnd[1] = VolChn1;
         mBGMusic_VolumeEnd[2] = VolChn2;
         mBGMusic_VolumeEnd[3] = VolChn3;
    }
}

/*
 * BGMusicStart
 * Desc: Lanza el BGMusic empezando por su intro y prepara la funcion que 
 *    lanzara los demas wavs que pertenecen a la BGMusic.
 */
void SoundManager::BGMusicStart(int VolIntro,int VolChn1,int VolChn2,int VolChn3,bool loop) {
	float diff = mBGMvolume;
	diff = diff / MAX_VOLUME;
	int Vol[4];
	Vol[0] = VolIntro;
	Vol[1] = VolChn1;
	Vol[2] = VolChn2;
	Vol[3] = VolChn3;

	if (!mBGMusic_bIntroPlay && mBGMusic_bIntroEnd && mBGMusic_bMusicPlay){ // Caso de no tener Intro
		mBGMusic_Channel[0] = -1;
		mBGMusic_VolumeEnd[0] = mBGMusic_VolumePlay[0] = 0; 
		
		for (int i=1 ; i<4 ; i++){
			if (loop){
				mBGMusic_Channel[i] = playBGM(mBGMFileName[i].c_str(),-1);
			}else{
				mBGMusic_Channel[i] = playBGM(mBGMFileName[i].c_str(),0);
			}
			if (mBGMusic_Channel[i] != -1) { // Solo si el canal se ha activado.
				volumeChn(mBGMusic_Channel[i],(int)(Vol[i]*diff));
			}
			mBGMusic_VolumeEnd[i] = mBGMusic_VolumePlay[i] = Vol[i]; 
		}		
        mBGMusic_bIntroPlay = true;
		mBGMusic_bIntroEnd = true;
        mBGMusic_bMusicPlay = true;
		mBGMusic_bMusicLoop  = loop;
	}
	else{
		mBGMusic_Channel[0] = playBGM(mBGMFileName[0].c_str(),0);
		// Guardo el volumen actual al que se lanza.
		if (mBGMusic_Channel[0] != -1) { // Solo si el canal se ha activado.
			volumeChn(mBGMusic_Channel[0],(int)(VolIntro*diff));
		}
		mBGMusic_VolumePlay[0] = VolIntro;

		// El volumen destino al empezar sera igual al que se lanza.
		mBGMusic_VolumeEnd[0] = mBGMusic_VolumePlay[0]; 
		mBGMusic_VolumeEnd[1] = VolChn1; 
		mBGMusic_VolumeEnd[2] = VolChn2; 
		mBGMusic_VolumeEnd[3] = VolChn3; 
		mBGMusic_VolumePlay[1] = mBGMusic_VolumeEnd[1];
		mBGMusic_VolumePlay[2] = mBGMusic_VolumeEnd[2];
		mBGMusic_VolumePlay[3] = mBGMusic_VolumeEnd[3];
		mBGMusic_bIntroPlay = true;
		mBGMusic_bIntroEnd = false;
		mBGMusic_bMusicPlay = false;
		mBGMusic_bMusicLoop  = loop;
	}
}


/*
 * BGMusicStop
 * Desc: Para la reproduccion de BGMusic en seco.
 *
 * Parametros : VOID
 *
 */
void SoundManager::BGMusicStop(bool fadeOut, Uint32 timeFadeOut) {
	std::string currentMusic = sSoundManager->getBGMMusicName();
	if (currentMusic == "NONE"){
		return;
	}
	//Verificamos si la musica ya esta parada.
	int result = 0;
	for (int i=0 ; i<4 ; i++){
		if (mBGMusic_Channel[i] != -1){
			if (mMixer[mBGMusic_Channel[i]].isBGM){
				result++;
			}
		}
	}
	if (!result){// La musica ya esta parada.
		// Reinit BGM parameters
		BGMreinitVar();
		return;
	}
	if (fadeOut){
		BGMusicFadeOut(timeFadeOut);
	}else{
		//Setting bools for correct passing through ChannelFinished
		mBGMusic_bIntroPlay = true;
		mBGMusic_bIntroEnd = true;
		mBGMusic_bMusicPlay = true;

		for (int i=0 ; i<4 ; i++){
			if (mBGMusic_Channel[i]!= -1){
				stopChn(mBGMusic_Channel[i]);
				mBGMusic_Channel[i] = -1;
			}
		}
		// Reinit BGM parameters
		BGMreinitVar();
	}
}

/*
 * BGMreinitVar
 * Desc: Reinicia las variables que controlan el BGM como si fuera nuevo. Se usa en el stop y en el init de SoundManager
 *
 * Parametros : VOID
 *
 */
void SoundManager::BGMreinitVar (){
	mBGMusic_bIntroPlay = false;
    mBGMusic_bIntroEnd = false;
    mBGMusic_bMusicPlay = false;

	mBGMusic_Channel[0] = -1;
	mBGMusic_Channel[1] = -1;
	mBGMusic_Channel[2] = -1;
	mBGMusic_Channel[3] = -1;

	mBGMusic_VolumePlay[0] = 0;
	mBGMusic_VolumePlay[1] = 0;
	mBGMusic_VolumePlay[2] = 0;
	mBGMusic_VolumePlay[3] = 0;

	mBGMusic_VolumeEnd[0] = 0;
	mBGMusic_VolumeEnd[1] = 0;
	mBGMusic_VolumeEnd[2] = 0;
	mBGMusic_VolumeEnd[3] = 0;

	mBGMusic_IncVolumen = 5;	// Default value for fades ingame
	mBGMusic_TimeLastUpdate = 0;

	mBGMusic_isFadeOut = false;
	mBGMMusicName = "NONE";
}

/*
 * BGMClear
 * Desc: Quitar de la mem�ria los sonidos BGM cargados.
 *
 * Parametros : VOID
 *
 */
void SoundManager::BGMClear() {
	for (int i=0; i<4; i++){
		if (mBGMFileName[i].length()){
			removeBGM(mBGMFileName[i].c_str());
		}
	}
}

/*
 * BGMusicFadeOut
 * Desc: Marca el bool para empezar a hacer el fadeout.
 *
 * Parametros : time. Es el tiempo que se le asigna a mBGMusic_IncVolumen que ha de esperar entre frames para hacer un decremento
 *
 */
void  SoundManager::BGMusicFadeOut(Uint32 time) {
	mBGMusic_IncVolumen = time;
	mBGMusic_isFadeOut = true;
}


void  SoundManager::setVolumeOnMixer (SoundType type){
	 // Control Channels at mMixer
	 for (int i=0 ; i < MAX_CHANNELSOUND; i++){
		 if (mMixer[i].volume != -1) {
			 switch (type){
				 case BGM:
					 if (mMixer[i].isBGM) { // BGM
						 for (int j=0 ; j<4 ; j++){
							 if (i == mBGMusic_Channel[j]){
								 int volume = (mBGMusic_VolumePlay[j] * (mBGMvolume * 1000) / MAX_VOLUME) / 1000;
								 volumeChn(i, volume);
								 break;
							 }
						 }
					 }
					break;
				 case FX:
					 if(!mMixer[i].isBGM) { // FX
						 int volume = (mMixer[i].volumeMAX * (mFXvolume * 1000) / MAX_VOLUME) / 1000;
						 volumeChn(i, volume);
					 }
					break;
			 }
		 }
	 }
};

void SoundManager::loadLevelBGM(const char* file_path, bool removeBGM){
#if defined (__PUBLICRELEASE__) && defined (__USE_PACK__)
	loadLevelBGMPack(file_path, removeBGM);
#else
	FileManager fichero(BGMMusicFile,"r");

	if(!fichero.isOpened()){
		std::cout << "Fichero BGM no encontrado, buscamos en el pack. . ." << std::endl;
		loadLevelBGMPack(file_path, removeBGM);
		return;
	}
	std::string linePath;
	unsigned char soundLoaded = 0;
	do{
		linePath = fichero.getLine();
		std::string levelPath = file_path;
		std::string readPath = linePath;
		if (levelPath == readPath){
			BGMusicStop();	// Stops all BGM Channels
			std::string sndPath[4];
			for (char i=0;i<4;i++){
				linePath = fichero.getLine();
				sndPath[i] = linePath;
				std::string BGMinRam = getBGMFileName(i);
				if(sndPath[i] != "#"){
					if ( BGMinRam != sndPath[i] ){
						if ((removeBGM) && (BGMinRam != "")){
							removeSound(BGMinRam.c_str(),BGM);
						}
						addSound(sndPath[i].c_str(),BGM);
					}
					soundLoaded++;
				}else{ // No hay carga pero es posible que quede residuo de alguno anterior
					if ((removeBGM) && (BGMinRam != "")){ // En ese caso lo eliminamos.
						removeSound(BGMinRam.c_str(),BGM);
					}
					sndPath[i] = ""; // quito el # para que no cargue un sonido llamado "#" ;)
				}

			}
			if (sndPath[0] != ""){ // En caso de tener intro se inicia con play de intro
				BGMusicInit(sndPath[0].c_str(),sndPath[1].c_str(),sndPath[2].c_str(),sndPath[3].c_str());
			}else{
				BGMusicInit(sndPath[1].c_str(),sndPath[2].c_str(),sndPath[3].c_str());
			}
		}
	} while (!fichero.isEOF());
	mBGMMusicName = file_path;
	fichero.close();
#endif
}

void SoundManager::loadLevelBGMPack(const char* file_path, bool removeBGM){
	/*MemFile *memfile = sUnpacker->loadDataFile(BGMMusicFile);

	if(memfile == NULL){
#ifndef __PUBLICRELEASE__
		std::cout << "Fichero BGM no encontrado en el pack" << std::endl;
#endif
		return;
	}
	char linePath[255];
	unsigned char soundLoaded = 0;
	do{
		memfile->getLine(linePath,255);
		std::string levelPath = file_path;
		std::string readPath = linePath;
		levelPath.append("\n");
		if (levelPath == readPath){
			std::string sndPath[4];
			for (char i=0;i<4;i++){
				memfile->getLine(linePath,255);
				int temp = strlen(linePath);
				linePath[temp-1] = '\0';
				sndPath[i] = linePath;
				std::string BGMinRam = getBGMFileName(i);
				if(sndPath[i] != "#"){
					if ( BGMinRam != sndPath[i] ){
						if ((removeBGM) && (BGMinRam != "")){
							removeSound(BGMinRam.c_str(),BGM);
						}
						addSound(sndPath[i].c_str(),BGM);
					}
					soundLoaded++;
				}else{ // No hay carga pero es posible que quede residuo de alguno anterior
					if ((removeBGM) && (BGMinRam != "")){ // En ese caso lo eliminamos.
						removeSound(BGMinRam.c_str(),BGM);
					}
					sndPath[i] = ""; // quito el # para que no cargue un sonido llamado "#" ;)
				}
			}
			BGMusicStop();	// Stops all BGM Channels
			BGMreinitVar();
			if (sndPath[0] != ""){ // En caso de tener intro se inicia con play de intro
				BGMusicInit(sndPath[0].c_str(),sndPath[1].c_str(),sndPath[2].c_str(),sndPath[3].c_str());
			}else{
				BGMusicInit(sndPath[1].c_str(),sndPath[2].c_str(),sndPath[3].c_str());
			}
		}
	} while (!memfile->mfEOF());
	mBGMMusicName = file_path;
	free(memfile);*/
}

void SoundManager::stopAllLoops(){
	for (int i=0; i < MAX_CHANNELSOUND; i++){
		if ((mMixer[i].loop) && (!mMixer[i].isBGM)){	// Canales en Loop
				stopChn(i); // Paralo pol!
		}
	}
}

void SoundManager::pauseFX(){
	for (int i=0; i < MAX_CHANNELSOUND; i++){
		if ((!mMixer[i].isBGM) && (mMixer[i].volume != -1)){	// Canales solo con FX
			Mix_Pause(i); // Pausalo pol!
		}
	}
}

void SoundManager::resumeFX(){
	for (int i=0; i < MAX_CHANNELSOUND; i++){
		if ((!mMixer[i].isBGM) && (mMixer[i].volume != -1)){	// Canales solo con FX
			Mix_Resume(i); // Vuelvelos a encender pol!
		}
	}
}

void SoundManager::pauseBGM(){
	for (int i=0; i < MAX_CHANNELSOUND; i++){
		if (mMixer[i].isBGM){	// Canales solo con BGM
			Mix_Pause(i); // Pausalo pol!
		}
	}
}

void SoundManager::resumeBGM(){
	for (int i=0; i < MAX_CHANNELSOUND; i++){
		if (mMixer[i].isBGM){	// Canales solo con BGM
			Mix_Resume(i); // Vuelvelos a encender pol!
		}
	}
}

void SoundManager::setSourceChannel (int channel, Sint16 posX, Sint16 posY){
	mMixer[channel].SourceX = posX;
	mMixer[channel].SourceY = posY;
}

void SoundManager::setCharacterPos (Sint16 posX, Sint16 posY){
	mCharacterPosX = posX;
	mCharacterPosY = posY;
}

#ifdef __RENDER_MIXER__
void SoundManager::renderMixer(void){
	Sint16 chnW = 6;
	Sint16 initX = (SCREEN_WIDTH - MAX_CHANNELSOUND * chnW) /2;
	Sint16 initY = SCREEN_HEIGHT/2;
	//Fondo
	sFxGfx->rect(initX-5, initY-5, chnW* (MAX_CHANNELSOUND+1), chnW*4 + 128 + 5, 0x7F3F3F3F);
	for (int i=0; i < MAX_CHANNELSOUND; i++){
		//Marcas de ON/OFF y con Stereo
		Uint32 color = 0x7FFF0000;
		if (mMixer[i].isBGM) color |= 0x0000003F;

		if (mMixer[i].volume == -1){
			sFxGfx->rect(initX + i *chnW, initY, chnW-2, chnW*2-2 ,0x7F7F7F7F); //Gris OFF
		}else{
			sFxGfx->rect(initX + i *chnW, initY, chnW-2, chnW*2-2 ,color); //Vermell ON
		}
		//Marcas de loop
		if (!mMixer[i].loop){
			sFxGfx->rect(initX + i *chnW, initY + chnW*2, chnW-2, chnW-2 ,0x7F7F7F7F); //Gris OFF
		}else{
			sFxGfx->rect(initX + i *chnW, initY + chnW*2, chnW-2, chnW-2 ,0x7F0000FF); //Blau ON
		}
		//Volumen
		if (mMixer[i].volume != -1){
			sFxGfx->rect(initX + i *chnW, initY + chnW*3, chnW-2, 128-mMixer[i].volume ,0x7F7F7F7F); //Gris
			sFxGfx->rect(initX + i *chnW, initY + chnW*3 + 128-mMixer[i].volume, chnW-2, mMixer[i].volume ,color); //Rojo
		}else{
			sFxGfx->rect(initX + i *chnW, initY + chnW*3, chnW-2, 128 ,0x7F7F7F7F); //Gris
		}
		//Panning
		if ( (mMixer[i].SourceX != -1) && (mMixer[i].volume != -1) ){
			float screens = 0;
			Sint16 pan = mCharacterPosX - mMixer[i].SourceX;// Me da el signo de la direccion del sonido (Neg Right, Pos Left)
			Sint32 distanceX = abs(pan);
			Sint32 distanceY = abs(mCharacterPosY - mMixer[i].SourceY);
			screens = ((distanceX * distanceX) + (distanceY * distanceY)) / (float)((SCREEN_WIDTH*SCREEN_WIDTH)/*+(SCREEN_HEIGHT*SCREEN_HEIGHT)*/);
			Sint16 AttVol = static_cast<Sint16>(255-(255-(255*(screens/3))));
			if (AttVol<0){
				AttVol = 0;
			}
			if (AttVol>255){
				AttVol = 255;
			}
			Sint16 rightSnd = 255;
			Sint16 leftSnd = 255;
			Sint16 panValue = 255-(255 * (distanceX*100/(SCREEN_WIDTH*2)))/100;
			if (panValue > 0 ){
				if (pan < 0){
					leftSnd = panValue;
				}else{
					rightSnd = panValue;
				}
			}else{
				if (pan < 0){
					leftSnd = 0;
				}else{
					rightSnd = 0;
				}
			}
			//sFxGfx->rect(initX + i *chnW, initY + chnW*3 + panValue/2, chnW-2, 2 ,0x7FFFFFFF); //Blanco
			//Atenuador
			//sFxGfx->rect(initX + i *chnW +1, initY + chnW*3 + AttVol/2, chnW-4, 2 ,0x7F00003F); //Azulado
		}
	}
}
#endif

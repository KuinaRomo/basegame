#ifndef FXGFX_H
#define FXGFX_H

#include "includes.h"

//! FxGfx class
/*!
	Generate special Graphics on Surfaces. Paint shapes, pixels, etc.
	This class is Singleton and never must take an SDL_surface as parameter.
	Works close to Video and ResourceManager
*/
class FxGfx
{
	public:
		//! Constructor of an empty FxGfx.
		FxGfx();

		//! Destructor.
		~FxGfx();

		//! Auxiliar for when loading Surfaces. Locks the Surface
		/*!
			\param ID ID of the graphic
		*/
		void lockSurface(Uint32 ID);

		//! Auxiliar for when loading Surfaces. Unlocks the Surface
		/*!
			\param ID ID of the graphic
		*/
		void unlockSurface(Uint32 ID);

		//! Copy a part of a Surface to another
		/*!
			\param
			\param
			\param
			\param
		*/
		void Copy(Uint32 IDsrc, C_Rectangle src, Uint32 IDdst, C_Rectangle dst);

		//! Puts pixel color DIRECT to videoSurface
		/*!
			\param x X coordinate of the pixel
			\param y Y coordinate of the pixel
			\param pixel pixel color
		*/
		void putPixel(int x, int y, Uint32 pixel);

		//! Puts pixel color on a surface by ID
		/*!
			\param x X coordinate of the pixel
			\param y Y coordinate of the pixel
			\param pixel pixel color
			\param ID ID of the graphic
		*/
		void putPixel(int x, int y, Uint32 pixel, Sint32 ID);

		//! Gets pixel color DIRECT from videoSurface
		/*!
			\param x X coordinate of the pixel
			\param y Y coordinate of the pixel
		*/
		Uint32 getPixel(int x, int y);

		//! Gets pixel color from a surface by ID
		/*!
			\param x X coordinate of the pixel
			\param y Y coordinate of the pixel
			\param ID ID of the graphic
		*/
		Uint32 getPixel(int x, int y, Sint32 ID);

		//! Resizes an image [BEWARE returns SDL_surface]
		/*!
			\param surface graphic to resize
			\param width new width of the graphic
			\param height new height of the graphic
			\return A New SDL_surface with graphic resized
		*/
		SDL_Surface* scaleSurface(SDL_Surface* surface, Uint16 width, Uint16 height);

		//! Paint a C_Rectangle DIRECT on videoSurface
		/*!
			\param x X Coordinate (Up-Left)
			\param y Y Coordinate (Up-Left)
			\param w C_Rectangle Width 
			\param h C_Rectangle Height
			\param color rectangle color 0xRRGGBB
		*/
		void rectFill(Uint16 x,Uint16 y,Uint16 w,Uint16 h,Uint32 color);

		//! Paint a C_Rectangle on a surface by ID
		/*!
			\param x X Coordinate (Up-Left)
			\param y Y Coordinate (Up-Left)
			\param w C_Rectangle Width 
			\param h C_Rectangle Height
			\param color rectangle color 0xAARRGGBB
			\param ID ID of the graphic
		*/
		void rectFill(Uint16 x,Uint16 y,Uint16 w,Uint16 h,Uint32 color, Sint32 ID);

		//! Paint a C_Triangle DIRECT on videoSurface (only lines)
		/*!
			\param tri Triangle to paint
			\param color triangle color 0xAARRGGBB
		*/
		void triangleLine(C_Triangle tri, Uint32 color);
		
		//! Paint a filled C_Triangle DIRECT on videoSurface
		/*!
			\param tri Triangle to paint
			\param color triangle color 0xAARRGGBB
		*/
		void triangleFill(C_Triangle tri, Uint32 color);

		void polygonLine(std::vector<Point>& points, Uint32 color, Uint32 limit = 0);
		void polygonFill(std::vector<Point>& points, Uint32 color, Uint32 limit = 0);

		//! Paint a C_Rectangle DIRECT on VideoSurface, only the edges.
		/*!
			\param x X Coordinate (Up-Left)
			\param y Y Coordinate (Up-Left)
			\param w C_Rectangle Width
			\param h C_Rectangle Height
			\param color rectangle color 0xAARRGGBB
		*/
		void rectLine (Uint16 x,Uint16 y,Uint16 w,Uint16 h,Uint32 color);

		//! Draw a line DIRECT on videoSurface
		/*!
			\param x0 X Coordinate source
			\param y0 Y Coordinate source
			\param x1 X Coordinate target
			\param y1 Y Coordinate target
			\param color 0xRRGGBB
		*/
		void line(Sint16 x0, Sint16 y0, Sint16 x1, Sint16 y1, Uint32 color);

		//! Draw a line DIRECT on videoSurface checking screen boundaries
		/*!
			\param x0 X Coordinate source
			\param y0 Y Coordinate source
			\param x1 X Coordinate target
			\param y1 Y Coordinate target
			\param color 0xRRGGBB
		*/
		void lineSafe(Sint16 x0, Sint16 y0, Sint16 x1, Sint16 y1, Uint32 color);

		//! Draw a circle DIRECT on videoSurface
		/*!
			\param xm X Coordinate center
			\param ym Y Coordinate center
			\param r Radius
			\param color circle color 0xRRGGBB
		*/
		void circle(Sint16 xm, Sint16 ym, Sint16 r, Uint32 color);

		//! Draws a circle on videoSurface checking screen boundaries
		/*!
			\param xm X Coordinate center
			\param ym Y Coordinate center
			\param r Radius
			\param color circle color 0xRRGGBB
		*/
		void circleSafe(Sint16 xm, Sint16 ym, Sint16 r, Uint32 color);

		//! Puts pixel color RGB with alpha no change. Must be RGBA (32 bits surface)
		/*!
			\param x X coordinate of the pixel
			\param y Y coordinate of the pixel
			\param ID ID of the graphic
		*/
		void putPixelRGBonAlpha(int x, int y, Uint32 pixel, Sint32 ID);

		void convertPixelstoData(Sint32 ID, Uint8 data[]);

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		std::string getClassName(){return "FxGfx";};

		//! Gets Singleton instance
		/*!
			\return Instance of FxGfx (Singleton).
		*/
		static FxGfx* getInstance();

	private:
		//! Paints a polygon (only outlines)
		void drawPolygon(const Sint16 * vx, const Sint16 * vy, int n, Uint32 color);

		//! Paints a filled polygon (only outlines)
		void fillPolygon(const Sint16 * vx, const Sint16 * vy, int n, Uint32 color);

		void putPixelSurface(int x, int y, Uint32 pixel, SDL_Surface *surface);
		Uint32 getPixelSurface(int x, int y, SDL_Surface *surface);
		void rectSurface (Uint16 x,Uint16 y,Uint16 w,Uint16 h,Uint32 color, SDL_Surface *surface);

		static FxGfx*				pInstance;		/*!<  Singleton instance*/
};

#endif

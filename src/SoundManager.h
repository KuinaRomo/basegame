#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include "includes.h"
#include <map>

#define MAX_CHANNELSOUND	128
#define MAX_VOLUME			128
enum SoundType{BGM, FX};

typedef struct{
	int			volume;		// Actual Volume in channel
	int			volumeMAX;	// Original volume proportional to mFXVolume
	bool		loop;		// is a loop sound
	bool		isBGM;		// is a BGMusic channel
	std::string	nameSnd;	// name of sound
	Sint16		SourceX;	// X position where sounds has its source
	Sint16		SourceY;	// Y position where sounds has its source
} ChannelMix;

//! SoundManager class
/*!
	Handles the load and management of the sounds in the game.
*/
class SoundManager
{
	public:
		//! Constructor of an empty SoundManager.
		SoundManager();

		//! Destructor.
		~SoundManager();

		//! Initialize Sound system Using SDL_Mixer at 44.1Khz Stereo and with MAX_CHANNELSOUND
		/*!
			\return Zero is all is Initialized and OK.
		*/
		Uint8 init();

		//! Reinit Mixer values. All to stop
		void resetMixer();

		//! Updates BGMusic Volume in fade
		void update();

		//! Adds a Sound to SoundManager and adds to BGM or FX Map.
		/*!
			\param file Filepath to the Sound. It will be ID String.
			\param type Maybe Soundtype BGM or FX
			\return 1 if there's an error when loading or 2 if is load but not insert on any Map.
		*/
		Uint8 addSound(const char* file, SoundType type);

		//! Deletes a sound from Memory and SoundManager map
		/*!
			\param file ID String to the sound
			\param type Maybe Soundtype BGM or FX
		*/
		void removeSound(const char* file, SoundType type);

		//! Gets the sound ID string BGM
		/*!
			\param file ID string to the sound BGM
			\return Mix_Chunk of the sound (RAW sound data pointer)
		*/
		Mix_Chunk* getBGM(const char* file);

		//! Gets the sound ID string FX
		/*!
			\param file ID string to the sound FX
			\return Mix_Chunk of the sound (RAW sound data pointer)
		*/
		Mix_Chunk* getFX(const char* file);

		//! Set Global FX Volume
		/*!
			\param fx_vol Volume for FX sounds (0-128)
		*/
		void setFXvolume(Uint8 fx_vol){if (fx_vol > MAX_VOLUME) fx_vol = MAX_VOLUME;mFXvolume = fx_vol;};

		//! Set Global BGM Volume
		/*!
			\param bgm_vol Volume for BGM sounds (0-128)
		*/
		void setBGMvolume(Uint8 bgm_vol){if (bgm_vol > MAX_VOLUME) bgm_vol = MAX_VOLUME; mBGMvolume = bgm_vol;};

		//! Gets Global FX Volume
		/*!
			\return mFXvolume Volume for FX sounds (0-128)
		*/
		Uint8 getFXvolume(){return mFXvolume;};

		//! Gets Global BGM Volume
		/*!
			\return mFXvolume Volume for BGM sounds (0-128)
		*/
		Uint8 getBGMvolume(){return mBGMvolume;};

		//! Start to play BGM sound at mBGMvolume
		/*!
		 	\param file ID string to the sound BGM
		 	\param loops Number of loops, -1 is infinite loops. 0 once, 1 twice, ...
		 	\return the channel the sample is played on. On any errors, -1 is returned.
		 */
		int	  playBGM(const char* file, Sint8 loops);

		//! Start to play FX sound at mFXvolume
		/*!
		 	\param file ID string to the sound FX
		 	\param loops Number of loops, -1 is infinite loops. 0 once, 1 twice, ...
			\param channelNeed (optional) Force a channel to play. Maybe can't play if forceChannel isn't true
			\param forceChannel (optional) if true stop sound on channelNeed and force play.
		 	\return the channel the sample is played on. On any errors, -1 is returned.
		 */
		int	  playFX(const char* file, Sint8 loops, int channelNeed = -1, bool forceChannel = false);

		//! Start to play FX sound at mFXvolume
		/*!
			\param posX X coordinate for source sound
			\param posY Y coordinate for source sound
			\param file ID string to the sound FX
			\param loops Number of loops, -1 is infinite loops. 0 once, 1 twice, ...
			\param channelNeed (optional) Force a channel to play. Maybe can't play if forceChannel isn't true
			\param forceChannel (optional) if true stop sound on channelNeed and force play.
			\return the channel the sample is played on. On any errors, -1 is returned.
		 */
		int	  playFX(Sint16 posX, Sint16 posY, const char* file, Sint8 loops, int channelNeed = -1, bool forceChannel = false);

		//! Tells you if channel is playing, or not.
		/*!
		 	\param chn Channel to test whether it is playing or not. -1 will tell you how many channels are playing.
		 	\return 0 if the channel is not playing. Otherwise if you passed in -1, the number of channels playing is returned.
		 			If you passed in a specific channel, then 1 is returned if it is playing.
		 */
		int   playingChn(int chn);


		//! Halt channel playback, or all channels if -1 is passed in. After Stops ChannelFinished is executed.
		/*!
		 	\param chn Channel to stop playing, or -1 for all channels.
		 */
		void  stopChn(int chn);

		//! Halt ALL SOUNDS with the same name, playing now on channels.
		/*!
		 	\param name Sound to stop in all channels.
		 */
		void stopSnd(std::string name);

		//! Set Volume to a channel.
		/*!
		 	\param chn Channel to set mix volume for. -1 will set the volume for all allocated channels.
		 	\param volume The volume to use from 0 to MIX_MAX_VOLUME(128). If less than 0 then the volume will not be set.
		 	\return current volume of the channel. If channel is -1, the average volume is returned.
		 */
		int volumeChn (int chn, int volume);

		//! Pause Channel or ALL Channels (-1)
		/*!
			\param chn Channel to pause or (-1) ALL Channels.
		*/
		void pauseChn (int chn);

		//! Pause Channel or ALL Channels (-1)
		/*!
			\param chn Channel to pause or (-1) ALL Channels.
		*/
		void resumeChn (int chn);

		//! Sets all params of BGMusic to init position and set sound to use as intro and 3 voices.
		/*!
		 	\param intro_file ID string to the sound BGM for intro music.
		 	\param ch1_file ID string to the sound BGM for voice 1.
		 	\param ch2_file ID string to the sound BGM for voice 2.
		 	\param ch3_file ID string to the sound BGM for voice 3.
		 */
		void  BGMusicInit(const char* intro_file, const char* ch1_file, const char* ch2_file, const char* ch3_file);

		//! Sets all params of BGMusic to init position and set sound to use ONLY 3 voices no intro.
		/*!
		 	\param ch1_file ID string to the sound BGM for voice 1.
		 	\param ch2_file ID string to the sound BGM for voice 2.
		 	\param ch3_file ID string to the sound BGM for voice 3.
		 */
		void  BGMusicInit(const char* ch1_file, const char* ch2_file, const char* ch3_file);

		//! Change volume of BGM Music  voices
		/*!
		 	\param vol_ch1 New volume for voice 1.
		 	\param vol_ch2 New volume for voice 2.
		 	\param vol_ch3 New volume for voice 3.
		 */
		void  BGMusicUpdate(int vol_ch1, int vol_ch2, int vol_ch3);

		//! Start to play BGM Music with 3 voices at its volume. Use callback SDL function !! BGMusicIntroFinPlayMusic !!.
		/*!
			\param vol_intro New volume for intro
		 	\param vol_ch1 New volume for voice 1.
		 	\param vol_ch2 New volume for voice 2.
		 	\param vol_ch3 New volume for voice 3.
		 */
		void  BGMusicStart(int vol_intro, int vol_ch1, int vol_ch2, int vol_ch3, bool loop = true);

		//! Stop All voices of BGM Music.
		/*!
			\param fadeOut id true makes a fadeout stop. If false cuts BGMusic.
			\param timeFadeOut ms to wait for decrement volume between frames
		*/
		void  BGMusicStop(bool fadeOut = false, Uint32 timeFadeOut = 5);

		// Reinit BGM parameters
		void BGMreinitVar ();

		//! Clean from memory al BGM Music data.
		void  BGMClear();
				
		//! Makes a fadeout of BGMusic. Activates BGMinFade bool.
		/*!
			\param time ms to wait for decrement volume between frames
		*/
		void  BGMusicFadeOut(Uint32 time);

		//! Change volume to FX or BGM sounds on channels.
		/*!
			\param type FX or BGM 
		*/
		void  setVolumeOnMixer (SoundType type);

		//! Loads the Music BGMusic level
		/*!
			\param file_path Path to the .sht file of the map
		*/
		void loadLevelBGM(const char* file_path, bool removeBGM = true);

		//! Loads the Music BGMusic level from the pack.
		/*!
			\param file_path Path to the .sht file of the map
		*/
		void loadLevelBGMPack(const char* file_path, bool removeBGM = true);

		//! All channel with FX sound in loop will be stoped
		void stopAllLoops();

		//! Pauses all FX channels
		void pauseFX();

		//! Resume all FX channels
		void resumeFX();

		//! Pauses all BGM channels
		void pauseBGM();

		//! Resume all BGM channels
		void resumeBGM();


		// ! Sets Position X,Y of sound source for a channel
		/*!
			\param channel channel to fix source position
			\param posX X coordinate for source sound
			\param posY Y coordinate for source sound
		*/
		void setSourceChannel (int channel, Sint16 posX, Sint16 posY);


		// ! Sets Position X,Y where Main Character is.
		/*!
			\param posX X coordinate of Character
			\param posY Y coordinate of Character.
		*/
		void setCharacterPos (Sint16 posX, Sint16 posY);

#ifdef __RENDER_MIXER__
		void renderMixer(void);
#endif
		//! Get BGMusic filename
		std::string	getBGMFileName(unsigned char ID){return mBGMFileName[ID];};
		//! Get BGMusic channel
		int			getBGMChannel(unsigned char ID){return mBGMusic_Channel[ID];};
		//! Get BGMusic channel
		int			getBGMVolPlay(unsigned char ID){return mBGMusic_VolumePlay[ID];};
		//! Get BGMusic volume
		int			getBGMVolEnd(unsigned char ID){return mBGMusic_VolumeEnd[ID];};
		//! Get BGMusic name loaded
		std::string	getBGMMusicName(){return mBGMMusicName;};
		//! Get BGMusic bool mBGMusic_bIntroPlay
		bool		isBGMIntroPlay(){ return mBGMusic_bIntroPlay;};
		//! Get BGMusic bool mBGMusic_bIntroEnd
		bool		isBGMIntroEnd(){ return mBGMusic_bIntroEnd;};
		//! Get BGMusic bool mBGMusic_bMusicPlay
		bool		isBGMMusicPlay(){ return mBGMusic_bMusicPlay;};
		//! Get BGMusic bool mBGMusic_bMusicLoop
		bool		isBGMMusicLoop(){ return mBGMusic_bMusicLoop;};

		//! Set BGMusic channel
		void		setBGMChannel(unsigned char ID, int channel){mBGMusic_Channel[ID] = channel;};
		//! Set BGMusic volume
		void		setBGMVolPlay(unsigned char ID, int vol){mBGMusic_VolumePlay[ID] = vol;};
		//! Set BGMusic mBGMusic_bIntroEnd
		void		setBGMIntroEnd(bool status){mBGMusic_bIntroEnd = status;};
		//! Set BGMusic mBGMusic_bMusicPlay
		void		setBGMMusicPlay(bool status){mBGMusic_bMusicPlay = status;};
		//! Set BGMusic_IncVolumen
		void	setBGMusicIncVolumen(Uint32 time = 5){mBGMusic_IncVolumen = time;};
		//! Set BGMusic name  (for internal use in game)
		void	setBGMMusicName(std::string name){mBGMMusicName = name;};

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		std::string getClassName(){return "SoundManager";};

		//! Gets Singleton instance
		/*!
			\return Instance of SoundManager (Singleton).
		*/
		static SoundManager* getInstance();

		ChannelMix				mMixer[MAX_CHANNELSOUND];	/*!< Control of channels in Mixer: On, off, loop, what sound, etc. */

	protected:

		std::string				mBGMFileName[4];		/*!< ID strings to the sound BGM*/
		std::string				mBGMMusicName;			/*!< Actual BGM music loaded. NONE value if empty */
		int						mBGMusic_Channel[4];	/*!< Channels used on BGM Music*/
		int						mBGMusic_VolumePlay[4];	/*!< Volume Active for intro and 3 voices of BGM Music.*/
		int						mBGMusic_VolumeEnd[4];	/*!< Volume Target for intro and 3 voices of BGM Music. Used in Fade in future*/
		Uint32					mBGMusic_IncVolumen;	/*!< ms for change volume of any BGM music voice. Used in Fade in future*/
		Uint32					mBGMusic_TimeLastUpdate; /*!< Time enlapsed in current update */
		bool					mBGMusic_isFadeOut;		/*!< Marks if BGMusic is fadding to volume zero */

		bool					mBGMusic_bIntroPlay;	/*!< If true Intro BGM music is playing*/
		bool					mBGMusic_bIntroEnd;		/*!< If true Intro BGM music has finished and stop*/
		bool					mBGMusic_bMusicPlay;	/*!< If true 3 voices BGM music is playing*/
		bool					mBGMusic_bMusicLoop;	/*!< If true 2 voices BGM music must loop */

	private:
		//! Deletes a BGM sound from the SoundManager BGMmap
		/*!
			\param file ID string to the sound
		*/
		void removeBGM(const char* file);

		//! Deletes a FX sound from the SoundManager FXmap
		/*!
			\param file ID string to the sound
		*/
		void removeFX(const char* file);

		//! Check if FX or BGM sound is in any SoundManager map
		/*!
			\param file ID string to the sound
			\param type Maybe Soundtype BGM or FX
			\return true if sound is there.
		*/
		bool isLoaded(const char* file, SoundType type);
		
		std::map<std::string, Mix_Chunk*> mBGMMap;		/*!< Map that stores Chunks of BGM sounds. Useful for direct access*/
		std::map<std::string, Mix_Chunk*> mFXMap;		/*!< Map that stores Chunks of FX sounds. Useful for direct access*/

		Uint8					mBGMvolume;				/*!< General BGM volume. May be aplied all you change a BGM sound.*/
		Uint8					mFXvolume;				/*!< General FX volume. May be aplied all you change a FX sound.*/

		Sint16					mCharacterPosX;			/*!< Chracter position X in world to do changes in volume */
		Sint16					mCharacterPosY;			/*!< Chracter position Y in world to do changes in volume */

		static SoundManager*	pInstance;			   /*!<  Singleton instance*/
};

#endif

/*
 * USO:
 * Se plantea el uso de estas fuentes con texto en UTF-8
 * ver estas webs para su codificación y orden
 * http://www.fileformat.info/info/charset/UTF-8/encode.htm
 * http://es.wikipedia.org/wiki/UTF-8
 * Ver formato de codificación para entener como tratamos la cadena de texto.
 * Vamos a coger los 256 caracteres
 * EXCEPTO ! Los 32 primeros que son de control.
 * Total 256-32 = 224 Caracteres ha de tener nuestra fuente.
 * Unicode (U+0020h) - (U+00FFh)
 * El primer caracter es ESPACIO EN BLANCO
 */
#ifndef TEXTMANAGER_H
#define TEXTMANAGER_H

//c++
#include <vector>
#include <map>
//SDL
#include "includes.h"
//Propios de clase
//Propios genericos
#define NUM_LANGUAGES 5

#define TEXT_FOLDER "Assets/txt/"
#define FONTS_FOLDER "Assets/font/"

//#define USE_COMPILED_FONTS
//#define COMPILE_FONT_AFTER_LOAD

//! Letter Data for fonts
typedef struct{
	Sint32	imageFile;	/*!< ID image for letter */
	Uint32	PosX;		/*!< X coordinate for letter */
	Uint16	width;		/*!< Width for letter */
} Letter;

//! Letter Data for text
typedef struct{
	C_Rectangle	Pos;		/*!< x,y,w,h for letter in Text class*/
	Sint32		imageFile;	/*!< ID image for letter */
} LetterText;

//! Font Data Class
class FontGfx{
	public:
		FontGfx();
		~FontGfx();

		std::map<Uint16, Letter> character;	/*!< Map of Letters-symbols */
		Sint16	textHeight;		/*!<  Height of the font*/
		Sint8	maxVariants;	/*!<  Number of variations of the font*/
		Letter	number[10];		/*!<  Special optimization for numbers. No use of map */
};

//! TextManager class
/*!
	Handles the load of the written texts of the game.
*/
class TextManager {
	public:
		enum Language {ENG, SPA, CAT, JAP, KOR};

		//! Instantiation oh the TextManager
		TextManager();

		//! Destructor.
		~TextManager();

		//! Initializes the texts of the game
		/*!
			\param language Language of the game's texts
		*/
		void init(Language lang = ENG);

		//! Returns if texts are loaded
		bool isTextLoaded(){return mTextLoaded;};

		//! Gets the text from the TextMap.
		/*!
			\param tag ID to search the text
		*/
		const char* getText(std::string tag);

		//! Changes the language
		void changeLanguage(Language lang);

		//! Returns the language
		Language getLanguage(){return mTextLanguage;};

		//! Load font from a .fox file
		/*!
			\param file File path of the font type
			\param text_height Height of the text
		*/
		void loadFontType(const char* file, Uint16 text_height);

		//! Load font from a .foxxy file
		/*!
			\param file File path of the font type
		*/
		void loadCompiledFontType(const char* file);

		//! Saves font on a .foxxy file
		/*!
			\param file File path for the font type
			\param aFont FontGfx to save
		*/
		void compileFont(const char* file, FontGfx* aFont);

		//! Load font from a .fox file in the pack.
		/*!
			\param file File path of the font type
			\param text_height Height of the text
		*/
		void loadFontTypePack(const char* file, Uint16 text_height);

		//! Returns a font type given an index
		FontGfx* getFontType(Uint16 index){
			Uint16 id = index;
			if(id >= FontType.size()){id = FontType.size()-1;}
			return FontType[index];
		};

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		std::string getClassName(){return "TextManager";};

		
		//! Gets Singleton instance
		/*!
			\return Instance of TextManager (Singleton).
		*/
		static TextManager* getInstance();

	protected:
		void loadAllFonts();

		//! Loads text from a file
		/*!
			\param file File path of the game texts
		*/
		void loadTextMap(const char* file);

		//! Loads text from a file in the pack
		/*!
			\param file File path of the game texts
		*/
		void loadTextMapPack(const char* file);

		//Text Tag for game
		std::map<std::string, std::string>  mTextMap;	/*!<  Contains all the texts of the game*/	
		bool				mTextLoaded;				/*!<  True if the TextMap has been loaded*/
		Language			mTextLanguage;				/*!<  Language of the text*/

		//Fonts definition and containers
		std::vector<FontGfx*>	FontType;		/*!< All fonts in game.*/
		static TextManager*		pInstance;		/*!< Singleton instance*/
		bool					mFontsLoaded;	/*!< If fonts have been loaded yet*/
};

#endif /* TEXT_H_ */

#include "singletons.h"

//Include our classes
#include "SceneMenu.h"

SceneMenu::SceneMenu(): Scene(){ //Calls constructor in class Scene
}

SceneMenu::~SceneMenu(){
}

void SceneMenu::init(){
	//sTxtManager->changeLanguage(TextManager::SPA);
	Scene::init(); //Calls to the init method in class Scene
	mpGraphicID = sResManager->getGraphicID("Assets/gfx/test.png");
	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = sResManager->getGraphicWidth(mpGraphicID);
	mpGraphicRect.h = sResManager->getGraphicHeight(mpGraphicID);

	mpText1 = new Text(0, 0);
	mpText1->setString(0,0, "HOLA");
	//mpText1->setKey(0, 0, "SAMPLE");

	sSoundManager->addSound("Assets/snd/jingle.ogg", FX); //A�adir sonido //BGM (banda sonora)
}

void SceneMenu::load(){
	Scene::load(); //Calls to the init method in class Scene
	mChnSnd = sSoundManager->playFX("Assets/snd/jingle.ogg", 0); //-1 infinito
}

void SceneMenu::updateScene(){
	inputEvent();
}

void SceneMenu::drawScene(){
	sFxGfx->rectFill(50, 50, 100, 100, 0xFF0000);
	imgRender(mpGraphicID, 200, 300, mpGraphicRect, 255);
	mpText1->render(30, 30, 255, false);
}

//-------------------------------------------
//				   INPUT
//-------------------------------------------
void SceneMenu::inputEvent(){
	if (sInputControl->getAcceptSignal()) { //Space
		sDirector->changeScene(SceneDirector::LEVEL);
		//sSoundManager ->stopChn(mChnSnd)
	}
	int keysReleased = sInputControl->getButtonsRelease();
	//sInputControl->getBackSignal() BOTON_2 BACKSPACE O ESCAPE
	if (keysReleased & BUTTON_1) {//Space
		sDirector->changeScene(SceneDirector::FIGHT);
	}

}

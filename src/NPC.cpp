//Include our classes
#include "NPC.h"
#include "singletons.h"


NPC::NPC() : Entity(){
	mpSpeed = 300;
	mpDirection = DOWN;
}

NPC::~NPC(){
}

void NPC::init(){
	Entity::init();
}
void NPC::init(int x, int y){
	Entity::init(x, y);
}
void NPC::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);
}

void NPC::render() {
	Entity::render();
}

void NPC::update() {
	Entity::update();
}

void NPC::updateControls() {
	if (mTurn) {
		std::cout << mpDirection << std::endl;
		if (mpDirection == UP) {
			mpDirection = DOWN;
		}
		else {
			mpDirection = UP;
		}
		std::cout << mpDirection << std::endl;
		mTurn = false;
	}


	/*if (checkCollisionWithMap() == true) {
		mpDirection = UP;
	}
	else {
		mpDirection = DOWN;
		mpRandom = 0;
	}*/
}

bool NPC::isOfClass(std::string classType){
	if(classType == "NPC" || 
		classType == "Entity"){
		return true;
	}
	return false;
}


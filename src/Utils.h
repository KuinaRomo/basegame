#ifndef UTILS_H
#define UTILS_H

#include "includes.h"
#include <algorithm>

#define PI 3.14159265f
#define PI2RAD  (3.14159265f/180.0f)
#define MAX_DIGITS_U32 10

//! Colision entre dos rect�ngulos
bool C_RectangleCollision(C_Rectangle RectA, C_Rectangle RectB);

bool C_RectangleTouch(C_Rectangle RectA, C_Rectangle RectB);

//! Colision entre un rect�ngulo y un tri�ngulo
bool RectangleTriangleCollision(C_Rectangle a_rect, C_Triangle a_tri);

//! Devuelve true si el punto est� dentro del rect�ngulo
bool isPointInRectangle(Point a_point, C_Rectangle a_rect);

//! Devuelve true si el punto est� dentro del tri�ngulo
bool isPointInTriangle(Point a_point, C_Triangle a_tri);

//! Given three colinear points p, q, r, the function checks if point q lies on line segment 'pr'
bool onSegment(Point p, Point q, Point r);

/*!To find orientation of ordered triplet (p, q, r).
 The function returns following values
 0 --> p, q and r are colinear
 1 --> Clockwise
 2 --> Counterclockwise
 */
int orientation(Point p, Point q, Point r);

//! The main function that returns true if line segment 'p1q1' and 'p2q2' intersect.
bool doIntersect(Point p1, Point q1, Point p2, Point q2);

//Distancia entre dos puntos
bool PointsAtDistance(Sint16 x1, Sint16 y1, Sint16 x2, Sint16 y2, Uint32 distance);
//Longitud de un n�mero entero
Uint32 integerLength(Uint32 value);
//! Checks if a point in a matrix belongs to the respective ellipse curve. Chequea si un punto pertenece a una elipse de tama�o halfWidth*2 x halfHeight*2, dentro del primer cuadrante.
/*!
	\return True or False if the animation is one-cycle and has ended.
	\param pointX Horizontal cell.
	\param pointY Vertical cell.
	\param halfHeight Vertical dimension of the matrix.
	\param halfWidth Horizontal dimension of the matrix.
*/
bool checkEllipsePoint(Uint32 pointX, Uint32 pointY, Uint32 halfHeight, Uint32 halfWidth);

//! Clamps an integer to the range specified with [a,b]
Sint32 Sint32clamp(Sint32 v, Sint32 a, Sint32 b);

//! Returns a point rotated after a pivot
/*!
	\return True or False if the animation is one-cycle and has ended.
	\param pivot Point of reference.
	\param a_point Point to rotate.
	\param angle Angle of rotation (in rad)
*/
Point rotatePoint(Point pivot, Point a_point, float angle);

//! Converts Integer to String
std::string itos(Uint32 number, Uint16 num_digits);

bool findInString(std::string& aString, std::string to_find);

//! Replaces first occurrence of a substring in a string
void replaceString(std::string& aString, std::string replaced, std::string replacing);

//! Replaces all occurrences of a substring in a string
void replaceAllInString(std::string& aString, std::string replaced, std::string replacing);

//! Splits a string given a character for separation
std::vector<std::string> splitString(std::string aString, char separationCharacter);

bool isStringInVector(std::vector<std::string> &aVector, std::string aString);

bool findPointInVector(Point aPoint, std::vector<Point> &aVector);
bool findPointInVector(Point aPoint, std::vector<Point> &aVector, int &index);

//! Deletes the file in this path
void deleteFile(const char* file_path);

//! Converts Texture absolute coordinates [0, WIDTH] to OpenGL [0,1]
float TextureToOGLX(Sint16 x_coord, Uint16 text_dim = 0);
float TextureToOGLY(Sint16 y_coord, Uint16 text_dim = 0);

//! Converts Screen absolute coordinates [0, WIDTH] to OpenGL [-1,1]
float ScreenToOGLX(Sint16 x_coord, Uint16 screen_dim = SCREEN_WIDTH);
float ScreenToOGLY(Sint16 y_coord, Uint16 screen_dim = SCREEN_HEIGHT);

//! Converts OpenGL coordinates [-1,1] to screen absolute
Sint16 OGLToScreenX(float x_ogl, Uint16 screen_dim = SCREEN_WIDTH);
Sint16 OGLToScreenY(float y_ogl, Uint16 screen_dim = SCREEN_HEIGHT);

int min(int num1, int num2);
int max(int num1, int num2);
#endif

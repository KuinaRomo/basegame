#ifndef TEXT_H
#define TEXT_H

//c++
#include <vector>
#include <map>
//SDL
#include "includes.h"
#include "TextManager.h"
//Propios de clase
//Propios genericos

#define FONT_SHADOW_WIDTH 57
#define FONT_SHADOW_HEIGHT 37
#define FONT_SHADOW_TIP_W 22

//! Text class
/*!
	Handles the load and display of the written texts of the game.
*/
class Text {
	public:
		//! Enum TextAlign.
		/*! Align of the text */
		enum TextAlign {TEXT_LEFT,TEXT_RIGHT,TEXT_CENTER};

		enum TextSpeed {VERY_SLOW, SLOW, NORMAL, FAST, VERY_FAST};

		//! Constructor of a basic text.
		/*!
			\param numFont Font number to use for this text
			\param variant "Color/Variation" of the font
		*/
		Text (Uint16 numFont, Uint8 variant = 0);

		//! Destructor.
		~Text();

		//! Sets a text string to a position of the screen
		/*!
			\param x X Coordinate 
			\param y Y Coordinate
			\param tag Tag from text to display. This tag must be in file loaded
			\param align Align of the text
		*/
		void setKey (Sint16 x, Sint16 y, std::string tag, TextAlign align = TEXT_LEFT);
		
		//! Sets a text string to a position of the screen
		/*!
			\param x X Coordinate 
			\param y Y Coordinate
			\param tag Tag from text to display. This tag must be in file loaded
			\param align Align of the text
		*/
		void setString (Sint16 x, Sint16 y, std::string text, TextAlign align = TEXT_LEFT);
		
		//! Sets a text string to a position of the screen
		/*!
			\param append String to be append to tag. Useful for display variables.
		*/
		void appendString(Sint16 x, Sint16 y, std::string append);

		//! Decode UTF-8 and Push Letters in Text Class
		void decodeUTF8andPushLetters();

		//! Sets the text position on the screen
		/*!
			\param x X Coordinate 
			\param y Y Coordinate
			\param align Align of the text
		*/
		void setXY (Sint16 x, Sint16 y, TextAlign align = TEXT_LEFT);

		//! Sets the text X position on the screen
		/*!
			\param x X Coordinate
			\param align Align of the text
		*/
		void setX (Sint16 x, TextAlign align = TEXT_LEFT);

		//! Sets the text Y position on the screen
		/*!
			\param y Y Coordinate
		*/
		void setY (Sint16 y);

		//! Returns X position
		Sint16 getX(){return mPosDraw.x;};

		//! Returns Y position
		Sint16 getY(){return mPosDraw.y;};

		//! Returns Width of the text in pixels
		Uint16 getW(){return mPosDraw.w;};

		TextAlign getAlign(){return mAlign;};

		//! Changes the color or variation of the font
		/*!
			\param variant "Color/Variation" of the text
		*/
		void setVariant(Uint8 variant = 0);
		
		//! Updates the font
		void update();

		//! Renders the font
		void render(Uint8 alpha = 255, bool shadow = false);

		//! Renders the font at a specified point (not recommended)
		void render(Sint16 x, Sint16 y, Uint8 alpha = 255, bool shadow = false);

		//! Returns the height in pixels of the font
		/*!
			\return Returns height (Uint16)
		*/
		Uint16 getFontHeight(){return mFont->textHeight;};

		//! Sets the text to appear
		void setAnimation(bool anim);

		//! Begins animation
		void beginAnimation(){mAnimInCourse = true;};

		//! Stops animation
		void stopAnimation(){mAnimInCourse = false;};

		//! Returns if the animation has finished
		bool hasAnimationFinished();

		//! Sets the animation time for next character
		void setAnimationTime(Uint16 time){mNextFrameTime = time;};

		void setAnimationTime(TextSpeed speed);

		//! Resets the text (useful when changing language)
		void resetText();

		std::string getKey(){return mTextKey;};
		Uint16 getNumFont(){return mFontNum;};
		Uint8 getVariant(){return mVariant;};

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		std::string getClassName(){return "Text";};

	protected:
		FontGfx*				mFont;			/*!<  Pointer to the font to use*/
		Sint16					mLastCharToRender;/*!< Last char to render of text*/

		bool					mAnimation;		/*!<  Is animated*/
		bool					mAnimInCourse;	/*!<  Animation in course*/

		Uint16					mCurrentTime;	/*!<  Current time of the char*/
		Uint16					mNextFrameTime;	/*!<  Time for the next char*/

		Sint16					mTextPosX;
		Sint16					mTextPosY;

		Uint16					mFontNum;		/*!<  Number of the font*/
		Uint8					mVariant;		/*!<  Variation or color of the font*/
		std::string				mTextKey;		/*!<  Key of the string to display (if any)*/
		std::string				mTextSt;		/*!<  String to display*/
		TextAlign 				mAlign;			/*!<  Align of the text*/
		C_Rectangle				mPosDraw;		/*!<  Position of the text*/
		std::vector<LetterText>	mPosLetter;		/*!<  Position of each character in the font graphic*/

		static Sint32			mFontShadow;	/*!<  Shadow for the first font only*/
		static C_Rectangle		mShadowRect;	/*!<  Rectangle for the shadow*/
};

#endif /* TEXT_H_ */

#include "singletons.h"

#include "Video.h"

Video* Video::instance = NULL;

Video* Video::getInstance()
{
	if (!instance) {
		instance = new Video();
	}
	return instance;
}

Video::Video()
{
	mFullScreen = false;	// Window mode by default
	mTarget = DEFAULT;
	//Start SDL 
	SDL_Init( SDL_INIT_EVERYTHING );

	SDL_WM_SetCaption("Base Game Engine",NULL);

	//Setup screen
	mScreen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, BPP, SDL_HWSURFACE | SDL_DOUBLEBUF);// | SDL_FULLSCREEN);

	SDL_ShowCursor(false);	
}

Video::~Video(){
	close();
}

void Video::renderGraphic(Uint32 img, C_Rectangle rect, Sint16 posX, Sint16 posY){
	SDL_Rect r, rectAux;

	r.x = posX;
	r.y = posY;

	rectAux.h = rect.h;
	rectAux.w = rect.w;
	rectAux.x = rect.x;
	rectAux.y = rect.y;
	SDL_BlitSurface(sResManager->getGraphicByID(img), &rectAux, mScreen, &r);
}

void Video::renderGraphic(Uint32 img, C_Rectangle rect, Sint16 posX, Sint16 posY, Uint8 alpha){
	SDL_Rect r, rectAux;

	r.x = posX;
	r.y = posY;

	rectAux.h = rect.h;
	rectAux.w = rect.w;
	rectAux.x = rect.x;
	rectAux.y = rect.y;

	SDL_Surface *origin = sResManager->getGraphicByID(img);
	// Caso conColorKey cubierto, siempre que no sea el color Key el 0x000000
	if (origin->format->colorkey){
		SDL_SetAlpha(origin, SDL_SRCALPHA , alpha);
		SDL_BlitSurface(origin, &rectAux, mScreen, &r);
		SDL_SetAlpha(origin, SDL_SRCALPHA , 255);
	}else{
		Uint32 rmask, gmask, bmask, amask;
		Uint8 offpixel;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
		rmask = 0xff000000;
		gmask = 0x00ff0000;
		bmask = 0x0000ff00;
		amask = 0x000000ff;
		offpixel = 0;
#else
		rmask = 0x000000ff;
		gmask = 0x0000ff00;
		bmask = 0x00ff0000;
		amask = 0xff000000;
		offpixel = 3;
#endif
		// Duplicamos superficie del tama�o a tratar
		// Creaci�n
		SDL_Surface *sfc = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, rectAux.w, rectAux.h, 32, rmask, gmask, bmask, amask);
		// Quitamos el bit de Alpha
		SDL_SetAlpha(origin, 0, origin->format->alpha);
		// Copiamos
		SDL_BlitSurface(origin, &rectAux, sfc, NULL);
		// Restauramos los bits Alpha a la copia y a la original
		SDL_SetAlpha(sfc, SDL_SRCALPHA , sfc->format->alpha);
		SDL_SetAlpha(origin, SDL_SRCALPHA , 255);
		// Tramos la nueva superficie para aplicarle este nuevo Alpha "a mano y al pixel"
		if(SDL_MUSTLOCK(sfc)){
			if(SDL_LockSurface(sfc) < 0) 
			{
				SDL_FreeSurface(sfc);
				return;
			}
		}
		Uint8 * pixels = (Uint8 *)sfc->pixels;
		Uint32 pitch_padding = (sfc->pitch - (4 * sfc->w));
		pixels += offpixel; 
		float alphaF = alpha/255.0f;
		for(int row = 0; row < sfc->h; ++row){
			for(int col = 0; col < sfc->w; ++col){
				if (*pixels > 0){
					*pixels = static_cast<Uint8>((*pixels) * alphaF);
				}
				pixels += 4;
			}
			pixels += pitch_padding;
		}
		if(SDL_MUSTLOCK(sfc)){
			SDL_UnlockSurface(sfc);
		}
		//volcamos la superficie a pantalla
		SDL_BlitSurface(sfc, NULL, mScreen, &r);
		SDL_FreeSurface(sfc);
	}
}

void Video::getScreenRegion(Uint32 img, C_Rectangle rect, Sint16 posX, Sint16 posY){
	SDL_Rect r, rectAux;

	r.x = posX;
	r.y = posY;
	r.w = rect.w;
	r.h = rect.h;

	rectAux.x = 0;
	rectAux.y = 0;

	SDL_BlitSurface(mScreen, &r, sResManager->getGraphicByID(img), &rectAux);
}

void Video::clearScreen( Uint32 color_key){
	// ColorKey en ARGB format A se ignora of course
	Uint8 R,G,B;
	R = (color_key>>16) & 255;
	G = (color_key>>8) & 255;
	B = color_key & 255;
	SDL_FillRect( mScreen,&mScreen->clip_rect,SDL_MapRGB(mScreen->format,R,G,B) );
}

void Video::updateScreen(){
	SDL_Flip(mScreen);
}

void Video::waitTime(Uint32 ms){
	SDL_Delay(ms);
}

void Video::close()
{
	//Deallocate surface
	SDL_FreeSurface( mScreen );
	mScreen = NULL;

	//Quit SDL subsystems
	SDL_Quit();
}

void Video::setFullscreen(bool fullscreen){
#if defined(__WIIU__) || defined (__3DS__)
#else
//-----------------------------------------
	if(fullscreen){
	#ifndef __PUBLICRELEASE__
		std::cout<<"FULLSCREEN"<<std::endl;
	#endif
		mScreen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, BPP, SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_FULLSCREEN);
		mFullScreen = true;
	}else{
	#ifndef __PUBLICRELEASE__
		std::cout<<"WINDOWED"<<std::endl;
	#endif
		mScreen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, BPP, SDL_HWSURFACE | SDL_DOUBLEBUF);// | SDL_FULLSCREEN);
		mFullScreen = false;
	}
//-----------------------------------------
#endif
}

void Video::lockScreen(){
	SDL_LockSurface(mScreen);
};
void Video::unlockScreen(){
	SDL_UnlockSurface(mScreen);
};

void Video::setRenderTarget(RenderTarget target){
#ifdef __WIIU__
	mTarget = target;
#else 
	#ifdef __3DS__
		if(target == MAIN_SCREEN || target == SCREEN_ONLY){
			mTarget = target;
		}else{
			mTarget = MAIN_SCREEN;
		}
	#else
		#ifdef SIMULATE_TWO_SCREENS
			if(target == MAIN_SCREEN || target == SUBSCREEN_ONLY){
				mTarget = target;
			}else{
				mTarget = MIRROR;
			}
		#else
			mTarget = DEFAULT;
		#endif
	#endif
#endif
}

void Video::openScreen(){
}

void Video::openSubScreen(){
}

void Video::closeScreen(){
}

void Video::closeSubScreen(){
}

void Video::copyScreenToSubScreen(){

}
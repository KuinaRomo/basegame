#ifndef SCENEFIGHT_H
#define SCENEFIGHT_H

#include "Scene.h"
#include "FightEntity.h"
#include "Player.h"
#include "Dante.h"
#include "Spartan.h"

class SceneFight : public Scene
{
	public:
		SceneFight();
		~SceneFight();

		void updateScene();
		void drawScene();

		//! Initializes the Scene.
		virtual void init();

		bool isOfClass(std::string classType);
		std::string getClassName(){return "SceneFight";};

				
	protected:

		//ofTrueTypeFont font;
		Text* mInfoPlayer;
		Text* mInfoPlayer2;
		//!La altura donde se ponen los nombres y vida de los personajes. Se usa en un bucle en el render para dibujar la información
		int mHigh;
		//!Turno que toca. Si hay alguien en esta posicion en el entities, si es player se abre el menu. Si es enemy se ataca
		int mTurn;
		//!Controla el espacio donde se coloca una EntityFight cuando se carga su barra de carga
		int mSpace;
		
		//!Comprueba si hay algun menu abierto. Si no lo hay abre el siguiente. Si es un enemy comprueba si ya ha atacado.
		bool mOpenedMenu;
		
		//!Vector que contiene todos los players
		std::vector <FightEntity*> mPlayers;
		//!Vector que contiene todas las entities y es la que controla el orden en el que han de atacar.
		std::vector <FightEntity*> mEntities;
		//!Variable temporal para meter los player en el arrays.
		FightEntity* a_player;
	
	private:
	
};

#endif

#ifndef SPARTAN_H
#define SPARTAN_H

#include "includes.h"
#include "PlayerFight.h"

class Spartan : public PlayerFight
{
	public:
		Spartan();
		~Spartan();

		void update();
		void render();

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Spartan";};



	protected:
		
	
	private:
	
};

#endif

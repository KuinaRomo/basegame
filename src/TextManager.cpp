//c++
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

//SDL
//Propios de clase
#include "TextManager.h"
//Propios genericos
#include "singletons.h"
#include "FileManager.h"
#include "Utils.h"

// Init for FontGfx object. Just only data
FontGfx::FontGfx() {
	textHeight = -1;
	maxVariants = -1;
}
// Destructor for FontGfx object
FontGfx::~FontGfx() {
}


TextManager* TextManager::pInstance = NULL;

TextManager* TextManager::getInstance(){
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new TextManager();
	}
	return pInstance;
}

TextManager::TextManager() {
	mTextLanguage = ENG;
	mTextLoaded = false;
	mFontsLoaded = false;
}

TextManager::~TextManager() {
}

void TextManager::init(Language lang){
	if(!mFontsLoaded){
		loadAllFonts();
	}
	if (mTextLoaded){
		return;	//Si ya están cargados los textos NO se recargan.
	}
	std::string text_file_path = TEXT_FOLDER;
	std::string aux_file_path = text_file_path;
	aux_file_path.append("text_files.txt");
	switch(lang){
		case ENG:
			text_file_path.append("eng");
			break;
		case SPA:
			text_file_path.append("spa");
			break;
		case CAT:
			text_file_path.append("cat");
			break;
		case JAP:
			text_file_path.append("jap");
			break;
		default:
			std::cout<<"ERROR: Language not supported"<<std::endl;
			return;
			break;
	}
	text_file_path.append("/");

	FileManager File (aux_file_path.c_str(), "r");
	std::vector<std::string> files_to_read;
	std::string line;
	Uint16 totalFiles = 0;
	if (File.isOpened()){
		while ( !File.isEOF() ){
			line = File.getLine();
			if(line[0] != '#'){
				files_to_read.push_back(line);
				totalFiles++;
			}
		}
	}else{
		return;
	}
	File.close();

	for(Uint16 i = 0; i < totalFiles; i++){
		aux_file_path = text_file_path;
		aux_file_path.append(files_to_read[i]);
		loadTextMap(aux_file_path.c_str());
	}
	
	mTextLanguage = lang;
	mTextLoaded = true;
}

void TextManager::loadTextMap(const char* file_path){
#if defined (__PUBLICRELEASE__)
	loadTextMapPack(file_path);
#else
	std::string line, tag, sentence;
	FileManager File (file_path, "r");
	size_t CharBreak;

	if (File.isOpened()){
		while ( !File.isEOF() ){
			line = File.getLine();
			if(line[0] != '#'){
				CharBreak = line.rfind("\t");
				tag = line.substr(0,CharBreak);
				sentence = line.substr(CharBreak+1);
				mTextMap.insert(std::pair<std::string, std::string>(tag, sentence));
			}
		}
		File.close();
	}else{
		std::cerr << "Text file not found: " << file_path << " Searching in the pack. . ." << std::endl;
		loadTextMapPack(file_path);
	}
#endif
}

void TextManager::loadTextMapPack(const char* file_path){
}

const char* TextManager::getText (std::string tag){
	std::map<std::string,std::string>::iterator iTag = mTextMap.find(tag);
	if(iTag == mTextMap.end()){
		std::cerr << "Error while retrieving " << tag <<" from text"<< std::endl;
		return "[!]";
		//return "";
	}else{
		return iTag->second.data();
	}
}

void TextManager::changeLanguage (Language lang){
	if (mTextLoaded){
		mTextLoaded = false;
		mTextMap.clear();
		init(lang);
	}
}

void TextManager::loadAllFonts(){
	std::string font_file_path = FONTS_FOLDER;
	std::string aux_file_path = font_file_path;

	aux_file_path.append("fonts.txt");

	FileManager File (aux_file_path.c_str(), "r");
	std::vector<std::string> files_to_read;
	std::vector<Uint16>	font_heights;
	std::string line;
	Uint16 totalFiles = 0;
	if (File.isOpened()){
		while ( !File.isEOF() ){
			line = File.getLine();
			if(line[0] != '#'){
				std::vector<std::string> subStrings = splitString(line, '\t');
				aux_file_path = font_file_path;
				aux_file_path.append(subStrings[0]);
				files_to_read.push_back(aux_file_path);
				font_heights.push_back(std::atoi(subStrings[1].c_str()));
				totalFiles++;
			}
		}
	}else{
		return;
	}
	File.close();

	for(Uint16 i = 0; i < totalFiles; i++){
		aux_file_path = font_file_path;
		aux_file_path.append(files_to_read[i]);
		#ifdef USE_COMPILED_FONTS
			loadCompiledFontType(files_to_read[i].c_str());
		#else
			loadFontType(files_to_read[i].c_str(), font_heights[i]);
		#endif
	}

	mFontsLoaded = true;
}

void TextManager::loadFontType(const char* file_path, Uint16 text_height){
#ifdef __PUBLICRELEASE__
	loadFontTypePack(file_path, text_height);
#else
	std::string line;
	FileManager File (file_path, "r");
	size_t CharBreak;
	Sint32	imageGfx = -1;
	Uint32 colorKeyChar;
	Uint32 colorKeyGfx;
	Uint32 numCharGfx;

	FontGfx*	tempFont = new FontGfx();
	std::vector<int>	coorXGfx;
	Uint32	totalLetters = 0;
	Uint32	lineLetter = 0;
	tempFont->textHeight = text_height;

	if (File.isOpened()){
		while ( !File.isEOF() ){
			line = File.getLine();
			std::string dash = line.substr(0,2);
			if (dash == "##"){						// new gfx to load
				std::string file, fileGfx;
				file = file_path;
				CharBreak = file.rfind("/");							// Gets path form file_path
				fileGfx = file.substr(0,CharBreak+1);
				fileGfx.append(line.substr(2));							// Adds file GFX
				imageGfx = sResManager->getGraphicID(fileGfx.c_str());	// Load image
				int width, height;
				sResManager->getGraphicSize(imageGfx,width,height);
				Uint32 pixelWidth = width;
				if (tempFont->maxVariants == -1){						// Just only first GFX fix Variants
					tempFont->maxVariants = (height-1) / text_height;
				}
				// El Primer pixel me da el color de las separaciones.
				// El Segundo pixel me da el colorkey(=colortransparente) de toda la imagen.
				sFxGfx->lockSurface(imageGfx);
				colorKeyChar = sFxGfx->getPixel(0,0,imageGfx);
				colorKeyGfx = sFxGfx->getPixel(1,0,imageGfx);

				// Vamos a contar cuantas letras hay y en que coordenadas empieza cada una;
				coorXGfx.clear();
				for (Uint32 i=0 ; i < pixelWidth;i++){
					if (colorKeyChar == sFxGfx->getPixel(i,0,imageGfx)){
						coorXGfx.push_back(i);
					}
				}
				numCharGfx = coorXGfx.size()-1;	// Last point isn't a letter 
				sFxGfx->unlockSurface(imageGfx);
				// Marcamos la imagen con su color transparente
				sResManager->setColorKey(imageGfx,colorKeyGfx);
				totalLetters = 0;
			}
			else{		//line with characters
				Uint32 bytesPerLine = line.size();
				lineLetter = 0;
				for (Uint16 i=0; i<bytesPerLine ; i++){
					Uint16	Index = 0;
					Uint8 Caracter;
					Caracter = line[i];
					if (Caracter < 0x80){ // Hasta (U+0080h) se trata igual que un ASCII convencional
						Index = Caracter;//-0x20; // Empieza en el ESPACIO en BLANCO (U+0020h)
					}else {
						if ((Caracter&0xF0) == 0xc0){ // Estamos en codificación a 2 bytes.
							if ( ((Caracter&0x1f) == 2) || ((Caracter&0x1f) == 3) ) { // Solo hasta 255. El resto no tendria representación en la fuente.
								Uint8 Caracter2 = line[i+1];
								Index = (((Caracter & 0x03) << 6) | (Caracter2 & 0x3F));// - 0x20; // Empieza en el ESPACIO en BLANCO (U+0020h)
								i++;
							}
							else {
								Index = 0; //Si es de otro rango aparece un espacio en blanco.
								i++;
							}
						}
						if ((Caracter&0xF0) == 0xE0){ // Estamos en codificación a 3 bytes.
							Uint8 Caracter2 = line[i+1];
							Uint8 Caracter3 = line[i+2];
							Index = (Caracter&0x0F)<<12 | (Caracter2&0x3F)<<6 | (Caracter3&0x3F);
							//Index -=0x3040;
							i+=2;
						}
					}
					Letter	tempLetter;
					tempLetter.imageFile = imageGfx;
					tempLetter.PosX = coorXGfx[totalLetters+lineLetter];
					tempLetter.width = coorXGfx[totalLetters+lineLetter+1] - coorXGfx[totalLetters+lineLetter];
					lineLetter ++;
					tempFont->character.insert(std::pair<Uint16, Letter>(Index,tempLetter));
					if ((Index < 0x3A) && (Index > 0x2F)){	// Special Letter data for numbers. Duplicate Data.
						tempFont->number[Index-0x30] = tempLetter;
					}
				}
				totalLetters += lineLetter;
			}
		}
		File.close();
		FontType.push_back(tempFont);
#ifdef COMPILE_FONT_AFTER_LOAD
		std::string filePath = file_path;
		filePath.append("xy"); //.fox to .foxxy
		compileFont(filePath.c_str(), tempFont);
#endif
	}else{
		std::cerr << "Font file not found: " << file_path << " Searching in the pack. . ." << std::endl;
		loadFontTypePack(file_path, text_height);
	}
#endif
}

Sint16 getVectorIndex(std::vector<std::string> &aVector, std::string aString){
	Uint16 size = aVector.size();
	for(Uint16 i = 0; i < size; i++){
		if(aVector[i] == aString){
			return i;
		}
	}
	return -1;
}

void TextManager::compileFont(const char* file, FontGfx* aFont){
	FileManager savefile(file, "wb");
	if(!savefile.isOpened()){
		#ifndef __PUBLICRELEASE__
			std::cout<<"ERROR CREATING FILE " << file << "."<<std::endl;
		#endif
		return;
	}

	std::vector<std::string> filepaths; //Guardamos los paths a parte
	for(Uint16 i = 0; i < 10; i++){		//Primero los números
		Sint32 ID = aFont->number[i].imageFile;
		std::string path = sResManager->getGraphicPathByID(ID);
		if(path != "NULL"){
			if(!isStringInVector(filepaths, path)){
				filepaths.push_back(path);
			}
		}
	}
	//Luego el resto de letras
	for(std::map<Uint16, Letter>::iterator it= aFont->character.begin(); 
									it!=aFont->character.end(); ++it){
		Letter aLetter = it->second;
		Sint32 ID = aLetter.imageFile;
		std::string path = sResManager->getGraphicPathByID(ID);
		if(path != "NULL"){
			if(!isStringInVector(filepaths, path)){
				filepaths.push_back(path);
			}
		}
	}

	Uint16 numberOfElements = aFont->character.size(); //Importante, o no sabemos cuantos hay
	savefile.write(&numberOfElements,sizeof(numberOfElements),1);
	//Guardamos variantes y altura de la fuente
	savefile.write(&aFont->textHeight,sizeof(aFont->textHeight),1);
	savefile.write(&aFont->maxVariants,sizeof(aFont->maxVariants),1);

	//Guardamos los filepaths
	numberOfElements = filepaths.size();
	savefile.write(&numberOfElements,sizeof(numberOfElements),1);
	for(Uint16 i = 0; i < numberOfElements; i++){
		Uint16 pathSize = filepaths[i].size();
		savefile.write(&pathSize,sizeof(pathSize),1);
		for(Uint16 j = 0; j < pathSize; j++){
			savefile.write(&filepaths[i][j],sizeof(filepaths[i][j]),1);
		}
	}

	//Guardamos los números que van a parte
	for(Uint16 i = 0; i < 10; i++){
		Letter aLetter = aFont->number[i];
		std::string path = sResManager->getGraphicPathByID(aLetter.imageFile);
		Sint16 index = getVectorIndex(filepaths, path);
		savefile.write(&index,sizeof(index),1);
		savefile.write(&aLetter.PosX,sizeof(aLetter.PosX),1);
		savefile.write(&aLetter.width,sizeof(aLetter.width),1);
	}
	//Guardamos los caracteres de dentro del map
	for (std::map<Uint16, Letter>::iterator it= aFont->character.begin(); 
		it!=aFont->character.end(); ++it){
			Uint16 position = it->first;
			savefile.write(&position,sizeof(position),1);
			Letter aLetter = it->second;
			std::string path = sResManager->getGraphicPathByID(aLetter.imageFile);
			Sint16 index = getVectorIndex(filepaths, path);
			savefile.write(&index,sizeof(index),1);
			savefile.write(&aLetter.PosX,sizeof(aLetter.PosX),1);
			savefile.write(&aLetter.width,sizeof(aLetter.width),1);
	}
	savefile.close();
#ifndef __PUBLICRELEASE__
	std::cout<<"Font successfully compiled to "<<file<<std::endl;
#endif
}

void TextManager::loadCompiledFontType(const char* file){
	FileManager savefile(file, "rb");
	if(!savefile.isOpened()){
		#ifndef __PUBLICRELEASE__
			std::cout<<"ERROR OPENING FILE " << file << "."<<std::endl;
		#endif
		return;
	}
	FontGfx* aFont = new FontGfx();
	std::vector<std::string> filepaths; //Leemos los paths a parte
	
	Uint16 numberOfElements; //Importante, o no sabemos cuantos hay
	savefile.read(&numberOfElements,sizeof(numberOfElements),1);
	//Leemos variantes y altura de la fuente
	savefile.read(&aFont->textHeight,sizeof(aFont->textHeight),1);
	savefile.read(&aFont->maxVariants,sizeof(aFont->maxVariants),1);

	//Leemos los filepaths
	Uint16 numberOfFilePaths;
	savefile.read(&numberOfFilePaths,sizeof(numberOfFilePaths),1);
	filepaths.resize(numberOfFilePaths);
	for(Uint16 i = 0; i < numberOfFilePaths; i++){
		Uint16 pathSize;
		savefile.read(&pathSize,sizeof(pathSize),1);
		filepaths[i].resize(pathSize);
		for(Uint16 j = 0; j < pathSize; j++){
			savefile.read(&filepaths[i][j],sizeof(filepaths[i][j]),1);
		}
	}

	//Leemps los números que van a parte
	for(Uint16 i = 0; i < 10; i++){
		Letter aLetter;
		Sint16 index;
		savefile.read(&index,sizeof(index),1);
		aLetter.imageFile = -1;
		if(index > -1){
			aLetter.imageFile = sResManager->getGraphicID(filepaths[index].c_str());
		}
		savefile.read(&aLetter.PosX,sizeof(aLetter.PosX),1);
		savefile.read(&aLetter.width,sizeof(aLetter.width),1);

		aFont->number[i] = aLetter;
	}
	//Leemos los caracteres de dentro del map
	for(Uint16 i = 0; i < numberOfElements; i++){
		Uint16 position;
		savefile.read(&position,sizeof(position),1);
		Letter aLetter;
		Sint16 index;
		savefile.read(&index,sizeof(index),1);
		aLetter.imageFile = -1;
		if(index > -1){
			aLetter.imageFile = sResManager->getGraphicID(filepaths[index].c_str());
		}
		savefile.read(&aLetter.PosX,sizeof(aLetter.PosX),1);
		savefile.read(&aLetter.width,sizeof(aLetter.width),1);
		aFont->character.insert(std::pair<Uint16, Letter>(position,aLetter));
	}
	savefile.close();
	FontType.push_back(aFont);
#ifndef __PUBLICRELEASE__
	std::cout<<"Font successfully loaded from "<<file<<std::endl;
#endif
}

void TextManager::loadFontTypePack(const char* file_path, Uint16 text_height){
	/*std::string line;
	MemFile* memfile;
	size_t CharBreak;
	Sint32	imageGfx = -1;
	Uint32 colorKeyChar;
	Uint32 colorKeyGfx;
	Uint32 numCharGfx;

	FontGfx*	tempFont = new FontGfx();
	std::vector<int>	coorXGfx;
	Uint32	totalLetters = 0;
	Uint32	lineLetter = 0;
	tempFont->textHeight = text_height;

	memfile = sUnpacker->loadDataFile(file_path);

	if (memfile == NULL){
		std::cerr << "Font file not found in the pack!" << std::endl;
	}else{
		while ( !memfile->mfEOF() ){
			memfile->getLine(line);
			std::string dash = line.substr(0,2);
			if (dash == "##"){						// new gfx to load
				std::string file, fileGfx;
				file = file_path;
				CharBreak = file.rfind("/");							// Gets path form file_path
				fileGfx = file.substr(0,CharBreak+1);
				fileGfx.append(line.substr(2));							// Adds file GFX
				imageGfx = sResManager->getGraphicID(fileGfx.c_str());	// Load image
				int width, height;
				sVideoHandler->getGraphicSize(imageGfx,width,height);
				Uint32 pixelWidth = width;
				if (tempFont->maxVariants == -1){						// Just only first GFX fix Variants
					tempFont->maxVariants = (height-1) / text_height;
				}
				// El Primer pixel me da el color de las separaciones.
				// El Segundo pixel me da el colorkey(=colortransparente) de toda la imagen.
				sFxGfx->lockSurface(imageGfx);
				colorKeyChar = sFxGfx->getPixel(0,0,imageGfx);
				colorKeyGfx = sFxGfx->getPixel(1,0,imageGfx);

				// Vamos a contar cuantas letras hay y en que coordenadas empieza cada una;
				coorXGfx.clear();
				for (Uint32 i=0 ; i < pixelWidth;i++){
					if (colorKeyChar == sFxGfx->getPixel(i,0,imageGfx)){
						coorXGfx.push_back(i);
					}
				}
				numCharGfx = coorXGfx.size()-1;	// Last point isn't a letter 
				sFxGfx->unlockSurface(imageGfx);
				// Marcamos la imagen con su color transparente
				sResManager->setColorKey(imageGfx,colorKeyGfx);
				totalLetters = 0;
			}
			else{		//line with characters
				Uint32 bytesPerLine = line.size();
				lineLetter = 0;
				for (Uint16 i=0; i<bytesPerLine ; i++){
					Uint16	Index = 0;
					Uint8 Caracter;
					Caracter = line[i];
					if (Caracter < 0x80){ // Hasta (U+0080h) se trata igual que un ASCII convencional
						Index = Caracter;//-0x20; // Empieza en el ESPACIO en BLANCO (U+0020h)
					}else {
						if ((Caracter&0xF0) == 0xc0){ // Estamos en codificación a 2 bytes.
							if ( ((Caracter&0x1f) == 2) || ((Caracter&0x1f) == 3) ) { // Solo hasta 255. El resto no tendria representación en la fuente.
								Uint8 Caracter2 = line[i+1];
								Index = (((Caracter & 0x03) << 6) | (Caracter2 & 0x3F));// - 0x20; // Empieza en el ESPACIO en BLANCO (U+0020h)
								i++;
							}
							else {
								Index = 0; //Si es de otro rango aparece un espacio en blanco.
								i++;
							}
						}
						if ((Caracter&0xF0) == 0xE0){ // Estamos en codificación a 3 bytes.
							Uint8 Caracter2 = line[i+1];
							Uint8 Caracter3 = line[i+2];
							Index = (Caracter&0x0F)<<12 | (Caracter2&0x3F)<<6 | (Caracter3&0x3F);
							//Index -=0x3040;
							i+=2;
						}
					}
					Letter	tempLetter;
					tempLetter.imageFile = imageGfx;
					tempLetter.PosX = coorXGfx[totalLetters+lineLetter];
					tempLetter.width = coorXGfx[totalLetters+lineLetter+1] - coorXGfx[totalLetters+lineLetter];
					lineLetter ++;
					tempFont->character.insert(std::pair<Uint16, Letter>(Index,tempLetter));
					if ((Index < 0x3A) && (Index > 0x2F)){	// Special Letter data for numbers. Duplicate Data.
						tempFont->number[Index-0x30] = tempLetter;
					}
				}
				totalLetters += lineLetter;
			}
		}
		free(memfile);
		FontType.push_back(tempFont);
		#ifdef COMPILE_FONT_AFTER_LOAD
			std::string filePath = file_path;
			filePath.append("xy"); //.fox to .foxxy
			compileFont(filePath.c_str(), tempFont);
		#endif
	}*/
}
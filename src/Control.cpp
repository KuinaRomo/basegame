//c++
#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
//SDL
//Propios de clase
#include "Control.h"
#include "Utils.h"
#include "Timer.h"
//Propios genericos


Control* Control::pInstance = NULL;// Inicializar el puntero

Control* Control::getInstance (){
  if (pInstance == NULL)  // �Es la primera llamada?
    pInstance = new Control(); // Creamos la instancia

    return pInstance; // Retornamos la direcci�n de la instancia
}

Control::Control() {
	mJoystickName = "None";
	mPadJoy = NULL;
	mContinuousAcceptSignal = false;
    mIsHat = false;
	mUseHat = true;
    mIsDelayKey = false;
    mCloseSignal = false;
    mDelayKeyTimeMax = 0;
    mDelayKeyTimeIni = SDL_GetTicks();
	SDL_InitSubSystem(SDL_INIT_JOYSTICK);
	mJoyButton.assign (MAXBUTTONS,-1);      // Create & Fill all to -1
	mKeyButton.assign (MAXBUTTONS,-1);      // Create & Fill all to -1
	mDirectionPad_Last = 0;
	mDirectionHat_Last = 0;
    mapDefaultKeyButtons();

	searchPad();

	if(getJoystickName() != "None"){
		mapDefaultPadButtons();	// load defaults.
	}
}

Control::~Control() {
}

/*
 * Init
 * Desc: Inicia variables del Control del joc.
 *		Se lanza en cada Update
 * Parametros : VOID
 */
 
void Control::init(void){
	mDirection = 0;
	mDirectionHat = 0;
	mButtons = 0;
	mDirectionRelease = 0;
	mDirectionHatRelease = 0;
	mButtonsPressed = 0;
	mButtonsRelease = 0;
	mKey = 0;
	mMouseBut = 0;
	mMouseButRelease = 0;
	mBackSignal = false;
	mCloseSignal = false;
	mEscapeSignal = false;
	if(!mContinuousAcceptSignal){
		mAcceptSignal = false;
	}
	mBackMenuSignal = false;
	mFocusOutSignal = false;
	mPadJoyEjeX = 0;
	mPadJoyEjeY = 0;
}

void Control::close(void){
	if (mPadJoy){
      SDL_JoystickClose(mPadJoy);
	}
}

Uint8 Control::getDirection(void){
	return mDirection;
}

Uint8 Control::getDirectionHat(void){
	return mDirectionHat;
}

Uint8 Control::getDirectionRelease(void){
	return mDirectionRelease;
}

Uint16 Control::getButtons(void){
	return mButtons;
}

Uint16 Control::getButtonsRelease(void){
	return mButtonsRelease;
}


Uint16 Control::getButtonsPressed(void) {
	return mButtonsPressed;
}

Uint16 Control::getKey(void){
	return mKey;
}

Sint16 Control::getPadJoyEjeX(void){
	return mPadJoyEjeX;
}

Sint16 Control::getPadJoyEjeY(void){
	return mPadJoyEjeY;
}

int Control::getMouseX(void){
	return mMouseX;
}

int Control::getMouseY(void){
	return mMouseY;
}

Uint8 Control::getMouseBut(void){
	return mMouseBut;
}

Uint8 Control::getMouseButRelease(void){
	return mMouseButRelease;
}


bool Control::getBackSignal(void){
	return mBackSignal;
}

bool Control::getAcceptSignal(void){
	return mAcceptSignal;
}

bool Control::getMenuBackSignal(void){
	return mBackMenuSignal;
}

bool Control::getCloseSignal(void){
	return mCloseSignal;
}

bool Control::getFocusOutSignal(){
	return mFocusOutSignal;
}

bool Control::getEscapeSignal(){
	return mEscapeSignal;
}

void Control::setAcceptSignalCont(bool continuous){
	mContinuousAcceptSignal = continuous;
	if(!continuous){
		mAcceptSignal = false;
	}
}

const char*	Control::getJoystickName(void){
	return mJoystickName;
}

/*
 * SetDelayKeyTime
 * Desc: Activa o desactiva el delay de capturas
 *		Si se desactiva, da igual el tiempo que se le pase. Lo ignora.
 *
 * Parametros : On (true|false), ms (tiempo en milisegundos)
 * Retorna : VOID
 */
void Control::setDelayKeyTime(bool On, Uint32 ms){
	mIsDelayKey = On;
	if (ms>1000)
		ms = 1000;
	mDelayKeyTimeMax = ms;
}

void Control::showCursor(bool show){
	if (show)
		SDL_ShowCursor(SDL_ENABLE);
	else
		SDL_ShowCursor(SDL_DISABLE);
}

/*
 * update
 * Desc: Captura les tecles i mandos del joc
 *
 * Parametros : VOID
 * Retorna : VOID
 */
void Control::update(void){
	init();
	// Limitador de tiempo de captura
	if ( (mIsDelayKey) && (SDL_GetTicks()-mDelayKeyTimeIni < mDelayKeyTimeMax) ){
		SDL_PumpEvents();
		//SDL_GetMouseState(&mMouseX, &mMouseY);
		return;
	}
	
	//Captura de teclas y mando
	while(SDL_PollEvent(&mEvento)){
		switch(mEvento.type){
			case SDL_ACTIVEEVENT:
				if(mEvento.active.gain == 0 && mEvento.active.state == SDL_APPINPUTFOCUS){
					mFocusOutSignal = true;
				}
				break;
			case SDL_QUIT:  // Si es de salida [Cruz de ventana o Alt+F4]
					mCloseSignal = true;
					break;
			case SDL_KEYDOWN:
					switch(mEvento.key.keysym.sym){
						case SDLK_ESCAPE:         // La tecla ESC es la marcada como atras en menus.
                             mBackSignal = true;
							 mEscapeSignal = true;
							 break;
						case SDLK_BACKSPACE:        
                             mBackMenuSignal = true;
							 break;
						case SDLK_RETURN:
							 mAcceptSignal = true;
							 break;
						case SDLK_UP: 	// No se usa, pero dejo la estructura a modo de ejemplo
						case SDLK_DOWN:	// por si se amplia el uso de la clase
						case SDLK_LEFT:
						case SDLK_RIGHT:
						case SDLK_SPACE:
                             break;
						case SDLK_F1:
						case SDLK_F2:
						case SDLK_F3:
                             mKey = mEvento.key.keysym.sym;
                             break;
						default:
                             break;
					}
					for (Uint8 i = 0; i < MAXBUTTONS; i++) {
						if (mKeyButton[i] != -1) {    // Solo botones definidos.
							if (mEvento.key.keysym.sym == mKeyButton[i]) {
								mButtonsPressed |= 1 << i;
							}
						}
					}
					break;		// No se usa, pero dejo la estructura a modo de ejemplo
			case SDL_KEYUP:		// por si se amplia el uso de la clase
                    switch(mEvento.key.keysym.sym){
						case SDLK_UP:
                             mDirectionRelease |= PAD_UP;
							 break;
						case SDLK_DOWN:
                             mDirectionRelease |= PAD_DOWN;
							 break;
						case SDLK_LEFT:
                             mDirectionRelease |= PAD_LEFT;
							 break;
						case SDLK_RIGHT:
                             mDirectionRelease |= PAD_RIGHT;
							 break;
					   default:
							break;
                    }
                    // Recorre la configuracion de teclas y prepara el Uint16 de Buttons.
                    for (Uint8 i=0; i < MAXBUTTONS; i++){
                    	 if (mKeyButton[i] != -1){    // Solo botones definidos.
                    		 if (mEvento.key.keysym.sym == mKeyButton[i]){
                    			 mButtonsRelease |= 1 << i;
							 }
                    	 }
                    }
				   break;
			case SDL_JOYBUTTONDOWN:
				if (mPadJoy){
					if (mEvento.jbutton.button == 0/*mJoyButton[0]*/){
						if(mContinuousAcceptSignal){
							mAcceptSignal = true;
						}
					}
					for (Uint8 i = 0; i < MAXBUTTONS; i++) {
						if (mJoyButton[i] != -1) {    // Solo botones definidos.
							if (mEvento.jbutton.button == mJoyButton[i]) {
								mButtonsPressed |= 1 << i;
							}
						}
					}
				}
				break;
			case SDL_JOYBUTTONUP:
				// Solo si hay Joystick configurado y conectado.
				// Se tiene que hacer aquí porque el poll de Estados se procesan aquí TODOS
			    if (mPadJoy){
					// Special mAcceptSignal
					if (mEvento.jbutton.button == 0/*mJoyButton[0]*/){
						if(!mContinuousAcceptSignal){
							mAcceptSignal = true;
						}else{
							mAcceptSignal = false;
						}
					}
					if (mEvento.jbutton.button == 1/*mJoyButton[5]*/){
						mBackMenuSignal = true;
					}
					if (mEvento.jbutton.button == 7/*mJoyButton[10]*/){
						mBackSignal = true;
					}
					// Recorre la configuracion de botones de joystick y prepara el Uint16 de Buttons.
					for (Uint8 i=0; i < MAXBUTTONS; i++){
						 if (mJoyButton[i] != -1){    // Solo botones definidos.
							 if (mEvento.jbutton.button == mJoyButton[i]){
								 mButtonsRelease |= 1 << i;
							 }
						 }
					}
			    }
				break;
			case SDL_MOUSEBUTTONUP:
				mMouseButRelease = 1 << (mEvento.button.button-1);
				break;
			case SDL_MOUSEBUTTONDOWN:
				mMouseBut = 1 << (mEvento.button.button-1);
				break;

		} // Cierra mEvento.type
	} // Cierra While

	Uint8 *keystate = SDL_GetKeyState(NULL);
    SDL_GetMouseState(&mMouseX, &mMouseY);

    if (mPadJoy){
       mPadJoyEjeX=SDL_JoystickGetAxis(mPadJoy, 0);
       mPadJoyEjeY=SDL_JoystickGetAxis(mPadJoy, 1);

       for (Uint8 i=0; i < MAXBUTTONS; i++) {
           if (mJoyButton[i] != -1){    // Solo botones definidos.
               if (SDL_JoystickGetButton(mPadJoy, mJoyButton[i]) ){  // Pasamos por todos los botones.
					mButtons |= 1 << i; // Pone a 1 el bit correspondiente a los botones pulsados.
			   }
           }
       }

       if (mIsHat){
    	   switch (SDL_JoystickGetHat(mPadJoy, 0)){ // Para mandos de XBOX360 o con HAT+Analogico
    			case SDL_HAT_UP:
    				mButtons |= HATBUTTON1;
					mDirectionHat |= PAD_UP;
    				break;
    			case SDL_HAT_RIGHT:
    				mButtons |= HATBUTTON2;
					mDirectionHat |= PAD_RIGHT;
    				break;
    			case SDL_HAT_DOWN:
    				mButtons |= HATBUTTON3;
					mDirectionHat |= PAD_DOWN;
    				break;
    			case SDL_HAT_LEFT:
    				mButtons |= HATBUTTON4;
					mDirectionHat |= PAD_LEFT;
    				break;
				case SDL_HAT_LEFTDOWN:
					mDirectionHat |= PAD_LEFT | PAD_DOWN;
					break;
				case SDL_HAT_LEFTUP:
					mDirectionHat |= PAD_LEFT | PAD_UP;
					break;
				case SDL_HAT_RIGHTDOWN:
					mDirectionHat |= PAD_RIGHT | PAD_DOWN;
					break;
				case SDL_HAT_RIGHTUP:
					mDirectionHat |= PAD_RIGHT | PAD_UP;
					break;
    			default:
    				break;
           }
        } // cierre mIsHat
    }

	if ( ( keystate[SDLK_UP] ) || (mPadJoyEjeY < -16000) ){
		mDirection |= PAD_UP;
	}
	if ( ( keystate[SDLK_RIGHT] ) || (mPadJoyEjeX > 16000) ){
		mDirection |= PAD_RIGHT;
	}
	if ( ( keystate[SDLK_DOWN] ) || (mPadJoyEjeY > 16000) ){
		mDirection |= PAD_DOWN;
	}
	if ( ( keystate[SDLK_LEFT] ) || (mPadJoyEjeX < -16000) ){
		mDirection |= PAD_LEFT;
	}

	//Control de Release para Pad y para Hat
	//Pad
	if ((mDirectionPad_Last & PAD_UP) && ((mDirection & PAD_UP) == 0) ){
		mDirectionRelease |= PAD_UP;
	}
	if ((mDirectionPad_Last & PAD_RIGHT) && ((mDirection & PAD_RIGHT) == 0) ){
		mDirectionRelease |= PAD_RIGHT;
	}
	if ((mDirectionPad_Last & PAD_DOWN) && ((mDirection & PAD_DOWN) == 0) ){
		mDirectionRelease |= PAD_DOWN;
	}
	if ((mDirectionPad_Last & PAD_LEFT) && ((mDirection & PAD_LEFT) == 0) ){
		mDirectionRelease |= PAD_LEFT;
	}
	// Hat
	if ((mDirectionHat_Last & PAD_UP) && ((mDirectionHat & PAD_UP) == 0) ){
		mDirectionHatRelease |= PAD_UP;
	}
	if ((mDirectionHat_Last & PAD_RIGHT) && ((mDirectionHat & PAD_RIGHT) == 0) ){
		mDirectionHatRelease |= PAD_RIGHT;
	}
	if ((mDirectionHat_Last & PAD_DOWN) && ((mDirectionHat & PAD_DOWN) == 0) ){
		mDirectionHatRelease |= PAD_DOWN;
	}
	if ((mDirectionHat_Last & PAD_LEFT) && ((mDirectionHat & PAD_LEFT) == 0) ){
		mDirectionHatRelease |= PAD_LEFT;
	}

	//std::cout << "Control:" << (int)mDirection << "," << (int)mDirectionPad_Last << "Rel:" << (int)mDirectionRelease << "\n";
	mDirectionPad_Last = 0;
	mDirectionHat_Last = mDirectionHat;
	if (mPadJoyEjeY < -16000){
		mDirectionPad_Last |= PAD_UP;
	}
	if (mPadJoyEjeX > 16000){
		mDirectionPad_Last |= PAD_RIGHT;
	}
	if (mPadJoyEjeY > 16000){
		mDirectionPad_Last |= PAD_DOWN;
	}
	if (mPadJoyEjeX < -16000){
		mDirectionPad_Last |= PAD_LEFT;
	}

    // Recorre la configuracion de teclas y prepara el Uint16 de Buttons.
    for (Uint8 i=0; i < MAXBUTTONS; i++){
    	 if (mKeyButton[i] != -1){    // Solo botones definidos.
    		 if (keystate[mKeyButton[i]]){
    			 mButtons |= 1 << i;
			 }
    	 }
    }

   	// Si conseguimos capturar algo se reinicia el tiempo
    if ( (mDirection) || (mButtons) || (mKey) || (mMouseBut) ){
    	mDelayKeyTimeIni = SDL_GetTicks();
    }
}

void Control::searchPad(){
	if (SDL_NumJoysticks()){
		mPadJoy = SDL_JoystickOpen(0);        					// De moment el primer joystick que trobi.
		mJoystickName = SDL_JoystickName(0);
#ifndef __PUBLICRELEASE__
		std::cout << mJoystickName << " conectado." << std::endl;
#endif
		SDL_JoystickEventState(SDL_ENABLE);
        mButtonsMax = SDL_JoystickNumButtons(mPadJoy);      	// cuantos botones tiene el joystick. Para hacer bucles.
        if (mButtonsMax > MAXBUTTONS ) mButtonsMax = MAXBUTTONS;// Protecci�n para Uint16.
        if (SDL_JoystickNumHats(mPadJoy))                  		// Si tiene Hat lo usar� como buttons [Joy Xbox360]
           mIsHat = true;
	}
}

/*
 * MapDefaultPKeyButtons
 * Desc: Define el mapeado de los botones para el teclado por defecto
 *
 * Parametros : VOID
 * Retorna : VOID
 */
void Control::mapDefaultKeyButtons(){
     for (Uint8 i = 0; i<MAXBUTTONS; i++){
    	 mKeyButton[i] = keyboardButtons[i];   // asignado uno a uno los validos.
	 }
}

/*
 * mapDefaultPadButtons
 * Desc: Define el mapeado de los botones para el Pad por defecto (Xbox360)
 *
 * Parametros : VOID
 * Retorna : VOID
 */
void Control::mapDefaultPadButtons(){
     int inicial[MAXBUTTONS] = {0, 4, 5, 2, 3, 1, -1, -1, -1, -1, 7, 6, -1, -1, -1, -1};
     for (Uint8 i = 0; i<MAXBUTTONS; i++){
    	 mJoyButton[i] =  inicial[i];   // asignado uno a uno los validos.
	 }
}


void Control::mapButtonsVector(std::vector<int> buttons, bool padVector){
	if(padVector){
		mJoyButton = buttons;
	}else{
		mKeyButton = buttons;
	}
}

bool Control::isXboxPad(){
	bool returnValue = false;
	std::string padName = getJoystickName();
	if (findInString(padName, "XBOX 360") || 
		findInString(padName,"Xbox 360") ||
		findInString(padName,"xbox 360") ||
		findInString(padName,"X360 Controller")
		){
		returnValue = true;
	}

	return returnValue;
}

bool Control::isDirectionDown(Uint8 dir, int hatMode) {
	Uint8 directions = getDirection();
	Uint8 directionsHat = getDirectionHat();
	switch (hatMode) {
		case 0: //Only stick
			return directions & dir;
			break;
		case 1: //Only d-pad
			return directionsHat & dir;
			break;
		case 2: //Both
			return (directions | directionsHat) & dir;
			break;
	}
	return false;
}
bool Control::isDirectionReleased(Uint8 dir, int hatMode) {
	Uint8 directions = getDirectionRelease();
	Uint8 directionsHat = mDirectionHatRelease;
	switch (hatMode) {
	case 0: //Only stick
		return directions & dir;
		break;
	case 1: //Only d-pad
		return directionsHat & dir;
		break;
	case 2: //Both
		return (directions | directionsHat) & dir;
		break;
	}
	return false;
}
bool Control::isButtonDown(Uint16 btn) {
	return mButtons & btn;
}
bool Control::isButtonPressed(Uint16 btn) {
	return mButtonsPressed & btn;
}
bool Control::isButtonReleased(Uint16 btn) {
	return mButtonsRelease & btn;
}

bool Control::isMouseButtonPressed(Uint16 btn) {
	return mMouseBut & btn;
}

bool Control::isMouseButtonReleased(Uint16 btn) {
	return mMouseButRelease & btn;
}
//Include our classes
#include "SceneDirector.h"

//Escenas "navegables"
#include "SceneGame.h"
#include "SceneMenu.h"
#include "SceneFight.h"

SceneDirector* SceneDirector::pInstance = NULL;

SceneDirector* SceneDirector::getInstance(){
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new SceneDirector();
	}
	return pInstance;
}

SceneDirector::SceneDirector(){
}

SceneDirector::~SceneDirector(){
}

void SceneDirector::init(){
	mVectorScenes.resize(SCENE_LAST);

	SceneMenu* menu = new SceneMenu();
	SceneGame* game = new SceneGame();
	SceneFight* fight = new SceneFight();
	mVectorScenes[MAIN] = menu;
	mVectorScenes[LEVEL] = game;
	mVectorScenes[FIGHT] = fight;
	menu->init();
	game->init();
	fight->init();

	//Primera escena del juego
	mCurrScene = MAIN;
}

void SceneDirector::changeScene(SceneEnum next_scene, bool load_on_return, bool history){
	mVectorScenes[mCurrScene]->setLoaded(false);
	mVectorScenes[mCurrScene]->setReloadOnReturn(load_on_return);

	if(history){
		mPrevScene = mCurrScene;
	}
	mCurrScene = next_scene;
}

void SceneDirector::goBack(bool load_on_return){
	mVectorScenes[mCurrScene]->setLoaded(false);
	mVectorScenes[mCurrScene]->setReloadOnReturn(load_on_return);
	mCurrScene = mPrevScene;
}

Scene* SceneDirector::getCurrentScene(){
	return mVectorScenes[mCurrScene];
}

void SceneDirector::exitGame(){
	exit(0);
}

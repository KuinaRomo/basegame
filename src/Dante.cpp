//Include our classes
#include "Dante.h"

Dante::Dante() : PlayerFight() {
	mLife = 100;
	mTotalLoad = 250;
	mLoading = 0;
	mAttack = false;
	mMenu = false;
	mActionChosed = false;
	mPrepared = false;
	mActionSelected = "";
	mName = "Dante";
	
	mPositionCursor = 0;
	mActions.resize(4);
	mActions[0] = "Attack";
	mActions[1] = "Steal";
	mActions[2] = "Item";
	mActions[3] = "Escape";

	mAction1->setString(0, 0, mActions[0]);
	mAction2->setString(0, 0, mActions[1]);
	mAction3->setString(0, 0, mActions[2]);
	mAction4->setString(0, 0, mActions[3]);

	mLoadVelocity = 1;
}

Dante::~Dante(){
}

void Dante::update(){
	PlayerFight::update();
		
	
}
void Dante::render(){
	PlayerFight::render();
}

bool Dante::isOfClass(std::string classType){
	if(classType == "Dante" || classType == "Player" || classType == "Entity"){
		return true;
	}
	return false;
}
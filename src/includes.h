#ifndef INCLUDES_H
#define INCLUDES_H

#ifdef __linux__
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_mixer.h"
#endif
#ifdef WIN32
#include "SDL_image.h"
#include "SDL_mixer.h"
#endif

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include "math.h"

// Flags
//#define __PUBLICRELEASE__
//#define __ONLYCOLLISION__

#define TILE_SIZE 32
#define SCREEN_WIDTH 960	// PC/WiiU - 854, 1280, 1920 | 3DS - 400
#define SCREEN_HEIGHT 540	// PC/WiiU - 480, 720, 1080  | 3DS - 240

// Subpantallas para WiiU y 3DS
//#define __WIIU__
//#define __3DS__
#if defined(__WIIU__) || defined (__3DS__)
	#define SUBSCREEN_WIDTH 854		//WiiU - 854 | 3DS - 320
	#define SUBSCREEN_HEIGHT 480	//WiiU - 480 | 3DS - 240
#else
	#define SIMULATE_TWO_SCREENS
#endif
#define BPP 32

#define CURSOR_TIME_TO_SHOW 1000
#define CLICK_TIME_TO_SHOW 500

// Esta estructura es una replica a SDL_Rect
// Debe tener el mismo formato para igualarse
// x e y con signo. Un grafico puede situarse en negativo para aparecer por el lado Izq.
//! Struct C_Rectangle.
/*! Replica of SDL_Rect. */
typedef struct { 
	  Sint16 x;
	  Sint16 y;
	  Uint16 w;
	  Uint16 h;
} C_Rectangle;

//! Struct Point.
/*! A point in 2D. */
struct Point { 
	Sint32 x;
	Sint32 y;

	bool operator==(const Point& a) const
	{
		return (x == a.x && y == a.y);
	}
	bool operator!=(const Point& a) const
	{
		return (x != a.x || y != a.y);
	}
};

//! Struct Triangle.
/*! Struct that consists of 3 points. */
typedef struct { 
	Point a;
	Point b;
	Point c;
} C_Triangle;

// Tiempo pasado entre frames
extern Uint32 global_delta_time;

extern bool key_pressed[255];
extern bool key_down[255];
extern bool key_released[255];

enum Directions { NONE, UP, DOWN, LEFT, RIGHT };

//Eliminar vectores de punteros
template <class C> void FreeClear( C & cntr ) {
    for ( typename C::iterator it = cntr.begin(); 
              it != cntr.end(); ++it ) {
    	delete * it;
    }
    cntr.clear();
	cntr.shrink_to_fit(); //Hace que la memoria reservada sea la necesaria para el nuevo tama�o (0)
}
#endif

//Include our classes
#include "PlayerFight.h"

PlayerFight::PlayerFight() : FightEntity() {
	//Poner en player
	mNormalMenu.resize(4);
	mNormalMenu[0] = 180;
	mNormalMenu[1] = 150;
	mNormalMenu[2] = 120;
	mNormalMenu[3] = 90;

	mCursor = sResManager->getGraphicID("Assets/gfx/manoCursor.png");
	mImageMenu = sResManager->getGraphicID("Assets/gfx/menu.png");

	mPositionCursor = 0;

	mAction1 = new Text(0, 0);
	mAction2 = new Text(0, 0);
	mAction3 = new Text(0, 0);
	mAction4 = new Text(0, 0);

}

PlayerFight::~PlayerFight(){
}

void PlayerFight::update(){
	FightEntity::update();
	if (!mAttack && mLoading < mTotalLoad) {
		mPositionCursor = 0;
		mMenu = false;
	}
	if (mActionChosed) {
		mActionSelected = mActions[mPositionCursor];
		if (mActionSelected == "Attack") {
			std::cout << "Attack the monster. Open menu select monster" << std::endl;
			mActionChosed = false;
		}

		else if (mActionSelected == "Steal") {
			std::cout << "Steal the monster. Open menu select monster" << std::endl;
			mActionChosed = false;
		}

		else if (mActionSelected == "Escape") {
			std::cout << "Escape of the battle" << std::endl;
			mActionChosed = false;
		}

		else if (mActionSelected == "Item") {
			std::cout << "Select Item. Open menu item" << std::endl;
			mActionChosed = false;
		}

		else {
			std::cout << "Use magic. Open menu magic" << std::endl;
			mActionChosed = false;
		}

		if (!mActionChosed) {
			mMenu = false;
			mLoading = 0;
			mAttack = false;
		}
	}

	//sInputControl->getBackSignal() BOTON_2 BACKSPACE O ESCAPE
	if (sInputControl->isDirectionReleased(PAD_UP, 2)) {//Space
		if (mPositionCursor > 0) {
			mPositionCursor--;
		}
	}

	if (sInputControl->isDirectionReleased(PAD_DOWN, 2)) {
		if (mPositionCursor < 3) {
			mPositionCursor++;
		}
	}
}
void PlayerFight::render(){
	if (mMenu) {
		//ofSetColor(140, 140, 140);
		imgRender(mImageMenu, 100, SCREEN_HEIGHT - 200, C_Rectangle{ 0,0,250,150 });
		//ofDrawRectangle(100, SCREEN_HEIGHT - 200, 250, 150);
		mAction1->render(150, SCREEN_HEIGHT - 160, 255, false);
		mAction2->render(150, SCREEN_HEIGHT - 130, 255, false);
		mAction3->render(150, SCREEN_HEIGHT - 100, 255, false);
		mAction4->render(150, SCREEN_HEIGHT - 70, 255, false);
		imgRender(mCursor, 110, SCREEN_HEIGHT - mNormalMenu[mPositionCursor], C_Rectangle{ 0,0,32,32 });
	}
}

bool PlayerFight::isOfClass(std::string classType){
	if(classType == "FightEntity" || classType == "PlayerFight"){
		return true;
	}
	return false;
}

bool PlayerFight::getMenu() {
	return mMenu;
}

void PlayerFight::setMenu(bool men) {
	mMenu = men;
	return;
}

void PlayerFight::setActionChosed(bool action) {
	mActionChosed = action;
	return;
}

bool PlayerFight::getActionChosed() {
	return mActionChosed;
}

std::string PlayerFight::getActionSelected() {
	return mActionSelected;
}


#include "singletons.h"
//#include "ResourceManager.h"

#include <algorithm>
#include <sstream>

ResourceManager* ResourceManager::pInstance = NULL;

ResourceManager* ResourceManager::getInstance(){
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new ResourceManager();
	}
	return pInstance;
}

ResourceManager::ResourceManager(){
	mFirstFreeSlot = 0;
}

ResourceManager::~ResourceManager(){
	mGraphicsMap.clear();
	mGraphicsVector.clear();
	mIDMap.clear();
}

Sint32 ResourceManager::addGraphic(const char* file){
	//Carga una imagen y la guarda en los contenedores
	SDL_Surface* graph1 = NULL;

	//Con esto lo mira en la carpeta.
	graph1 = IMG_Load(file);

	//Con esto lo mira en el pack.
	/*if(graph1 == NULL){
		graph1 = sUnpacker->loadImage(file);
	}*/

    if (graph1 == NULL){
		std::cerr << "Error when loading image: "<< file << ": " << SDL_GetError() << std::endl;
		return -1;
    }
    else {
		// Realizamos la conversi�n de la imagen a la del formato de pantalla
		// Tenemos en cuenta si la imagen original lleva alpha (32 bits) o no. Mejoramos tiempo de conversi�n.
		// TODO : En el caso de imagenes con ColorKey, podriamos controlarlo y
		//      Hacer el SDL_SetColorKey antes del SDL_DisplayFormatAlpha
		//      As� se convertiria directamente.
		SDL_Surface* graph2 = NULL;
		if (graph1->format->BitsPerPixel == 32){
			graph2 = SDL_DisplayFormatAlpha(graph1);
		}else{
			graph2 = SDL_DisplayFormat(graph1);
		}
		SDL_FreeSurface (graph1);

		mGraphicsMap.insert(std::pair<std::string, SDL_Surface*>(file, graph2));
		Sint32 returnValue = 0;
		if(mFirstFreeSlot == mGraphicsVector.size()){
			mGraphicsVector.push_back(graph2);
			mFirstFreeSlot++;
			returnValue = mGraphicsVector.size()-1;
		}else{
			mGraphicsVector[mFirstFreeSlot] = graph2;
			Uint32 temp = mFirstFreeSlot;
			mFirstFreeSlot = updateFirstFreeSlot();
			returnValue = temp;
		}
		return returnValue;
    }
}

Sint32 ResourceManager::addResizedGraphic(const char* file, Uint16 width, Uint16 height){
	//Carga una imagen de fichero y la guarda en los contenedores
	SDL_Surface* graph1 = getGraphic(file);
	
	if(graph1 == NULL){
		return -1;
	}
	std::stringstream ss;
	ss << file << "[" << width << "x" << height <<"]";
	std::string path = ss.str();

	SDL_Surface* new_surface = sFxGfx->scaleSurface(graph1, width, height);

	mGraphicsMap.insert(std::pair<std::string, SDL_Surface*>(path, new_surface));

	Sint32 returnValue = 0;
	if(mFirstFreeSlot == mGraphicsVector.size()){
		mGraphicsVector.push_back(new_surface);
		mFirstFreeSlot++;
		returnValue = mGraphicsVector.size()-1;
	}else{
		mGraphicsVector[mFirstFreeSlot] = new_surface;
		Uint32 temp = mFirstFreeSlot;
		mFirstFreeSlot = updateFirstFreeSlot();
		returnValue = temp;
	}
	return returnValue;
	return 0;
}

void ResourceManager::removeGraphic(const char* file){
	std::map<std::string,Sint32>::iterator iter = mIDMap.find(file);
	if(iter == mIDMap.end()){
#ifndef __PUBLICRELEASE__
		std::cout<<"The file "<<file<<" is not loaded yet. Cannot delete it."<<std::endl;
#endif
		return;
	}

	Uint32 ID = getGraphicID(file);

#ifndef __PUBLICRELEASE__
	std::cout<<"Removing " << file << " from the resources"<<std::endl;
#endif
	//mGraphicsVector.erase(mGraphicsVector.begin() + ID);
	if(mFirstFreeSlot > ID){
		mFirstFreeSlot = ID;
	}

	SDL_Surface* surface = getGraphicByID(ID);
	SDL_FreeSurface(surface);

	mGraphicsVector[ID] = NULL;
	mGraphicsMap.erase(file);
	mIDMap.erase(file);
	delete(surface);
}

SDL_Surface* ResourceManager::getGraphic(const char* file){
	// Si la imagen ha sido cargada, devuelve el puntero
	// Si no, carga la imagen y devuelve el puntero

	std::map<std::string,SDL_Surface*>::iterator it = mGraphicsMap.find(file);
	if(it == mGraphicsMap.end()){
#ifndef __PUBLICRELEASE__
		std::cout<<"Adding Graphic " << file << " (first request)"<<std::endl;
#endif
		addGraphic(file);
		it = mGraphicsMap.find(file);
	}
	return it->second;
}

Sint32 ResourceManager::getGraphicID(const char* file){
	// Mira si el gr�fico ya ha sido cargado
	// Si no ha sido cargado, lo carga y devuelve la �ltima posici�n del vector
	// Si ya est� en el map, busca el gr�fico en el vector y devuelve la posici�n

	std::map<std::string,SDL_Surface*>::iterator it = mGraphicsMap.find(file);
	Sint32 returnValue = -1;
	if(it == mGraphicsMap.end()){
#ifndef __PUBLICRELEASE__
		std::cout<<"Adding Graphic " << file << " (first request)"<<std::endl;
#endif
		Sint32 index = addGraphic(file);
		mIDMap.insert(std::pair<std::string, Sint32>(file, index));
		returnValue = index;
	}else{
		std::map<std::string,Sint32>::iterator iter = mIDMap.find(file);
		if(iter == mIDMap.end()){
#ifndef __PUBLICRELEASE__
			std::cout<<"Failed to find "<<file<<" on the ID map"<<std::endl;
#endif
			returnValue = searchGraphic(it->second);
		}else{
			returnValue = iter->second;
		}
	}

	return returnValue;
}

Sint32 ResourceManager::getResizedImageID(const char* file, Uint16 width, Uint16 height){
	// Mira si el grafico redimensionado con los mismos parámetros ya ha sido creado y cargado
	// Si no ha sido cargado, lo carga y devuelve la ultima posicion del vector
	// Si ya esta en el map, busca el grafico en el vector y devuelve la posicion

	std::stringstream ss;
	ss << file << "[" << width << "x" << height <<"]";
	std::string path = ss.str();

	getGraphic(file);
	Sint32 returnValue = -1;
	std::map<std::string,SDL_Surface*>::iterator it = mGraphicsMap.find(path);
	if(it == mGraphicsMap.end()){
#ifndef __PUBLICRELEASE__
		std::cout<<"Resizing Graphic " << file << " to "<< width <<"x"<< height <<"(first request)"<<std::endl;
#endif
		Uint32 index = addResizedGraphic(file, width, height);
		mIDMap.insert(std::pair<std::string, Sint32>(path, index));
		returnValue = index;
	}else{
		std::map<std::string,Sint32>::iterator iter = mIDMap.find(path);
		if(iter == mIDMap.end()){
#ifndef __PUBLICRELEASE__
			std::cout<<"Failed to find "<<path<<" on the ID map"<<std::endl;
#endif
			returnValue = searchGraphic(it->second);
		}else{
			returnValue = iter->second;
		}
	}

	return returnValue;
}

SDL_Surface* ResourceManager::getGraphicByID(Uint32 ID){
	// Si el ID que se le pide est� dentro del rango del vector,
	// devuelve el valor en esa posici�n
	// Una ID nunca sera <0 porque size es sin signo. Aqui GCC daba un warning.
	if (ID < mGraphicsVector.size()){
		return mGraphicsVector[ID];
	}else{
		return NULL;
	}
}

Sint32 ResourceManager::searchGraphic(SDL_Surface* img){
	// Busca el gr�fico en el vector, si no lo encuentra, devuelve -1

	for(Uint32 i = 0; i < mGraphicsVector.size(); i++){
		if(mGraphicsVector[i] == img){
			return i;
		}
	}
	return -1;
}

std::string ResourceManager::getGraphicPathByID(Uint32 ID){
	if(ID >= mGraphicsVector.size()){return "NULL";}
	SDL_Surface* aSurface = mGraphicsVector[ID];

	std::map<std::string, SDL_Surface*>::iterator it;
	for(it = mGraphicsMap.begin(); it != mGraphicsMap.end(); ++it){
		if (it->second == aSurface){
			return it->first;
		}
	}
	return "NULL";
}

void ResourceManager::loadAllAssets(){

}

void ResourceManager::unloadAllAssets(){
	//Borra el map de gr�ficos
	mGraphicsMap.clear();
	mGraphicsVector.clear();
	mIDMap.clear();
}

void ResourceManager::setColorKey(Uint32 ID,Uint32 color_key){

	SDL_Surface *surface = NULL;
	surface = getGraphicByID(ID);
	// ColorKey en ARGB format A se ignora of course
	Uint8 R,G,B;
	R = (color_key>>16) & 255;
	G = (color_key>>8) & 255;
	B = color_key & 255;

	SDL_SetColorKey(surface, SDL_SRCCOLORKEY, SDL_MapRGB(surface->format,R,G,B));
}

void ResourceManager::setAlpha(Uint32 ID, Uint8 alpha_value){
	//NO FUNCIONA CON IMAGENES PNG CON CANAL ALPHA, MADAFACA!
	//RGBA->RGBA with SDL_SRCALPHA	
	//  The source is alpha-blended with the destination using the source alpha channel. 
	//  The alpha channel in the destination surface is left untouched. 
	SDL_SetAlpha(getGraphicByID(ID),SDL_SRCALPHA,alpha_value);
}

Uint32 ResourceManager::updateFirstFreeSlot(){
	for(Uint32 i = 0; i < mGraphicsVector.size(); i++){
		if(mGraphicsVector[i] == NULL){
			return i;
		}
	}
	return mGraphicsVector.size();
}

void ResourceManager::printLoadedGraphics(){
	std::map<std::string,SDL_Surface*>::iterator it = mGraphicsMap.begin();
	std::cout<<"---- Loaded Graphics ----"<<std::endl;
	while(it != mGraphicsMap.end()){
		std::cout<< it->first << std::endl;
		it++;
	}
	std::cout<<"-------------------------"<<std::endl;
}


Sint32 ResourceManager::createGraphic(const char* name, Uint16 width, Uint16 height){
	SDL_Surface* surface = sVideoManager->getScreen();

    if(!width || !height){
        return 0;
	}

    SDL_Surface* new_surface = SDL_CreateRGBSurface(surface->flags, width, height, surface->format->BitsPerPixel,
        surface->format->Rmask, surface->format->Gmask, surface->format->Bmask, surface->format->Amask);

	mGraphicsMap.insert(std::pair<std::string, SDL_Surface*>(name, new_surface));

	Sint32 returnValue = 0;
	if(mFirstFreeSlot == mGraphicsVector.size()){
		mGraphicsVector.push_back(new_surface);
		mFirstFreeSlot++;
		mIDMap.insert(std::pair<std::string, Sint32>(name, mGraphicsVector.size()-1));
		returnValue = mGraphicsVector.size()-1;
	}else{
		mGraphicsVector[mFirstFreeSlot] = new_surface;
		Uint32 temp = mFirstFreeSlot;
		mFirstFreeSlot = updateFirstFreeSlot();
		mIDMap.insert(std::pair<std::string, Sint32>(name, temp));
		returnValue = temp;
	}
	return returnValue;
}

void ResourceManager::getGraphicSize(Uint32 img, int &width, int &height){
	width = 0;
	height = 0;
	if (img != -1){
		width = sResManager->getGraphicByID(img)->w;
		height = sResManager->getGraphicByID(img)->h;
	}
}

Uint16 ResourceManager::getGraphicWidth(Uint32 img){
	if (img != -1){
		return sResManager->getGraphicByID(img)->w;
	}
	return 0;
}

Uint16 ResourceManager::getGraphicHeight(Uint32 img){
	if (img != -1){
		return sResManager->getGraphicByID(img)->h;
	}
	return 0;
}

#ifndef SINGLETONS_H
#define SINGLETONS_H

#include "Video.h"
#include "ResourceManager.h"
#include "SceneDirector.h"
#include "Control.h"
#include "FxGfx.h"
#include "TextManager.h"
#include "MapManager.h"
#include "SoundManager.h"
#include "FileManager.h"

extern Video*			sVideoManager;	/*!<  Handler for rendering*/
extern ResourceManager*	sResManager;	/*!<  Handler for loading and unloading graphical assets*/
extern SceneDirector*	sDirector;		/*!<  Handler for Scenes*/
extern Control*			sInputControl;	/*!<  Handler for user interface*/
extern FxGfx*			sFxGfx;			/*!<  Special effects on surfaces (painting, resizing, etc.)*/
extern TextManager*		sTxtManager;	/*!<  Loads all texts and fonts of the game*/
extern MapManager*		sMapManager;
extern SoundManager*	sSoundManager;

void instanceSingletons();

#endif

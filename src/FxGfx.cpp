#include "singletons.h"

FxGfx* FxGfx::pInstance = NULL;

FxGfx* FxGfx::getInstance(){
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new FxGfx();
	}
	return pInstance;
}

FxGfx::FxGfx(){
}

FxGfx::~FxGfx(){
}

void FxGfx::lockSurface(Uint32 ID){
	SDL_Surface *surface = NULL;
	surface = sResManager->getGraphicByID(ID);

	if (SDL_MUSTLOCK(surface)){
		SDL_LockSurface(surface);
	}
}

void FxGfx::unlockSurface(Uint32 ID){
	SDL_Surface *surface = NULL;
	surface = sResManager->getGraphicByID(ID);

	if (SDL_MUSTLOCK(surface)){
		SDL_UnlockSurface(surface);
	}
}

void FxGfx::Copy(Uint32 IDsrc, C_Rectangle src, Uint32 IDdst, C_Rectangle dst){
	SDL_Surface *surfaceSrc = sResManager->getGraphicByID(IDsrc);
	SDL_Surface *surfaceDst = sResManager->getGraphicByID(IDdst);
	SDL_Rect	srcrect,dstrect;

	srcrect.x = src.x;
	srcrect.y = src.y;
	srcrect.w = src.w;
	srcrect.h = src.h;

	dstrect.x = dst.x;
	dstrect.y = dst.y;


	SDL_BlitSurface(surfaceSrc, &srcrect, surfaceDst, &dstrect);
}

void FxGfx::putPixelSurface(int x, int y, Uint32 pixel, SDL_Surface *surface){
    int bpp = surface->format->BytesPerPixel;

    /* Here p is the address to the pixel we want to set */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
		case 1:
			*p = pixel;
			break;
		case 2:
			*(Uint16 *)p = pixel;
			break;
		case 3:
			if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
				p[0] = (pixel >> 16) & 0xff;
				p[1] = (pixel >> 8) & 0xff;
				p[2] = pixel & 0xff;
			} else {
				p[0] = pixel & 0xff;
				p[1] = (pixel >> 8) & 0xff;
				p[2] = (pixel >> 16) & 0xff;
			}
			break;

		case 4:
			Uint8 alpha = (pixel >> 24);
			if (!alpha || alpha == 0xFF){
				*(Uint32 *)p = pixel;
			}else if(alpha != 0x00){
				Uint32 color = getPixelSurface(x, y, surface);
				Uint8 dR, dG, dB, sR, sG, sB, fR, fG, fB;
				if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
					dR = (color >> 16) & 0xff;
					dG = (color >> 8) & 0xff;
					dB = color & 0xff;
					sR = (pixel >> 16) & 0xff;
					sG = (pixel >> 8) & 0xff;
					sB = pixel & 0xff; 
				} else {
					dR = color & 0xff;
					dG = (color >> 8) & 0xff;
					dB = (color >> 16) & 0xff;
					sR = pixel & 0xff;
					sG = (pixel >> 8) & 0xff;
					sB = (pixel >> 16) & 0xff; 
				}
				fR = (alpha*sR + (255-alpha)*dR)/255;
				fG = (alpha*sG + (255-alpha)*dG)/255;
				fB = (alpha*sB + (255-alpha)*dB)/255;

				if(SDL_BYTEORDER == SDL_BIG_ENDIAN){
					pixel = (fR << 16) | (fG << 8) | (fB);
				}else{
					pixel = (fR) | (fG << 8) | (fB << 16);
				}
				*(Uint32 *)p = pixel;
			}			
			break;
    }
}

void FxGfx::putPixel(int x, int y, Uint32 pixel){
	SDL_Surface *surface = sVideoManager->getScreen();
	putPixelSurface(x,y,pixel,surface);
}

void FxGfx::putPixel(int x, int y, Uint32 pixel, Sint32 ID){
	SDL_Surface *surface = sResManager->getGraphicByID(ID);
	putPixelSurface(x,y,pixel,surface);
}

Uint32 FxGfx::getPixelSurface(int x, int y, SDL_Surface *surface){
	int bpp = surface->format->BytesPerPixel;

	/* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp){
		case 1:
			return *p;
			break;
		case 2:
			return *(Uint16 *)p;
			break;
		case 3:
			if(SDL_BYTEORDER == SDL_BIG_ENDIAN){
				return p[0] << 16 | p[1] << 8 | p[2];
			}else{
				return p[0] | p[1] << 8 | p[2] << 16;
			}
			break;
		case 4:
			return *(Uint32 *)p;
			break;
		default:
			return 0;       /* shouldn't happen, but avoids warnings */
    }
}

Uint32 FxGfx::getPixel(int x, int y){
	SDL_Surface *surface = sVideoManager->getScreen();
	return getPixelSurface(x, y, surface);
}

Uint32 FxGfx::getPixel(int x, int y, Sint32 ID){
	SDL_Surface *surface = sResManager->getGraphicByID(ID);
	return getPixelSurface(x, y, surface);
}

SDL_Surface* FxGfx::scaleSurface(SDL_Surface* surface, Uint16 width, Uint16 height){
    if(!surface || !width || !height){
        return 0;
	}

    SDL_Surface* ret = SDL_CreateRGBSurface(surface->flags, width, height, surface->format->BitsPerPixel,
        surface->format->Rmask, surface->format->Gmask, surface->format->Bmask, surface->format->Amask);
 
    double    stretch_factor_x = (static_cast<double>(width)  / static_cast<double>(surface->w)),
        stretch_factor_y = (static_cast<double>(height) / static_cast<double>(surface->h));
 
    for(Sint32 y = 0; y < surface->h; y++){
        for(Sint32 x = 0; x < surface->w; x++){
            for(Sint32 o_y = 0; o_y < stretch_factor_y; ++o_y){
				for(Sint32 o_x = 0; o_x < stretch_factor_x; ++o_x){
                    putPixelSurface(static_cast<Sint32>(stretch_factor_x * x) + o_x,
                        static_cast<Sint32>(stretch_factor_y * y) + o_y, getPixelSurface(x, y, surface),ret);
				}
			}
		}
	}
    return ret;
}

void FxGfx::rectSurface (Uint16 x,Uint16 y,Uint16 w,Uint16 h,Uint32 color, SDL_Surface *surface){

	SDL_Rect Rectangulo;
	
	Rectangulo.x  = x;
	Rectangulo.y  = y;
	Rectangulo.w  = w;
	Rectangulo.h  = h;

	Uint8 alpha = (color >> 24);
	if (!alpha){
		SDL_FillRect(surface,&Rectangulo,color);
	}else{
		SDL_Surface *sfc = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_ANYFORMAT, w, h, surface->format->BitsPerPixel, 0, 0, 0, 0);
		SDL_FillRect(sfc, NULL, color&0x00FFFFFF);
		SDL_SetAlpha(sfc, SDL_SRCALPHA, alpha);
		SDL_BlitSurface(sfc, NULL, surface, &Rectangulo);
		SDL_FreeSurface(sfc);
	}
}

void FxGfx::rectFill(Uint16 x,Uint16 y,Uint16 w,Uint16 h,Uint32 color, Sint32 ID){
	SDL_Surface *surface = sResManager->getGraphicByID(ID);
	rectSurface (x, y, w , h, color, surface);
}
void FxGfx::rectFill(Uint16 x,Uint16 y,Uint16 w,Uint16 h,Uint32 color){
	SDL_Surface *surface = sVideoManager->getScreen();
	rectSurface (x, y, w , h, color, surface);
}

void FxGfx::triangleLine(C_Triangle tri, Uint32 color){
	SDL_Surface *surface = sVideoManager->getScreen();
	Sint16 vx[3];
	Sint16 vy[3];

	vx[0] = tri.a.x;
	vx[1] = tri.b.x;
	vx[2] = tri.c.x;

	vy[0] = tri.a.y;
	vy[1] = tri.b.y;
	vy[2] = tri.c.y;

	drawPolygon(vx,vy,3,color);
}

void FxGfx::triangleFill(C_Triangle tri, Uint32 color){
	SDL_Surface *surface = sVideoManager->getScreen();
	Sint16 vx[3];
	Sint16 vy[3];

	vx[0] = tri.a.x;
	vx[1] = tri.b.x;
	vx[2] = tri.c.x;

	vy[0] = tri.a.y;
	vy[1] = tri.b.y;
	vy[2] = tri.c.y;

	fillPolygon(vx,vy,3,color);
}

void FxGfx::polygonLine(std::vector<Point>& points, Uint32 color, Uint32 limit){
	SDL_Surface *surface = sVideoManager->getScreen();

	Uint16 size;
	if(limit == 0 || limit > points.size()){
		size = points.size();
	}else{
		size = limit;
	}

	Sint16* vx = new Sint16 [size];
	Sint16* vy = new Sint16 [size];

	for(Uint16 i = 0; i < size; i++){
		vx[i] = points[i].x;
		vy[i] = points[i].y;
	}
	
	drawPolygon(vx,vy,size,color);

	delete[] vx;
	delete[] vy;
}

void FxGfx::polygonFill(std::vector<Point>& points, Uint32 color, Uint32 limit){
	SDL_Surface *surface = sVideoManager->getScreen();

	Uint16 size;
	if(limit == 0 || limit > points.size()){
		size = points.size();
	}else{
		size = limit;
	}

	Sint16* vx = new Sint16 [size];
	Sint16* vy = new Sint16 [size];

	for(Uint16 i = 0; i < size; i++){
		vx[i] = points[i].x;
		vy[i] = points[i].y;
	}
	
	fillPolygon(vx,vy,size,color);

	delete[] vx;
	delete[] vy;
}


// DrawPolygon y FillPolygon basadas en funciones de la librer�a SDL_gfx

void FxGfx::drawPolygon(const Sint16 * vx, const Sint16 * vy, int n, Uint32 color){
    int result;
    int i;
    const Sint16 *x1, *y1, *x2, *y2;
	SDL_Surface *surface = sVideoManager->getScreen();

    /* Check visibility of clipping rectangle*/
    if ((surface->clip_rect.w==0) || (surface->clip_rect.h==0)) {
            return;
    }
 
    /* Vertex array NULL check */
    if (vx == NULL) {return;}
	if (vy == NULL) {return;}
 
    /* Sanity check */
    if (n < 3) {return;}
 
    /* Pointer setup*/
    x1 = x2 = vx;
    y1 = y2 = vy;
    x2++;
    y2++;
 
    /* Draw */
    result = 0;
    for (i = 1; i < n; i++) {
			lineSafe(*x1, *y1, *x2, *y2, color);
            x1 = x2;
            y1 = y2;
            x2++;
            y2++;
    }
	lineSafe(*x1, *y1, *vx, *vy, color);
    return;
}


static int *gfxPrimitivesPolyIntsGlobal = NULL;
static int gfxPrimitivesPolyAllocatedGlobal = 0;

int _gfxPrimitivesCompareInt(const void *a, const void *b){
        return (*(const int *) a) - (*(const int *) b);
}

void FxGfx::fillPolygon(const Sint16 * vx, const Sint16 * vy, int n, Uint32 color){
	SDL_Surface *surface = sVideoManager->getScreen();
	int result;
	int i;
	int y, xa, xb;
	int miny, maxy;
	int x1, y1;
	int x2, y2;
	int ind1, ind2;
	int ints;
	int *gfxPrimitivesPolyInts = NULL;
	int *gfxPrimitivesPolyIntsNew = NULL;
	int gfxPrimitivesPolyAllocated = 0;
	/* Check visibility of clipping rectangle*/
	if ((surface->clip_rect.w==0) || (surface->clip_rect.h==0)) {return;}
	/* Vertex array NULL check  */
	if (vx == NULL) {return;}
	if (vy == NULL) {return;}
 
    /* Sanity check number of edges*/
	if (n < 3) {return;}
 
         /* Map polygon cache*/
         gfxPrimitivesPolyInts = gfxPrimitivesPolyIntsGlobal;
         gfxPrimitivesPolyAllocated = gfxPrimitivesPolyAllocatedGlobal;

         /* Allocate temp array, only grow array */
         if (!gfxPrimitivesPolyAllocated) {
                 gfxPrimitivesPolyInts = (int *) malloc(sizeof(int) * n);
                 gfxPrimitivesPolyAllocated = n;
         } else {
                 if (gfxPrimitivesPolyAllocated < n) {
                         gfxPrimitivesPolyIntsNew = (int *) realloc(gfxPrimitivesPolyInts, sizeof(int) * n);
                         if (!gfxPrimitivesPolyIntsNew) {
                                 if (!gfxPrimitivesPolyInts) {
                                         free(gfxPrimitivesPolyInts);
                                         gfxPrimitivesPolyInts = NULL;
                                 }
                                 gfxPrimitivesPolyAllocated = 0;
                         } else {
                                 gfxPrimitivesPolyInts = gfxPrimitivesPolyIntsNew;
                                 gfxPrimitivesPolyAllocated = n;
                         }
                 }
         }
 
         /* Check temp array*/
         if (gfxPrimitivesPolyInts==NULL) {        
                 gfxPrimitivesPolyAllocated = 0;
         }
 
         /* Update cache variables */
        gfxPrimitivesPolyIntsGlobal =  gfxPrimitivesPolyInts;
        gfxPrimitivesPolyAllocatedGlobal = gfxPrimitivesPolyAllocated;
        
         /* Check temp array again */
         if (gfxPrimitivesPolyInts==NULL) {        
                 return;
         }
 
         /* Determine max Y*/
         miny = vy[0];
         maxy = vy[0];
         for (i = 1; (i < n); i++) {
                 if (vy[i] < miny) {
                         miny = vy[i];
                 } else if (vy[i] > maxy) {
                         maxy = vy[i];
                 }
         }
 
         /*Draw, scanning y*/
         result = 0;
         for (y = miny; (y <= maxy); y++) {
                 ints = 0;
                 for (i = 0; (i < n); i++) {
                         if (!i) {
                                 ind1 = n - 1;
                                 ind2 = 0;
                         } else {
                                 ind1 = i - 1;
                                 ind2 = i;
                         }
                         y1 = vy[ind1];
                         y2 = vy[ind2];
                         if (y1 < y2) {
                                 x1 = vx[ind1];
                                 x2 = vx[ind2];
                         } else if (y1 > y2) {
                                 y2 = vy[ind1];
                                 y1 = vy[ind2];
                                 x2 = vx[ind1];
                                 x1 = vx[ind2];
                         } else {
                                 continue;
                         }
                         if ( ((y >= y1) && (y < y2)) || ((y == maxy) && (y > y1) && (y <= y2)) ) {
                                 gfxPrimitivesPolyInts[ints++] = ((65536 * (y - y1)) / (y2 - y1)) * (x2 - x1) + (65536 * x1);
                         }           
                 }
 
                 qsort(gfxPrimitivesPolyInts, ints, sizeof(int), _gfxPrimitivesCompareInt);
 
                 for (i = 0; (i < ints); i += 2) {
                         xa = gfxPrimitivesPolyInts[i] + 1;
                         xa = (xa >> 16) + ((xa & 32768) >> 15);
                         xb = gfxPrimitivesPolyInts[i+1] - 1;
                         xb = (xb >> 16) + ((xb & 32768) >> 15);
						 lineSafe(xa,y,xb,y, color);
                 }
         }
 
         return;
}

void FxGfx::rectLine(Uint16 x,Uint16 y,Uint16 w,Uint16 h,Uint32 color){
	lineSafe(x, y, x, y + h, color);
	lineSafe(x, y, x + w, y, color);
	lineSafe(x + w, y, x + w, y + h, color);
	lineSafe(x, y + h, x + w, y + h, color);
}
void FxGfx::line(Sint16 x0, Sint16 y0, Sint16 x1, Sint16 y1, Uint32 color){
	SDL_Surface*  surface = sVideoManager->getScreen();

	//Limits
	if (y0<0) return;
	if (y0>SCREEN_HEIGHT) return;
	if (x0<0) x0 = 0;
	if (x1<0) x1 = 0;
	if (x0>SCREEN_WIDTH) x0 = SCREEN_WIDTH;
	if (x1>SCREEN_WIDTH) x1 = SCREEN_WIDTH;

	Sint16 dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
	Sint16 dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
	Sint16 err = dx+dy, e2; /* error value e_xy */

	if (SDL_MUSTLOCK(surface)){
		SDL_LockSurface(surface);
	}

	for(;;){  /* loop */
		this->putPixelSurface(x0, y0, color, surface);
		if (x0==x1 && y0==y1){
			break;
		}
		e2 = 2*err;
		if (e2 >= dy) { 
			err += dy; x0 += sx;  /* e_xy+e_x > 0 */
		}
		if (e2 <= dx) { 
			err += dx; y0 += sy;  /* e_xy+e_y < 0 */
		}
	}

	if (SDL_MUSTLOCK(surface)){
		SDL_UnlockSurface(surface);
	}

}
void FxGfx::lineSafe(Sint16 x0, Sint16 y0, Sint16 x1, Sint16 y1, Uint32 color){
	SDL_Surface*  surface = sVideoManager->getScreen();

	//Limits
	if (y0<0) return;
	if (y0>SCREEN_HEIGHT) return;
	if (x0<0) x0 = 0;
	if (x1<0) x1 = 0;
	if (x0>SCREEN_WIDTH) x0 = SCREEN_WIDTH;
	if (x1>SCREEN_WIDTH) x1 = SCREEN_WIDTH;

	Sint16 dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
	Sint16 dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
	Sint16 err = dx+dy, e2; /* error value e_xy */

	if (SDL_MUSTLOCK(surface)){
		SDL_LockSurface(surface);
	}

	for(;;){  /* loop */
		if((x0>=0)&&(x0<SCREEN_WIDTH)&&(y0>=0)&&(y0<SCREEN_HEIGHT)){
			this->putPixelSurface(x0, y0, color, surface);
		}
		if (x0==x1 && y0==y1){
			break;
		}
		e2 = 2*err;
		if (e2 >= dy) {
			err += dy; x0 += sx;  /* e_xy+e_x > 0 */
		}
		if (e2 <= dx) {
			err += dx; y0 += sy;  /* e_xy+e_y < 0 */
		}
	}

	if (SDL_MUSTLOCK(surface)){
		SDL_UnlockSurface(surface);
	}

}
void FxGfx::circle(Sint16 xm, Sint16 ym, Sint16 r, Uint32 color){
	SDL_Surface*  surface = sVideoManager->getScreen();
	Sint16 x = -r, y = 0, err = 2-2*r; // II. Quadrant

	if (SDL_MUSTLOCK(surface)){
		SDL_LockSurface(surface);
	}

	do {
		this->putPixelSurface(xm-x, ym+y, color, surface); //   I. Quadrant
		this->putPixelSurface(xm-y, ym-x,color, surface); //  II. Quadrant
		this->putPixelSurface(xm+x, ym-y,color, surface); // III. Quadrant
		this->putPixelSurface(xm+y, ym+x,color, surface); //  IV. Quadrant
		r = err;
		if (r >  x){
			err += ++x*2+1;	// e_xy+e_x > 0
		}
		if (r <= y){
			err += ++y*2+1; // e_xy+e_y < 0
		}
	} while (x < 0);

	if (SDL_MUSTLOCK(surface)){
		SDL_UnlockSurface(surface);
	}
}

void FxGfx::circleSafe(Sint16 xm, Sint16 ym, Sint16 r, Uint32 color){
	SDL_Surface*  surface = sVideoManager->getScreen();
	Sint16 x = -r, y = 0, err = 2-2*r; // II. Quadrant

	if (SDL_MUSTLOCK(surface)){
		SDL_LockSurface(surface);
	}

	do {
		if(((xm-x)>=0)&&((xm-x)<SCREEN_WIDTH)&&((ym+y)>=0)&&((ym+y)<SCREEN_HEIGHT)){
			this->putPixelSurface(xm-x, ym+y, color, surface); //   I. Quadrant
		}
		if(((xm-y)>=0)&&((xm-y)<SCREEN_WIDTH)&&((ym-x)>=0)&&((ym-x)<SCREEN_HEIGHT)){
			this->putPixelSurface(xm-y, ym-x,color, surface); //  II. Quadrant
		}
		if(((xm+x)>=0)&&((xm+x)<SCREEN_WIDTH)&&((ym-y)>=0)&&((ym-y)<SCREEN_HEIGHT)){
			this->putPixelSurface(xm+x, ym-y,color, surface); // III. Quadrant
		}
		if(((xm+y)>=0)&&((xm+y)<SCREEN_WIDTH)&&((ym+x)>=0)&&((ym+x)<SCREEN_HEIGHT)){
			this->putPixelSurface(xm+y, ym+x,color, surface); //  IV. Quadrant
		}
		r = err;
		if (r >  x){
			err += ++x*2+1;	// e_xy+e_x > 0
		}
		if (r <= y){
			err += ++y*2+1; // e_xy+e_y < 0
		}
	} while (x < 0);

	if (SDL_MUSTLOCK(surface)){
		SDL_UnlockSurface(surface);
	}
}

void FxGfx::putPixelRGBonAlpha(int x, int y, Uint32 pixel, Sint32 ID){
	SDL_Surface *surface = sResManager->getGraphicByID(ID);
    int bpp = surface->format->BytesPerPixel;
	//Only Surface with RGBA (32 bits)
	if (bpp!=4){
		return;
	}
    /* Here p is the address to the pixel we want to set */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

	if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
           p[0] = (pixel >> 16) & 0xff;
           p[1] = (pixel >> 8) & 0xff;
           p[2] = pixel & 0xff;
    } else {
           p[0] = pixel & 0xff;
           p[1] = (pixel >> 8) & 0xff;
           p[2] = (pixel >> 16) & 0xff;
    }
}

void FxGfx::convertPixelstoData(Sint32 ID, Uint8 data[]){
	SDL_Surface *surface = sResManager->getGraphicByID(ID);
	int width = surface->w;
	int height = surface->h;
	int bpp = surface->format->BytesPerPixel;
	int counterData = 0;
	Uint8 *p;

	//Only Surface with RGBA (32 bits)
	if (bpp!=4){
		return;
	}

	for (int y = 0 ; y < height ; y++ ){
		for (int x = 0 ; x < width ; x++ ){
			p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
			data[counterData] = p[0];
			counterData++;
			data[counterData] = p[1];
			counterData++;
			data[counterData] = p[2];
			counterData++;
		}
	}

}
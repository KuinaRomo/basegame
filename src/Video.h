#ifndef VIDEO_H
#define VIDEO_H

#include "includes.h"

//! Video Class
/*!
 * Video & Gfx Manager for SDL using SDL_Surface
 * This is a Singleton Class
 */
class Video
{
	public:
		enum RenderTarget{DEFAULT, MIRROR, SCREEN_ONLY, SUBSCREEN_ONLY, MAIN_SCREEN, MAIN_SUBSCREEN};
		//! Constructor
		/*!
		 * Initializing SDL System, Window or Fullscreen, VideoCard, Resolution
		 */
		Video();

		//! Destructor. Calls close()
		~Video();

		//! Draws a 2D graphic from ID number ussing SDL_BlitSurface
		/*!
		 * \param img ID surface to paint
		 * \param rect position and size of source surface
		 * \param posX position X target to paint on screen
		 * \param posY position Y target to paint on screen
		 */
		void renderGraphic(Uint32 img, C_Rectangle rect, Sint16 posX, Sint16 posY);

		//! Draws a 2D graphic from ID number ussing SDL_BlitSurface
		/*!
		 * \param img ID surface to paint
		 * \param rect position and size of source surface
		 * \param posX position X target to paint on screen
		 * \param posY position Y target to paint on screen
		 * \param alpha transparency
		 */
		void renderGraphic(Uint32 img, C_Rectangle rect, Sint16 posX, Sint16 posY, Uint8 alpha);

		//! Get a Region from Screen to a ID number surface ussing SDL_BlitSurface
		/*!
		 * \param img ID surface to target
		 * \param rect position and size of destination surface
		 * \param posX position X source of screen
		 * \param posY position Y sorrce of screen
		 */
		void getScreenRegion(Uint32 img, C_Rectangle rect, Sint16 posX, Sint16 posY);

		//! Fill screen surface with color color_key
		/*!
		 * 	\param color_key Color of the color key in ARGB
		 */
		void clearScreen( Uint32 color_key);

		//! makes an SDL_Flip
		void updateScreen();

		//! pause program
		/*!
		 *	\param ms miliseconds to pause
		 */
		void waitTime(Uint32 ms);

		//! Destroy Surface, window and close SDL
		void close();

		//! Sets the game on fullscreen or windowed mode
		/*!
		 *	param fullscreen Set mFullScreen true or false
		 */
		void setFullscreen(bool fullscreen);

		//! Gets Screen surface
		SDL_Surface* getScreen() { return mScreen; };

		//! Locks Screen surface for direct access
		void lockScreen();

		//! Unlocks Screen surface for direct access
		void unlockScreen();

		//!	Sets RenderTarget for scenes
		void setRenderTarget(RenderTarget target);
		
		//!	Gets RenderTarget for scenes
		RenderTarget getRenderTarget(){return mTarget;};

		//! Opens screen to render
		void openScreen();

		//! Opens subscreen to render
		void openSubScreen();

		//! Closes screen after render
		void closeScreen();

		//! Opens screen after render
		void closeSubScreen();

		//! Copies the buffer from screen to subscreen
		void copyScreenToSubScreen();

		//! Return the ClassName of the object.
		/*!
		 * \return String with the ClassName of the object
		 */
		std::string getClassName(){return "Video";};

		//! This class is Singleton
		static Video* getInstance();

	private:
		static Video*	instance;			/*!< This class is Singleton */
		SDL_Surface*	mScreen;			/*!< Screen surface target */
		bool			mFullScreen;		/*!< If true all render is fullscreen */
		RenderTarget 	mTarget;			/*!< Where to render*/
};

#endif /* VIDEO_H */

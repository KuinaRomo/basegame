//Include our classes
#include "Player.h"
#include "singletons.h"


Player::Player() : Entity(){
	mpSpeed = 300;
}

Player::~Player(){
}

void Player::init(){
	Entity::init();
}
void Player::init(int x, int y){
	Entity::init(x, y);
}
void Player::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);
}

void Player::render(int offX, int offY) {
	Entity::render(offX, offY);
}

void Player::update() {
	Entity::update();

}

void Player::updateControls() {
	mpDirection = NONE;

	//Direction Down
	if (sInputControl->isDirectionDown(PAD_UP, 2)) {
		std::cout << "Up down" << std::endl;
		mpDirection = UP;
	}
	if (sInputControl->isDirectionDown(PAD_DOWN, 2)) {
		std::cout << "Down down" << std::endl;
		mpDirection = DOWN;
	}
	if (sInputControl->isDirectionDown(PAD_LEFT, 2)) {
		std::cout << "Left down" << std::endl;
		mpDirection = LEFT;
	}
	if (sInputControl->isDirectionDown(PAD_RIGHT, 2)) {
		std::cout << "Right down" << std::endl;
		mpDirection = RIGHT;
	}

	//Direction release
	if (sInputControl->isDirectionReleased(PAD_UP, 2)) {
		std::cout << "Up release" << std::endl;
	}
	if (sInputControl->isDirectionReleased(PAD_DOWN, 2)) {
		std::cout << "Down release" << std::endl;
	}
	if (sInputControl->isDirectionReleased(PAD_LEFT, 2)) {
		std::cout << "Left release" << std::endl;
	}
	if (sInputControl->isDirectionReleased(PAD_RIGHT, 2)) {
		std::cout << "Right release" << std::endl;
	}

	//Button press
	if (sInputControl->isButtonPressed(BUTTON_1)) {
		std::cout << "Button press 1" << std::endl;
	}
	if (sInputControl->isButtonPressed(BUTTON_2)) {
		std::cout << "Button press 2" << std::endl;
	}
	if (sInputControl->isButtonPressed(BUTTON_3)) {
		std::cout << "Button press 3" << std::endl;
	}
	if (sInputControl->isButtonPressed(BUTTON_4)) {
		std::cout << "Button press 4" << std::endl;
	}

	//Button down
	if (sInputControl->isButtonDown(BUTTON_1)) {
		std::cout << "Button down 1" << std::endl;
	}
	if (sInputControl->isButtonDown(BUTTON_2)) {
		std::cout << "Button down 2" << std::endl;
	}
	if (sInputControl->isButtonDown(BUTTON_3)) {
		std::cout << "Button down 3" << std::endl;
	}
	if (sInputControl->isButtonDown(BUTTON_4)) {
		std::cout << "Button down 4" << std::endl;
	}

	//Button release
	if (sInputControl->isButtonReleased(BUTTON_1)) {
		std::cout << "Button release 1" << std::endl;
	}
	if (sInputControl->isButtonReleased(BUTTON_2)) {
		std::cout << "Button release 2" << std::endl;
	}
	if (sInputControl->isButtonReleased(BUTTON_3)) {
		std::cout << "Button release 3" << std::endl;
	}
	if (sInputControl->isButtonReleased(BUTTON_4)) {
		std::cout << "Button release 4" << std::endl;
	}

	//Mouse
	/*
	Uint16 mouseX = sInputControl->getMouseX();
	Uint16 mouseY = sInputControl->getMouseY();

	std::cout << "X: " << mouseX << ", Y: " << mouseY << std::endl;
	if (sInputControl->isMouseButtonPressed(MOUSEBUT_L)) {
		std::cout << "Mouse left pressed" << std::endl;
	}
	if (sInputControl->isMouseButtonReleased(MOUSEBUT_L)) {
		std::cout << "Mouse left released" << std::endl;
	}
	//*/

	return;
}

bool Player::isOfClass(std::string classType){
	if(classType == "Player" || 
		classType == "Entity"){
		return true;
	}
	return false;
}


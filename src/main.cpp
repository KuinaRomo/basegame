#include "singletons.h"
#include "Timer.h"
#include "includes.h"

#include <iostream>
#include <algorithm>

//Esto es para que funcione en VS 2015
#ifdef main
#undef main
#endif
//-----------------------

Uint32 global_delta_time;
bool key_down[255];
bool key_pressed[255];
bool key_released[255];

//#define FORCE_FIXED_TIME

void mainLoop(){
	global_delta_time = 0;
	Timer* globalTimer = new Timer();
	globalTimer->start();
	Uint32 last_time = 0;
	Uint32 max_delta = 32;

	while(true){
		if(!sDirector->getCurrentScene()->isLoaded()){
			sDirector->getCurrentScene()->load();
		}

		//Update events
		sInputControl->update();

		//Update logic
		global_delta_time = globalTimer->getTicks() - last_time;

		last_time =  globalTimer->getTicks();
		Uint32 dtime = std::min<Uint32>(global_delta_time,max_delta);

		//Forzar un tiempo en concreto (17ms = 60 Hz)
#ifdef FORCE_FIXED_TIME
		dtime = 17;
		if (global_delta_time < dtime) sVideoManager->waitTime(17 - global_delta_time);
		last_time =  globalTimer->getTicks();
#endif

		global_delta_time = dtime;
		sDirector->getCurrentScene()->onUpdate();

		if(sDirector->getCurrentScene()->isLoaded()){
			sDirector->getCurrentScene()->onDraw();
		}

		if (sInputControl->getCloseSignal()) {
			sDirector->exitGame();
		}
	}
}

int main( int argc, char* argv[] ) { 
	instanceSingletons();
	sTxtManager->init();	//Se carga primero el sistema de Text, que se usa en Scenes
	sDirector->init();		//Inicializamos el Director
	//Init basic graphics
		//----Cosas----
	//Init basic sounds and music
		//----Cosas----

	sInputControl->showCursor(true);
	sInputControl->setDelayKeyTime(false);

	mainLoop();
	return 0; 
}


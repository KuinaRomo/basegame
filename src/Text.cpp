//c++
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

//SDL
//Propios de clase
#include "Text.h"
//Propios genericos
#include "singletons.h"

Sint32	Text::mFontShadow = -1;
C_Rectangle	Text::mShadowRect = {0,0,FONT_SHADOW_TIP_W, FONT_SHADOW_HEIGHT};

Text::Text (Uint16 numFont, Uint8 variant) {
	if(mFontShadow == -1){
		mFontShadow = sResManager->getGraphicID("Assets/font/fonts/text_shadow_18p.png");
	}
	mFontNum = numFont;
	mFont = sTxtManager->getFontType(numFont);
	mVariant = variant;
	if (mVariant >= mFont->maxVariants){
		mVariant = mFont->maxVariants-1;
	}
	mLastCharToRender = -1;
	mAnimation = false;
	mAnimInCourse = false;
	mCurrentTime = 0;
	mNextFrameTime = 100;
	mTextKey = "NONE";
	mTextPosX = 0;
	mTextPosY = 0;
}

Text::~Text() {
}

void Text::setAnimation(bool anim){
	mAnimation = anim;
	if(mAnimation){
		mLastCharToRender = -1;
		mCurrentTime = 0;
	}else{
		mLastCharToRender = mPosLetter.size()-1;
	}
}

void Text::setKey(Sint16 x, Sint16 y, std::string tag, TextAlign align){
	if (!sTxtManager->isTextLoaded()){
		return;	// No hay cargados los textos aun, asi que no podemos hacer el set. Se ignora.
	}
	mTextKey = tag;
	mTextSt = sTxtManager->getText(tag);
	mAlign = align;
	decodeUTF8andPushLetters();
	setXY(x,y,align);
	mLastCharToRender = mPosLetter.size()-1;
}

void Text::setString(Sint16 x, Sint16 y, std::string text, TextAlign align){
	mTextSt = text;
	mTextKey = "NONE";
	mAlign = align;
	decodeUTF8andPushLetters();
	setXY(x,y,align);
	mLastCharToRender = mPosLetter.size()-1;
}

void Text::appendString(Sint16 x, Sint16 y, std::string append){
	if (!sTxtManager->isTextLoaded()){
		return;	// No hay cargados los textos aun, asi que no podemos hacer el set. Se ignora.
	}
	mTextSt.append(append);
	mTextKey = "NONE";
	decodeUTF8andPushLetters();
	setXY(x,y,mAlign);
	mLastCharToRender = mPosLetter.size()-1;
}

void Text::decodeUTF8andPushLetters(){
	Uint16	LongText = mTextSt.length();
	Uint16	IniY;
	//Protegemos de m�ximos tipos definidos.
	Sint16 textHeight = mFont->textHeight;
	IniY = mVariant * textHeight +1;

	LetterText	tempLetter;
	mPosDraw.w = 0;

	mPosLetter.clear();

	for (Uint16 i=0; i<LongText ; i++){
		Uint16	Index = 0;
		Uint8 Caracter;
		Caracter = mTextSt[i];
		if(Caracter == '$'){//10 variantes de fuente como mucho [0-9]
			if(i+1 < LongText){
				Sint16 number = mTextSt[i+1]-48;
				if(number >= 0 && number <= 10){
					i++;
					if(number >= mFont->maxVariants){
						number = mFont->maxVariants - 1;
					}
					IniY = number * textHeight +1;
				}else{
					IniY = mVariant * textHeight +1;
				}
			}
			continue;
		}else{
			if (Caracter < 0x80){ // Hasta (U+0080h) se trata igual que un ASCII convencional
				Index = Caracter;//-0x20; // Empieza en el ESPACIO en BLANCO (U+0020h)
			}else {
				if ((Caracter&0xF0) == 0xc0){ // Estamos en codificaci�n a 2 bytes.
					if ( ((Caracter&0x1f) == 2) || ((Caracter&0x1f) == 3) ) { // Solo hasta 255. El resto no tendria representaci�n en la fuente.
						Uint8 Caracter2 = mTextSt[i+1];
						Index = (((Caracter & 0x03) << 6) | (Caracter2 & 0x3F));// - 0x20; // Empieza en el ESPACIO en BLANCO (U+0020h)
						i++;
					}
					else {
						Index = 0; //Si es de otro rango aparece un espacio en blanco.
						i++;
					}
				}
				if ((Caracter&0xF0) == 0xE0){ // Estamos en codificaci�n a 3 bytes.
					Uint8 Caracter2 = mTextSt[i+1];
					Uint8 Caracter3 = mTextSt[i+2];
					Index = (Caracter&0x0F)<<12 | (Caracter2&0x3F)<<6 | (Caracter3&0x3F);
					//Index -=0x3040;
					i+=2;
				}
			}
		}
		tempLetter.Pos.y = IniY;
		tempLetter.Pos.h = textHeight;
		if ((Index < 0x3A) && (Index > 0x2F)){	// Special optimization Letter data for numbers. No map use
			Letter number = mFont->number[Index-0x30];
			tempLetter.Pos.x = number.PosX;
			tempLetter.Pos.w = number.width;
			tempLetter.imageFile = number.imageFile;
		}
		else{
			std::map<Uint16, Letter>::iterator iter = mFont->character.find(Index);
			if(iter == mFont->character.end()){
				iter = mFont->character.begin();
			}
			tempLetter.Pos.x = iter->second.PosX;
			tempLetter.Pos.w = iter->second.width;
			tempLetter.imageFile = iter->second.imageFile;
		}
		mPosLetter.push_back(tempLetter);
		mPosDraw.w += tempLetter.Pos.w;	// Guardo el tama�o de la frase en Pixels para Aling
	}
}

void Text::setXY(Sint16 x, Sint16 y, TextAlign align){
	switch(align){
		case TEXT_LEFT:
			mPosDraw.x = x;
			break;
		case TEXT_RIGHT:
			mPosDraw.x = x-mPosDraw.w;
			break;
		case TEXT_CENTER:
			mPosDraw.x = x-(mPosDraw.w/2);
			break;
	}
	mPosDraw.y = y;
	mTextPosX = x;
	mTextPosY = y;
}

void Text::setX(Sint16 x, TextAlign align){
	switch(align){
		case TEXT_LEFT:
			mPosDraw.x = x;
			break;
		case TEXT_RIGHT:
			mPosDraw.x = x-mPosDraw.w;
			break;
		case TEXT_CENTER:
			mPosDraw.x = x-(mPosDraw.w/2);
			break;
	}
	mTextPosX = x;
}

void Text::setY(Sint16 y){
	mPosDraw.y = y;
	mTextPosY = y;
}

void Text::setVariant(Uint8 variant){
	Uint16	iniY;
	//Protegemos de m�ximos tipos definidos.
	mVariant = variant;
	if (mVariant >= mFont->maxVariants){
		mVariant = mFont->maxVariants-1;
	}
	Sint16 textHeight = mFont->textHeight;
	iniY = mVariant * textHeight +1;

	decodeUTF8andPushLetters();
}

void Text::update(){
	if(!mAnimation || !mAnimInCourse){return;}
	mCurrentTime += global_delta_time;
	if(mCurrentTime > mNextFrameTime){
		mCurrentTime = mCurrentTime - mNextFrameTime;
		mLastCharToRender++;
		if((Uint16)mLastCharToRender >= mPosLetter.size()-1){mAnimInCourse = false;}
	}
}

void Text::render(Uint8 alpha, bool shadow){
	if(mFontNum != 0 || mFontShadow == -1){
		shadow = false;
	}
	Uint16 X = mPosDraw.x; //Posicion de cada letra
	if(shadow){
		Uint16 shadowX = X - FONT_SHADOW_TIP_W;
		Uint16 shadowY = mPosDraw.y + (mFont->textHeight - mShadowRect.h)/2;
		while(shadowX < mPosDraw.x + mPosDraw.w){
			if(shadowX == X - FONT_SHADOW_TIP_W){
				mShadowRect.x = 0;
				mShadowRect.w = FONT_SHADOW_TIP_W;
			}else{
				mShadowRect.x = FONT_SHADOW_TIP_W;
				mShadowRect.w = FONT_SHADOW_WIDTH - 2*FONT_SHADOW_TIP_W;
			}
			sVideoManager->renderGraphic(mFontShadow, mShadowRect, shadowX, shadowY);
			shadowX += mShadowRect.w;
		}
		mShadowRect.x = FONT_SHADOW_WIDTH - FONT_SHADOW_TIP_W;
		mShadowRect.w = FONT_SHADOW_TIP_W;
		sVideoManager->renderGraphic(mFontShadow, mShadowRect, shadowX, shadowY);
	}
	for (Uint16 i=0; i <= mLastCharToRender; i++){
		sVideoManager->renderGraphic(mPosLetter[i].imageFile, mPosLetter[i].Pos, X, mPosDraw.y, alpha);
		X += mPosLetter[i].Pos.w;
	}
}

void Text::render(Sint16 x, Sint16 y, Uint8 alpha, bool shadow){
	setXY(x,y, mAlign);
	render(alpha, shadow);
	setXY(mTextPosX, mTextPosY, mAlign);
}

bool Text::hasAnimationFinished(){
	if(mLastCharToRender == mPosLetter.size()-1){
		return true;
	}
	return false;
}

void Text::setAnimationTime(TextSpeed speed){
	switch(speed){
		case VERY_SLOW:
			mNextFrameTime = 400;
			break;	
		case SLOW:
			mNextFrameTime = 200;
			break;
		case FAST:
			mNextFrameTime = 70;
			break;
		case VERY_FAST:
			mNextFrameTime = 20;
			break;
		default:
			mNextFrameTime = 100;
			break;
	}
}

void Text::resetText(){
	if(mTextKey != "NONE"){
		setKey(mTextPosX, mTextPosY, mTextKey, mAlign);
	}
}
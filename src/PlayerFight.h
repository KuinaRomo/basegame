#ifndef PLAYERFIGHT_H
#define PLAYERFIGHT_H

#include "FightEntity.h"

class PlayerFight : public FightEntity
{
	public:
		PlayerFight();
		~PlayerFight();

		void update();
		void render();

		bool isOfClass(std::string classType);
		virtual std::string getClassName(){return "PlayerFight";};

		bool getMenu();
		void setMenu(bool men);

		void setActionChosed(bool action);
		bool getActionChosed();

		std::string getActionSelected();

	protected:
		//!ID from the graphic of the cursor
		int mCursor;
		int mImageMenu;
		//!Posicion del cursor. Es un n�mero del 0 al 3 y dependiendo de �l se comprueba en un array la accion que hara el personaje
		int mPositionCursor;
		//!Alturas donde se mueve el cursor en el menu principal de los personajes en la ScenFight
		std::vector<int> mNormalMenu;

		//!Booleano que comprueva si el menu ha de ser visible en pantalla o no
		bool mMenu;
		//!Solo cuando es true el jugador puede elegir una accion que hacer. Tras elegir accion resetea parametros.
		bool mActionChosed;

		//!La accion elegida por el jugador
		std::string mActionSelected;
		//!Las posibles acciones y tambien lo que se dibuja en pantalla
		std::vector<std::string> mActions;

		Text* mAction1;
		Text* mAction2;
		Text* mAction3;
		Text* mAction4;
	
	private:
	
};

#endif

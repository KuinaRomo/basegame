#include "singletons.h"

Video*				sVideoManager;
ResourceManager*	sResManager;
SceneDirector*		sDirector;
Control*			sInputControl;
FxGfx*				sFxGfx;	
TextManager*		sTxtManager;
MapManager*			sMapManager;
SoundManager*		sSoundManager;

void instanceSingletons(){
	sVideoManager = Video::getInstance();
	sResManager	  = ResourceManager::getInstance();
	sDirector	  = SceneDirector::getInstance();
	sInputControl = Control::getInstance();
	sFxGfx		  = FxGfx::getInstance();
	sTxtManager	  = TextManager::getInstance();
	sMapManager   = MapManager::getInstance();
	sSoundManager = SoundManager::getInstance();
}
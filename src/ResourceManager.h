#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include "includes.h"
#include <map>
#include <vector>

//! ResourceManager class
/*!
	Handles the load and management of the graphics in the game.
*/
class ResourceManager
{
	public:
		//! Constructor of an empty ResourceManager.
		ResourceManager();

		//! Destructor.
		~ResourceManager();

		//! Deletes a graphic from the ResourceManager map
		/*!
			\param file Filepath to the graphic
		*/
		void removeGraphic(const char* file);

		//! Gets the graphic ID
		/*!
			\param file Filepath to the graphic
			\return ID of the graphic
		*/
		Sint32 getGraphicID(const char* file);

		//! Gets the graphic path given an ID
		/*!
			\param ID of the graphic
			\return Filepath to the graphic
		*/
		std::string getGraphicPathByID(Uint32 ID);

		Sint32 getResizedImageID(const char* file, Uint16 width, Uint16 height);

		//! Returns width and Height of a Texture
		/*!
		 *	param img ID texture
		 *	param width Return variable for width value
		 *	param height Return variable for height value
		 */
		void getGraphicSize(Uint32 img, int &width, int &height);


		//! Returns width and Height of a Texture
		/*!
		 *	param img ID texture
		 *	param width Return variable for width value
		 *	param height Return variable for height value
		 */
		Uint16 getGraphicWidth(Uint32 img);
		Uint16 getGraphicHeight(Uint32 img);

		//! Returns the SDL_Surface of the graphic
		/*!
			\param ID ID of the graphic
			\return SDL_Surface
		*/
		SDL_Surface* getGraphicByID(Uint32 ID);

		//! Loads all assets at once (not used)
		void loadAllAssets();

		//! Clears all containers at once (not used)
		void unloadAllAssets();

		//! Auxiliar for when loading Surfaces. Sets a color Key
		/*!
			\param ID ID of the graphic
			\param color_key Color of the color key in ARGB
		*/
		void setColorKey(Uint32 ID, Uint32 color_key);

		//! Change general Alpha value to paint a concrete surface
		/*!
			\param ID ID of the graphic
			\param alpha_value From SDL_ALPHA_TRANSPARENT(0) to SDL_ALPHA_OPAQUE(255)
		*/
		void setAlpha(Uint32 ID, Uint8 alpha_value);

		//! Prints the path to loaded graphics
		void printLoadedGraphics();

		//! Create a new surface graphic to the ResourceManager
		/*!
			\param name for the graphic
			\return -1 if there's an error when loading
		*/
		Sint32 createGraphic(const char* name, Uint16 width, Uint16 height);

		//! Saves a Surface as BMP
		void saveAsBMP(Uint32 ID, char* filename){SDL_SaveBMP(getGraphicByID(ID), filename);};

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		std::string getClassName(){return "ResourceManager";};

		//! Gets Singleton instance
		/*!
			\return Instance of ResourceManager (Singleton).
		*/
		static ResourceManager* getInstance();

	private:

		//! Adds a graphic to the ResourceManager
		/*!
			\param file Filepath to the graphic
			\return -1 if there's an error when loading
		*/
		Sint32 addGraphic(const char* file);

		//! Adds a resized graphic to the ResourceManager
		/*!
			\param file Filepath to the graphic
			\return -1 if there's an error when loading
		*/
		Sint32 addResizedGraphic(const char* file, Uint16 width, Uint16 height);

		//! Searches in the ResourceManager and gets the graphic by its name. If it isn't there, loads it
		/*!
			\param file Filepath of the graphic
			\return SDL_Surface
		*/
		SDL_Surface* getGraphic(const char* file);

		//! Searches the graphic in the vector and returns its ID
		/*!
			\param img SDL_Surface of the graphic
			\return ID of the graphic
		*/
		Sint32 searchGraphic(SDL_Surface* img);

		//! Searches the first NULL in mGraphicsVector and updates mFirstFreeSlot to store its position
		/*!
			\return Index of the first NULL in mGraphicsVector
		*/
		Uint32 updateFirstFreeSlot();

		std::map<std::string, SDL_Surface*> mGraphicsMap;	/*!<  Map that stores Surfaces. Useful for direct access*/
		std::vector<SDL_Surface*>			mGraphicsVector;/*!<  Vector that stores Surfaces. Useful for sequential access*/
		std::map<std::string, Sint32>		mIDMap;			/*!<  Map that stores ID. Links strings to ID, useful for sequential access*/

		Uint32								mFirstFreeSlot;	/*!<  First free slot in the mGraphicsVector*/
		static ResourceManager*				pInstance;		/*!<  Singleton instance*/
};

#endif

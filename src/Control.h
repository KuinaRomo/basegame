#ifndef CONTROL_H
#define CONTROL_H

//c++
#include <vector>
//SDL
#include "includes.h"
//Propios de clase
//Propios genericos

#define   PAD_UP      8
#define   PAD_RIGHT   4
#define   PAD_DOWN    2
#define   PAD_LEFT    1

#define   MAXBUTTONS 16		// Proteccion para Uint16.

#define   BUTTON_1      1	// space || Button A
#define   BUTTON_2      2	// z  || Button Y 
#define   BUTTON_3      4	// x  || Button B 
#define   BUTTON_4      8	// c  || Button X 

#define   BUTTON_5      16	// a  || LB 
#define   BUTTON_6      32	// s  || RB 
#define   BUTTON_7      64
#define   BUTTON_8      128

#define   BUTTON_9      256
#define   BUTTON_10     512
#define   BUTTON_11     1024	// return  || Start - Pause o Enter
#define   BUTTON_12     2048	// esc  || back - Pues eso pa atras.

#define   BUTTON_13     4096	// 1  || Hat Up 
#define   BUTTON_14     8192	// 2  || Hat Right 
#define   BUTTON_15     16384	// 3  || Hat Down 
#define   BUTTON_16     32768	// 4  || Hat Left

// TO DO: Poder redefinir el HAT tambien.

#define   HATBUTTON1	BUTTON_13  	// 1  || Hat Up - Camuflaje 1
#define   HATBUTTON2	BUTTON_14 	// 2  || Hat Right - Camuflaje 2
#define   HATBUTTON3	BUTTON_15 	// 3  || Hat Down - Camuflaje 3
#define   HATBUTTON4	BUTTON_16	// 4  || Hat Left - Camuflaje 4

#define   MOUSEBUT_L    1
#define   MOUSEBUT_C    2
#define   MOUSEBUT_R    4
#define   MOUSEWHEELUP	8
#define   MOUSEWHEELDOWN	16

//! Control class
/*!
	Class for getting the input from keyboard and/or gamepad.
*/
class Control
{
	private:
		int keyboardButtons[MAXBUTTONS] = {//Modify this to use other buttons to do things
			SDLK_SPACE,		SDLK_z,		SDLK_x, 	SDLK_c,
			SDLK_a, 		SDLK_s, 		-1, 		-1,
			-1,				-1, SDLK_RETURN,	SDLK_BACKSPACE,
			SDLK_1,			SDLK_2,		SDLK_3,		SDLK_4
		};
	protected:
		//! Constructor.
		Control();

	public:
		//! Destructor
		~Control();

		//! Gets Singleton instance
		/*!
			\return Instance of Control (Singleton).
		*/
		static Control*	getInstance();
	  
		//! Initializes game input (keyboard and gamepad if connected)
		void init();

		//! Deinitializes game input (keyboard and gamepad if connected)
		void close();

		//! Gets direction keys pressed
		Uint8 getDirection();

		//! Gets direction keys on hat pressed
		Uint8 getDirectionHat();

		//! Gets direction keys released
		Uint8 getDirectionRelease();

		//! Gets action button keys down
		Uint16 getButtons();

		//! Gets action button keys pressed
		Uint16 getButtonsPressed();

		//! Gets action button keys released
		Uint16 getButtonsRelease();


		//! Gets if direction is pressed down (including hat or not)
		/*!
			\param dir direction to test
			\param hatMode if you want to check stick, d-pad, or both (0, 1, 2)
			\return bool
		*/
		bool isDirectionDown(Uint8 dir, int hatMode);

		//! Gets if direction has been released (including hat or not)
		/*!
		\param dir direction to test
		\param hatMode if you want to check stick, d-pad, or both (0, 1, 2)
		\return bool
		*/
		bool isDirectionReleased(Uint8 dir, int hatMode);

		//! Gets if button is down
		bool isButtonDown(Uint16 btn);

		//! Gets if button is pressed
		bool isButtonPressed(Uint16 btn);

		//! Gets if button is released
		bool isButtonReleased(Uint16 btn);

		//! Gets if mouse button is pressed
		bool isMouseButtonPressed(Uint16 btn);

		//! Gets if mouse button is released
		bool isMouseButtonReleased(Uint16 btn);

		//! Gets the value of the pressed key 
		/*!
			\return mKey.
		*/
		Uint16 getKey();

		//! Returns the value of the joystick in X 
		Sint16 getPadJoyEjeX();

		//! Returns the value of the joystick in Y 
		Sint16 getPadJoyEjeY();

		//! Returns the position of the mouse in X 
		int getMouseX();

		//! Returns the position of the mouse in Y
		int getMouseY();

		//! Returns the buttons down on the mouse 
		Uint8 getMouseBut();
		
		//! Returns the buttons released on the mouse
		Uint8 getMouseButRelease();

		void setMousePosition(Uint16 x, Uint16 y){SDL_WarpMouse(x, y);};

		//! Returns if a button to go Back (ESC) is pressed
		bool getBackSignal();

		//! Returns if a button to Accept (ENTER or JOY 1st BUTTON) is pressed
		bool getAcceptSignal();

		//! Returns if a button to go Back in a menu (ESC) is pressed
		bool getMenuBackSignal();

		//! Returns if a button to go exit the game is pressed
		bool getCloseSignal();

		//! Returns if a the application has lost input focus
		bool getFocusOutSignal();

		//! Returns if the escape key is being pressed
		bool getEscapeSignal();

		//! Sets accept signal on press down
		void setAcceptSignalCont(bool continuous = false);

		//! Returns the Joystick game
		const char*	getJoystickName();

		//! Sets a delay time between each keystroke (by default 250ms)
		void setDelayKeyTime(bool on, Uint32 ms=250); 

		//! Shows or hides mouse cursor
		void showCursor(bool show);

		//! Updates gamePad and/or keyboard states.
		void update();

		//! Maps Default Keyboard buttons to game input for this Game
		void mapDefaultKeyButtons();

		//! Maps default Pad buttons (Xbox360) to game input for this Game
		void mapDefaultPadButtons();

		//! Maps a key or pad vector using another vector
		void mapButtonsVector(std::vector<int> buttons, bool padVector);

		//! Returns the ClassName of the object.
		std::string	getClassName(){return "Control";};

		//! Checks if there is a pad connected and opens it
		void searchPad();

		//! Sets the Hat to move the character
		void useHat(bool use){mUseHat = use;};

		//! Returns if the Hat is being used to move the character
		bool isUsingHat(){return mUseHat;};

		//! Returns if pad conected is a Xbox 360 pad
		bool isXboxPad();

		std::vector<int> getJoyButtons(){return mJoyButton;};
		std::vector<int> getKeyButtons(){return mKeyButton;};

	private:
		static Control*		pInstance;		/*!<  Singleton instance of the Class*/

		SDL_Event			mEvento;		/*!<  SDL Event for keystrokes and messages*/
		SDL_Joystick*		mPadJoy;		/*!<  Pointer to Joystick structure*/
		int					mButtonsMax;	/*!<  Maximum number of detected buttons*/
		std::vector<int>	mJoyButton;		/*!<  Buttons in use of a joystick*/
		std::vector<int>	mKeyButton;		/*!<  Keys in use of a keyboard*/
		bool				mIsHat;			/*!<  True if joystick has a Hat*/
		bool				mUseHat;		/*!<  True if using hat to move character */

		bool				mIsDelayKey;		/*!<  Set to true to activate input capture with delay*/
		Uint32				mDelayKeyTimeMax;	/*!<  Maximum delay*/
		Uint32				mDelayKeyTimeIni;	/*!<  Control initialization time*/

		Uint8				mDirection;			/*!<  Nibble under XXXX URDL (Up-Right-Down-Left)*/
		Uint8				mDirectionHat;		/*!<  Nibble under XXXX URDL (Up-Right-Down-Left) for Hat Only*/
		Uint8				mDirectionRelease;	/*!<  Release of a nibble under XXXX URDL (Up-Right-Down-Left)*/
		Uint8				mDirectionHatRelease;	/*!<  Release of a nibble under XXXX URDL (Up-Right-Down-Left)*/
		Uint8				mDirectionPad_Last;	/*!<  Last Direction Pad for make a release of Pad Direction*/
		Uint8				mDirectionHat_Last;	/*!<  Last Direction Hat for make a release of Pad Direction*/

		Uint16				mButtons;			/*!<  Fire Buttons (7-0) down*/
		Uint16				mButtonsPressed;	/*!<  Fire Buttons (7-0) pressed*/
		Uint16				mButtonsRelease;	/*!<  Fire Buttons (7-0) released*/

		Uint16				mKey;				/*!<  Pressed Key of a Keyboard*/

		Sint16				mPadJoyEjeX;		/*!<  Motion of the Analog Joystick in X*/
		Sint16				mPadJoyEjeY;		/*!<  Motion of the Analog Joystick in Y*/

		int					mMouseX;			/*!<  Absolute coordinate of the mouse in X*/
		int					mMouseY;			/*!<  Absolute coordinate of the mouse in X*/
		Uint8				mMouseBut;			/*!<  Mouse buttons pressed*/
		Uint8				mMouseButRelease;	/*!<  Mouse buttons released*/

		bool				mBackSignal;		/*!<  Key or Button to go Back (e.g. ESC)*/
		bool				mAcceptSignal;		/*!<  Key or Button to Accept (e.g. ENTER or JOY 1st BUTTON)*/
		bool				mBackMenuSignal;	/*!<  Key or Button to go Back in menus (BACKSPACE or JOY 2nd BUTTON)*/
		bool				mCloseSignal;		/*!<  Close of game window*/
		bool				mEscapeSignal;		/*!<  Escape Key*/
		bool				mFocusOutSignal;	/*!<  When the application loses input focus*/

		bool				mContinuousAcceptSignal; /*!< Sets accept signal on press down, not on up*/

		const char*			mJoystickName;		/*!<  Name of the gamepad*/
};
#endif /* CONTROL_H */

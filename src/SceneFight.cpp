//Include our classes
#include "SceneFight.h"

SceneFight::SceneFight() : Scene() {
	//font.loadFont("final_fantasy_36_font.ttf", 20);
	mHigh = 0;
	mOpenedMenu = false;
}

SceneFight::~SceneFight(){
}

void SceneFight::init() {
	Scene::init();
	//Inicializando jugadores
	//AQUI BUG QUE NO DEJA
	a_player = new Dante();
	mPlayers.push_back(a_player);
	a_player = new Spartan();
	mPlayers.push_back(a_player);

	//Cambiamos el valor de entities a el n�mero de players mas el numero de enemigos. Servira para saber el oden de los turnos.
	mEntities.resize(mPlayers.size());

	//Inicializamos variables
	mTurn = 0;
	mSpace = 0;

	//Inicializar texto
	mInfoPlayer = new Text(0, 0);
	//mInfoPlayer2 = new Text(0, 0);
}

void SceneFight::updateScene(){
	for (int i = 0; i < mPlayers.size(); i++) {
		mPlayers[i]->update();
		//Comprobamos si se a cargado el turno
		if (mPlayers[i]->getPrepared()) {
			//Cuando una entidad se le ha llenado la barra de turno se mete dentro del vector mEntities en el espacio mSpace y se suma uno a mSpace 
			//o se vuelve 0 si es igual a mEntities.size()
			mEntities[mSpace] = mPlayers[i];
			if (mSpace == mEntities.size() - 1) {
				mSpace = 0;
			}
			else {
				mSpace++;
			}
			mPlayers[i]->setPrepared(false);
		}
	}

	//Comprobamos si mMenu es false en todos. Si lo es abrimos un menu o atacamos con el boss
	mOpenedMenu = false;
	if (mEntities[mTurn] != NULL) {
		if (mEntities[mTurn]->getClassName() != "Enemy") {
			if (((PlayerFight*)mEntities[mTurn])->getMenu()) {
				mOpenedMenu = true;
				if (((PlayerFight*)mEntities[mTurn])->getActionChosed()) {
					((PlayerFight*)mEntities[mTurn])->setActionChosed(false);
					mEntities[mTurn] = NULL;
					/*if (mTurn < 1) {
						mTurn++;
					}
					else {
						mTurn = 0;
					}*/
				}
			}
		}
		//Comprobar si el monstruo esta listo para atacar
	}

	if (!mOpenedMenu) {
		//std::cout << "Hello world" << std::endl;
		mOpenedMenu = true;
		if(mEntities[mTurn] != NULL){
			if (mEntities[mTurn]->getClassName() != "Enemy") {
				//if (mEntities[mTurn]->getAttack()) {
					((PlayerFight*)mEntities[mTurn])->setMenu(true);
				//}
			}
		}
	}
	/*else {
		mOpenedMenu = false;
	}*/

	if (key_pressed['Z'] || key_pressed['z']) {
		if (mEntities[mTurn] != NULL) {
			if (mEntities[mTurn]->getAttack()) {
				((PlayerFight*)mEntities[mTurn])->setActionChosed(true);
				((PlayerFight*)mEntities[mTurn])->setMenu(false);
				mOpenedMenu = false;
				mEntities[mTurn] = NULL;
				if (mTurn < 1) {
					mTurn++;
				}
				else {
					mTurn = 0;
				}
			}
		}
	}
}

void SceneFight::drawScene() {
	//Dibujo del cuadrado de informacion inferior
	sFxGfx->rectFill(0, SCREEN_HEIGHT - 250, SCREEN_WIDTH, 250, 0x0000FF);
	
	//Draw information players
	std::cout << mPlayers.size() << std::endl;
	mHigh = 190;
	for (int i = 0; i < mPlayers.size(); i++) {
		std::string aux = mPlayers[i]->getName() + "   " + itos(mPlayers[i]->getLife(), 1);
		mInfoPlayer->setString(SCREEN_WIDTH - 500, SCREEN_HEIGHT - (mHigh - 20), aux);
		//font.drawString(mPlayers[i]->getName() + "   " + itos(mPlayers[i]->getLife(), 1), SCREEN_WIDTH - 500, SCREEN_HEIGHT - (mHigh - 20));
		//ofSetColor(192, 192, 192);
		//ofDrawRectangle(SCREEN_WIDTH - 350, SCREEN_HEIGHT - mHigh, 250, 25);
		sFxGfx->rectFill(SCREEN_WIDTH - 350, SCREEN_HEIGHT - mHigh, 250, 25, 0xC0C0C0);
		//ofSetColor(0, 205, 209);
		//ofDrawRectangle(SCREEN_WIDTH - 350, SCREEN_HEIGHT - mHigh, mPlayers[i]->getLoading(), 25);
		sFxGfx->rectFill(SCREEN_WIDTH - 350, SCREEN_HEIGHT - mHigh, mPlayers[i]->getLoading(), 25, 0x00CDD1);
		mHigh -= 50;
		//Call renders
		mPlayers[i]->render();
	}
	
}

bool SceneFight::isOfClass(std::string classType){
	if(classType == "SceneFight"){
		return true;
	}
	return false;
}




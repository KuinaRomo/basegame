//Include our classes
#include "singletons.h"
#include "Renderer.h"


void imgRender(int ID, int x, int y, C_Rectangle rect) {
	sVideoManager->renderGraphic(ID, rect, x, y);
	return;
}

void imgRender(int ID, int x, int y, C_Rectangle rect, int alpha) {
	sVideoManager->renderGraphic(ID, rect, x, y, alpha);
	return;
}

void imgRender(int ID, int x, int y, C_Rectangle rect, RenderColor color, int alpha) {
	sVideoManager->renderGraphic(ID, rect, x, y, alpha);
	return;
}

void imgRender(int ID, int x, int y, C_Rectangle rect, int r, int g, int b, int alpha) {
	sVideoManager->renderGraphic(ID, rect, x, y, alpha);
	return;
}
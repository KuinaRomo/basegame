#ifndef DANTE_H
#define DANTE_H

#include "includes.h"
#include "PlayerFight.h"

class Dante : public PlayerFight
{
	public:
		Dante();
		~Dante();

		void update();
		void render();

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Dante";};



	protected:
		
				
	private:
	
};

#endif
